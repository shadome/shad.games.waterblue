﻿using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.orchestration.model;
using shad.games.waterblue.orchestration.services;
using shad.games.waterblue.orchestration.services.objects;
using System;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.robot.services
{
    public static class RobotService
    {
        private static ClientToken token;

        private static readonly Random random = new();

        public static ClientCallbacks GetRobotCallbacks()
        {
            return new ClientCallbacks(
                onNewBattle: RobotService.OnNewBattle,
                onNewTurn: RobotService.OnNewTurn,
                onSwapAction: RobotService.OnSwapAction,
                onMoveAction: RobotService.OnMoveAction,
                onRequiredSwap: RobotService.OnRequiredSwap,
                onBattleEnd: RobotService.OnBattleEnd);
        }

        /* Internal */

        internal static void OnNewBattle(
            ClientToken token,
            Battlefield battlefield)
        {
            RobotService.token = token;
        }

        internal static void OnNewTurn(
            Battlefield battlefield)
        {
            var token = RobotService.token;
            var team = battlefield.field_monsters[token.team_colour]
                .Where(_bmonster => !_bmonster.IsKnockedOut())
                .ToImmutableArray();
            var target = battlefield.field_monsters[TeamColour.GetOpposite(token.team_colour)]
                .FirstOrDefault(_bmonster => !_bmonster.IsKnockedOut());
            if (target != default)
            {
                var target_position = battlefield.TryGetMonsterFieldPosition(target.id);
                foreach (var _bmonster in team)
                {
                    var move = _bmonster.moveset[0];
                    var move_action = MoveAction.Create(
                        caster_id: _bmonster.id,
                        move_id: move.id,
                        target_team: TeamColour.GetOpposite(token.team_colour),
                        target_position: target_position);
                    BattleService.RegisterMove(
                        token: token,
                        move_action: move_action);
                }
            }
            BattleService.LockRegistrations(token);
        }

        internal static void OnRequiredSwap(
            Battlefield battlefield,
            BattleMonsterId outgoing,
            ImmutableArray<BattleMonsterId> eligible_swap_positions)
        {
            var random_index = RobotService.random.Next(0, eligible_swap_positions.Length);
            var random_id = eligible_swap_positions[random_index];
            var swapaction = SwapAction.Create(outgoing: outgoing, incoming: random_id);
            BattleService.RegisterSwap(
                token: RobotService.token, 
                swap_action: swapaction);
            BattleService.LockRegistrations(token: RobotService.token);
        }

        /* empty callbacks */

        internal static void OnSwapAction(
            SwapAction swapaction,
            Battlefield before,
            Battlefield after)
        {
            
        }

        internal static void OnMoveAction(
            MoveActionLog move_action_log)
        {
            
        }
        
        internal static void OnBattleEnd(
            Battlefield battlefield,
            BattleResult result)
        {
            // happy robot or sad robot?
        }

    }
}
