using System.Collections.Immutable;
using System.Linq;
using shad.games.waterblue.robot.services;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.orchestration.model;
using shad.games.waterblue.orchestration.services;
using shad.games.waterblue.orchestration.services.objects;
using shad.games.waterblue.data;
using System.Threading.Tasks;
using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.lib.TextUtils;

namespace shad.games.waterblue.front.pages
{
    public partial class BattlefieldPage
    {
        protected async Task StartGame()
        {
            var robot_callbacks = RobotService.GetRobotCallbacks();
            var robot_team = new[]
                {
                    MonsterDictionary.n063_abra,
                    MonsterDictionary.n063_abra,
                    MonsterDictionary.n094_gengar,
                    // MonsterDictionary.n026_raichu,
                    // MonsterDictionary.n091_cloyster,
                }
                .ToImmutableArray();
            var player_callbacks = new ClientCallbacks(
                onNewBattle: this.OnNewBattle,
                onNewTurn: this.OnNewTurn,
                onRequiredSwap: this.OnRequiredSwap,
                onMoveAction: this.OnMoveAction,
                onSwapAction: this.OnSwapAction,
                onBattleEnd: this.OnBattleEnd);
            var player_team = new[]
                {
                    MonsterDictionary.n026_raichu,
                    MonsterDictionary.n091_cloyster,
                    MonsterDictionary.n094_gengar,
                    // MonsterDictionary.n063_abra,
                    // MonsterDictionary.n026_raichu,
                }
                .ToImmutableArray();
            await BattleService.RunBattleWithRobot(
                player_team: player_team,
                robot_team: robot_team,
                player_callbacks: player_callbacks,
                robot_callbacks: robot_callbacks);
        }

        // The first time this callback is called will occur during other events being processed (damage application, etc.).
        // If called more than once (two monsters KO), the second call will wait for the first call to be resolved.
        private void OnRequiredSwap(
            Battlefield battlefield, 
            BattleMonsterId outgoing_monster,
            ImmutableArray<BattleMonsterId> eligible_incoming_monsters) 
        {
            var monster_position = battlefield.TryGetMonsterFieldPosition(outgoing_monster);
            var monster_name = monster_position != null
                ? battlefield[monster_position.Value].monster.name.ToTitleCase()
                : "?unknown?";
            var log = $"Choose a replacement for {monster_name}.";
            var @event = new BattlefieldEvent(
                Type: BattlefieldEventType.RequiredSwap,
                BattlefieldState: battlefield,
                Log: log,
                RequriedSwapOutgoing: outgoing_monster,
                RequiredSwapEligibleReplacements: eligible_incoming_monsters);
            this.EventQueue.Enqueue(@event);
        }
        
        private void OnBattleEnd(
            Battlefield battlefield, 
            BattleResult result)
        {
            var message = result switch
            {
                BattleResult.Draw => "Draw",
                BattleResult.Win => "You have won!",
                BattleResult.Loss => "You have lost...",
                _ => "OnEndOfBattle technical error, should never happen",
            };
            var battlefield_event = new BattlefieldEvent(
                Type: BattlefieldEventType.BattleEnded,
                BattlefieldState: battlefield,
                Log: message);
            this.EventQueue.Enqueue(battlefield_event);

            base.StateHasChanged();
        }

        private void OnNewTurn(Battlefield battlefield)
        {
            var battlefield_event = new BattlefieldEvent(
                Type: BattlefieldEventType.NewTurn,
                BattlefieldState: battlefield,
                Log: BattlefieldPage.DEFAULT_WAITING_MESSAGE);
            this.EventQueue.Enqueue(battlefield_event);
            
            base.StateHasChanged();
        }

        private void OnNewBattle(
            ClientToken token, 
            Battlefield battlefield)
        {
            this.Token = token;
            this.ResetMonsterViewModels(battlefield: battlefield);
            var field_monsters_id_not_ko = battlefield.field_monsters
                .SelectMany(x => x.Value)
                .Where(_monster => !_monster.IsKnockedOut())
                .Select(_monster => _monster.id);
            foreach (var _id in field_monsters_id_not_ko)
            {
                var battlefield_event_1 = new BattlefieldEvent(
                    Type: BattlefieldEventType.UpdateMonsters,
                    BattlefieldState: battlefield,
                    Log: string.Empty);
                this.EventQueue.Enqueue(battlefield_event_1);
                var swap_action = SwapAction.Create(
                    outgoing: _id,
                    incoming: _id);
                var battlefield_event_2 = new BattlefieldEvent(
                    Type: BattlefieldEventType.SwapInAction,
                    BattlefieldState: battlefield,
                    Log: string.Empty,
                    SwapAction: swap_action);
                this.EventQueue.Enqueue(battlefield_event_2);
            }
        }

        private void OnMoveAction(
            MoveActionLog move_action_log)
        {
            var battlefield_before = move_action_log.battlefield_before;
            var battlefield_after = move_action_log.battlefield_after;
            var move_action = move_action_log.move_action;
            var field_monsters = battlefield_before.field_monsters.Values
                .SelectMany(x => x);
            var caster = field_monsters
                .FirstOrDefault(_monster => _monster.id == move_action.caster_id);
            var target = move_action.target_position == null
                ? (BattleMonster?)null
                : battlefield_before[move_action.target_position!.Value];
            var target_mitigation_log = target != null
                ? move_action_log.mitigation_logs.First(_log => _log.monster_id == target!.Value.id)
                : (MitigationLog?)null;
            var move = caster.moveset
                .FirstOrDefault(_move => _move.id == move_action.move_id);
            var message = $"{caster.monster.name.ToTitleCase()} uses {move.move.name.ToTitleCase()}";
            message += target == null
                ? string.Empty 
                : $" on {target!.Value.monster.name.ToTitleCase()}";
            message += 
                target_mitigation_log?.type_effectiveness == TypeEffectiveness.Effective ? $". It's super effective!" :
                target_mitigation_log?.type_effectiveness == TypeEffectiveness.Resistant ? $". It's not very effective..." :
                ".";

            var battlefield_event = new BattlefieldEvent(
                Type: BattlefieldEventType.MoveAction,
                BattlefieldState: battlefield_after,
                Log: message,
                MoveActionLog: move_action_log);
            this.EventQueue.Enqueue(battlefield_event);

            base.StateHasChanged();
        }

        private void OnSwapAction(
            SwapAction swap_action,
            Battlefield battlefield_before,
            Battlefield battlefield_after)
        {
            // will also be called for KO replacements,
            // do not animate if the swapping out is KO            
            var outgoing = battlefield_before.field_monsters.Values
                .SelectMany(x => x)
                .FirstOrDefault(_monster => _monster.id == swap_action.outgoing);
            var incoming = battlefield_before.party_monsters.Values
                .SelectMany(x => x)
                .FirstOrDefault(_monster => _monster.id == swap_action.incoming);
            var message = $"{outgoing.monster.name.ToTitleCase()}, come back. Let's go {incoming.monster.name.ToTitleCase()}!";
            var battlefield_event_1 = new BattlefieldEvent(
                Type: BattlefieldEventType.SwapOutAction,
                BattlefieldState: battlefield_before,
                Log: message,
                SwapAction: swap_action);
            var battlefield_event_2 = new BattlefieldEvent(
                Type: BattlefieldEventType.UpdateMonsters,
                BattlefieldState: battlefield_after,
                Log: message);
            var battlefield_event_3 = new BattlefieldEvent(
                Type: BattlefieldEventType.SwapInAction,
                BattlefieldState: battlefield_after,
                Log: message,
                SwapAction: swap_action);
            this.EventQueue.Enqueue(battlefield_event_1);
            this.EventQueue.Enqueue(battlefield_event_2);
            this.EventQueue.Enqueue(battlefield_event_3);

            base.StateHasChanged();
        }

    }
}
