using System.Linq;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.orchestration.services;
using System.Threading.Tasks;
using shad.games.waterblue.front.model;
using System.Collections.Immutable;

namespace shad.games.waterblue.front.pages
{
    public partial class BattlefieldPage
    {
        protected async override Task OnInitializedAsync()
        {
            await this.StartGame();
        }

        private async Task ProcessEvent()
        {
            if (this.IsEventBeingProcessed || !this.EventQueue.Any())
            {
                return;
            }
            this.IsEventBeingProcessed = true;
            var battlefield_event = this.EventQueue.Dequeue();
            this.BattleLogMessage = battlefield_event.Log;
            base.StateHasChanged();

            switch (battlefield_event.Type)
            {
                case BattlefieldEventType.UpdateMonsters:
                    this.ResetMonsterViewModels(battlefield_event.BattlefieldState);
                    this.ClearAnimations();
                    break;
                case BattlefieldEventType.RequiredSwap:
                    this.LoadSwapOut(
                        swapping_out: battlefield_event.RequriedSwapOutgoing!.Value,
                        is_choice: false,
                        eligible_swapins: battlefield_event.RequiredSwapEligibleReplacements!.Value);
                    break;
                case BattlefieldEventType.SwapInAction:
                    await this.AnimateSwapIn(
                        incoming: battlefield_event.SwapAction!.Value.incoming);
                    await Task.Delay(500);
                    break;
                case BattlefieldEventType.SwapOutAction:
                        System.Console.WriteLine("animating swapout from action");
                    await this.AnimateSwapOut(
                        outgoing: battlefield_event.SwapAction!.Value.outgoing);
                    await Task.Delay(500);
                    break;
                case BattlefieldEventType.MoveAction:
                    var move_action_log = battlefield_event.MoveActionLog!.Value;
                    await this.AnimateMove(
                        battlefield: battlefield_event.BattlefieldState, 
                        move_action: move_action_log.move_action);
                    this.ClearAnimations();
                    foreach (var _mitigation_log in move_action_log.mitigation_logs)
                    {
                        await this.AnimateDamagedByMove(target_id: _mitigation_log.monster_id);
                    }
                    this.ResetMonsterViewModels(battlefield_event.BattlefieldState);
                    base.StateHasChanged(); 
                    // awaiting the blink animation + healthbar update triggered by the battlefield refresh
                    await Task.Delay(500);
                    var knocked_out_monsters = move_action_log.mitigation_logs
                        .Where(_log => _log.has_knocked_out_target);
                    foreach (var _mitigation_log in knocked_out_monsters)
                    {
                        await this.AnimateSwapOut(outgoing: _mitigation_log.monster_id);
                    }
                    break;
                case BattlefieldEventType.NewTurn:
                case BattlefieldEventType.BattleEnded:
                default:
                    this.PendingMoveTargetSelection = null;
                    this.UserChoicePendingSwapOutId = null;
                    this.RegisteredMoveActions = new();
                    this.RegisteredSwapActions = new();
                    this.ResetMonsterViewModels(battlefield_event.BattlefieldState);
                    break;
            }
            this.CurrentBattlefieldState = battlefield_event.BattlefieldState;
            this.IsEventBeingProcessed = false;
            base.StateHasChanged();
        }
        
        /// <summary>
        /// Called when the swap button of a monster bar is clicked by the user.
        /// </summary>
        private void OnSwapClicked(BattleMonsterId monster_id)
        {
            // Do nothing if the swap action button is clicked when there already is a required pending swap selection
            if (this.RequiredSwapPendingSwapOutId != null)
            {
                return;
            }
            // whether the monster has already a swapout registered
            var is_unregistering_swap = this.RegisteredSwapActions
                .Any(_swapaction => _swapaction.outgoing == monster_id);
            // whether the swap window is open and the user clicks the swap button to close it
            var is_closing_window = this.UserChoicePendingSwapOutId == monster_id;
            /* First, unregister any possible action for the monster (swap or move) */
            var (pending_swaps, pending_moves) = BattleService.UnregisterAction(
                token: this.Token, 
                monster_id: monster_id);
            this.UpdateRegistrations(
                pending_moves: pending_moves, 
                pending_swaps: pending_swaps,
                shouldReset: true);
            /* If the user is not closing an open swap modal and is not cancelling a previous swap registration
             * - update the pending swap's outgoing monster id and 
             * - update the party monsters to set their "can_swap_in" flag */
            if (!is_closing_window && !is_unregistering_swap)
            {
                this.LoadSwapOut(swapping_out: monster_id, is_choice: true);
            }
            base.StateHasChanged();
        }

        private void OnMoveSelected(BattleMonsterId monster_id, BattleMoveId move_id)
        {
            this.UserChoicePendingSwapOutId = null;
            var monster = this.PlayerFieldMonsters
                .First(_monstervm => _monstervm?.Monster.Monster.id == monster_id)
                .Monster.Monster;
            var move = monster.moveset
                .First(_move => _move.id == move_id);
            // two clicks on the same move: cancelling the pending move target selection
            var move_clicked_twice = this.PendingMoveTargetSelection?.MoveId == move_id;
            // no choice of a target, cancelling any pending move target selection
            var is_move_with_fixed_targets = move.move.move_target_type != MoveTargetType.ChoiceBattlefieldSingleExceptCaster;
            var is_cancelling_selected_move = this.RegisteredMoveActions.Any(_moveaction => _moveaction.move_id == move_id);
            var should_reset = is_cancelling_selected_move || move_clicked_twice;
            var (pending_swaps, pending_moves) = BattleService.UnregisterAction(
                token: this.Token, 
                monster_id: monster_id);
            // register the move if it doesn´t need any target selection
            if (is_move_with_fixed_targets)
            {
                var move_action = MoveAction.Create(
                    caster_id: monster_id,
                    move_id: move_id,
                    target_team: null,
                    target_position: null);
                // updates in a side-effect manner the already defined pending collections for the upcoming UpdateRegistrations call
                (pending_swaps, pending_moves) = BattleService.RegisterMove(
                    token: this.Token,
                    move_action: move_action);
            }
            this.UpdateRegistrations(
                pending_moves: pending_moves, 
                pending_swaps: pending_swaps,
                shouldReset: should_reset);
            // note: same behaviour whether it should reset or a move without targets was registered
            if (is_move_with_fixed_targets || should_reset)
            {   // clicking on a move that was registered: only cancel the registration
                base.StateHasChanged();
                return;
            }
            // needing a target, setting up its pending selection state
            var targets = this.PlayerFieldMonsters
                .Concat(this.OpponentFieldMonsters)
                .Select(_monster => (
                    monster_id: _monster.Monster.Monster.id, 
                    is_selectable: _monster.Monster.Monster.id != monster_id && !_monster.Monster.Monster.IsKnockedOut()))
                .ToDictionary(x => x.monster_id, x => x.is_selectable);
            this.PendingMoveTargetSelection = new MoveTargetSelection(
                MoveId: move_id, 
                IsMonsterIdSelectableDict: targets,
                OnSelection: _OnTargetSelection);
            this.BattleLogMessage = $"Choose a target for {move.move.name}";
            base.StateHasChanged();

            void _OnTargetSelection(BattleMonsterId target_id)
            {
                var target_position = this.CurrentBattlefieldState.TryGetMonsterFieldPosition(target_id);
                var move_action = MoveAction.Create(
                    caster_id: monster_id,
                    move_id: move_id,
                    target_team: null,
                    target_position: target_position);
                var (pending_swaps, pending_moves) = BattleService.RegisterMove(
                    token: this.Token,
                    move_action: move_action);
                this.UpdateRegistrations(
                    pending_moves: pending_moves, 
                    pending_swaps: pending_swaps,
                    shouldReset: true);
                base.StateHasChanged();
            }
        }
        
        private void OnSwapOutSelected(BattleMonsterId incoming_id)
        {
            var pending_swap_out = this.RequiredSwapPendingSwapOutId ?? this.UserChoicePendingSwapOutId;
            if (pending_swap_out == null)
            { 
                return; 
            }
            var swap_action = SwapAction.Create(
                outgoing: pending_swap_out.Value,
                incoming: incoming_id);
            var (pending_swaps, pending_moves) = BattleService.RegisterSwap(
                token: this.Token,
                swap_action: swap_action);
            this.UpdateRegistrations(
                pending_moves: pending_moves, 
                pending_swaps: pending_swaps,
                shouldReset: true);
            // this variable is reset here rather than in UpdateRegistrations 
            // because it should only be reset when the user made a choice when a swapout is required
            this.RequiredSwapPendingSwapOutId = null;
            base.StateHasChanged();
        }

        /// <summary>
        /// Sets the view models of field and party monsters using the given battlefield.
        /// Can be used to acknowledge a new battlefield state 
        /// or reset the flags of the view models of monsters with an old battlefield state.
        /// </summary>
        private void ResetMonsterViewModels(
            Battlefield battlefield)
        {
            // Either creating for the first time or updating the FieldMonsterOuterViewModels from the given battlefield.
            // Note that the number of monsters on the field from the given battlefield should never be lower than
            // the number of monsters in the dedicated FieldMonsters list.
            // This invariant matches the game logic.
            this.PlayerFieldMonsters = battlefield.field_monsters[this.Token.team_colour]
                .Select((_monster, _i) => new FieldMonsterOuterViewModel(
                    GameStateUpdator: this.PlayerFieldMonsters?.ElementAtOrDefault(_i)?.GameStateUpdator,
                    Monster: new MonsterViewModel(
                        Monster: _monster,
                        IsFoe: false)))
                .ToList();
            this.OpponentFieldMonsters = battlefield.field_monsters[TeamColour.GetOpposite(this.Token.team_colour)]
                .Select((_monster, _i) => new FieldMonsterOuterViewModel(
                    GameStateUpdator: this.OpponentFieldMonsters?.ElementAtOrDefault(_i)?.GameStateUpdator,
                    Monster: new MonsterViewModel(
                        Monster: _monster,
                        IsFoe: true)))
                .ToList();
            this.PlayerPartyMonsters = battlefield.party_monsters[this.Token.team_colour]
                .Select(_model => new MonsterViewModel(
                    Monster: _model,
                    IsFoe: false,
                    CanSwapIn: false,
                    Status: MonsterStatus.Idle))
                .ToList();
            foreach (var _fieldmonstervm in this.PlayerFieldMonsters.Concat(this.OpponentFieldMonsters))
            {
                _fieldmonstervm.GameStateUpdator?.Invoke(_fieldmonstervm.Monster);
            }
        }

        /// <summary>
        /// Eligible swap-ins will be inflated in this function if not provided.
        /// </summary>
        private void LoadSwapOut(
            BattleMonsterId swapping_out,
            bool is_choice,
            ImmutableArray<BattleMonsterId>? eligible_swapins = null)
        {
            this.UserChoicePendingSwapOutId = is_choice ? swapping_out : null;
            this.RequiredSwapPendingSwapOutId = is_choice ? null : swapping_out;
            eligible_swapins =  eligible_swapins ?? BattleService
                .GetEligibleIncomingMonstersForSwap(
                    token: this.Token,
                    swapping_out: swapping_out);
            this.PlayerPartyMonsters = this.PlayerPartyMonsters
                .Select(_monster => new MonsterViewModel(
                    Monster: _monster.Monster,
                    IsFoe: _monster.IsFoe,
                    Status: _monster.Status,
                    CanSwapIn: eligible_swapins.Contains(_monster.Monster.id)))
                .ToList();
        }

        private void UpdateRegistrations(
            ImmutableArray<MoveAction> pending_moves, 
            ImmutableArray<SwapAction> pending_swaps,
            bool shouldReset = false)
        {
            this.RegisteredSwapActions = pending_swaps
                .ToList();
            this.RegisteredMoveActions = pending_moves
                .ToList();
            if (shouldReset)
            {
                this.UserChoicePendingSwapOutId = null;
                this.PendingMoveTargetSelection = null;
                this.BattleLogMessage = BattlefieldPage.DEFAULT_WAITING_MESSAGE;
            }
            // both monsters have registered an action, automatically submit the turn choices
            var valid_field_monster_nb = this.PlayerFieldMonsters
                .Count(_monster => !_monster.Monster.Monster.IsKnockedOut());
            var nb_of_registered_actions = pending_moves.Length + pending_swaps.Length;
            if (nb_of_registered_actions == valid_field_monster_nb)
            {
                BattleService.LockRegistrations(this.Token);
            }
        }

    }
}
