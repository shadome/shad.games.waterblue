using System.Linq;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.data;
using System.Threading.Tasks;
using shad.games.waterblue.front.model;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using shad.games.waterblue.front.animations.moves;
using shad.games.waterblue.front.animations;
using shad.games.waterblue.lib;
using System;

namespace shad.games.waterblue.front.pages
{
    public partial class BattlefieldPage
    {
        private async Task AnimateDamagedByMove(
            BattleMonsterId target_id)
        { 
            var animation_info = Animations.GetDamagedByMoveAnimationInfo();
            this.UpdateMonsterStatus(
                monster_id: target_id,
                monster_status: MonsterStatus.DamagedByMove);
            await Task.Delay(animation_info.WaitingTimeInMs);
        }

        private async Task AnimateMove(
            Battlefield battlefield, 
            MoveAction move_action)
        {
            var caster = battlefield.field_monsters.Values
                .SelectMany(x => x)
                .First(_monster => _monster.id == move_action.caster_id);
            var move_name = caster.moveset
                .First(_move => _move.id == move_action.move_id)
                .move.name;
            var target_id = move_action.target_position == null
                ? (BattleMonsterId?)null
                : battlefield[move_action.target_position!.Value].id;
            var caster_position = this.GetBattlePositionFromId(move_action.caster_id);
            var target_position = this.GetBattlePositionFromId(target_id);
            var animation_info = move_name switch {
                MoveDictionary.THUNDERSHOCK_NAME => ThundershockMoveAnimation.GetMoveAnimationInfo(caster_position, target_position),
                MoveDictionary.THUNDER_NAME => ThunderMoveAnimation.GetMoveAnimationInfo(target_position),
                MoveDictionary.THUNDERBOLT_NAME => ThunderboltMoveAnimation.GetMoveAnimationInfo(caster_position, target_position),
                MoveDictionary.SHADOW_BALL_NAME => ShadowBallMoveAnimation.GetMoveAnimationInfo(caster_position, target_position),
                _ => new AnimationInfo(WaitingTimeInMs: 0, CenterAnimation: null, FullscreenAnimations: new())
            };
            this.CenterAnimation = animation_info.CenterAnimation;
            this.FullscreenAnimations = animation_info.FullscreenAnimations ?? new();
            base.StateHasChanged();
            await Task.Delay(animation_info.WaitingTimeInMs);
        }

        private async Task AnimateSwapOut(
            BattleMonsterId outgoing)
        {
            var monster_name = this.CurrentBattlefieldState[this.CurrentBattlefieldState.TryGetMonsterFieldPosition(outgoing)!.Value].monster.name;
            System.Console.WriteLine($"Animating swapout of {monster_name}");
            var animation_info = Animations.GetSwapOutAnimationInfo();
            this.UpdateMonsterStatus(
                monster_id: outgoing, 
                monster_status: MonsterStatus.SwappingOut);
            await Task.Delay(animation_info.WaitingTimeInMs);
        }

        private async Task AnimateSwapIn(
            BattleMonsterId incoming)
        {
            var animation_info = Animations.GetSwapInAnimationInfo();
            this.UpdateMonsterStatus(
                monster_id: incoming, 
                monster_status: MonsterStatus.SwappingIn);
            await Task.Delay(animation_info.WaitingTimeInMs);
        }

        private void ClearAnimations() 
        {
            this.CenterAnimation = null;
            this.FullscreenAnimations = new List<RenderFragment>();
        }

        // TODO move the following to Utils or something like that

        private BattlefieldPosition GetBattlePositionFromId(BattleMonsterId? id)
            => new(
                Top: this.OpponentFieldMonsters.Any(_monster => _monster.Monster.Monster.id == id),
                Left: id == this.OpponentFieldMonsters.ElementAtOrDefault(0)?.Monster.Monster.id
                    || id == this.PlayerFieldMonsters.ElementAtOrDefault(0)?.Monster.Monster.id);

        private void UpdateMonsterStatus(
            BattleMonsterId monster_id,
            MonsterStatus monster_status)
        {
            // index of the monster if it is in the player field monsters
            var index_of_player_field_monsters = this.PlayerFieldMonsters
                .IndexOf(_fieldmonstervm => _fieldmonstervm.Monster.Monster.id == monster_id);
            // otherwise, index of the monster if it is in the opponent field monsters
            var index_of_opponent_field_monsters = index_of_player_field_monsters > -1
                ? -1
                : this.OpponentFieldMonsters
                    .IndexOf(_fieldmonstervm => _fieldmonstervm.Monster.Monster.id == monster_id);
            // pointer towards the collection to update (using List dereferencing)
            var viewmodel_collection_ptr = index_of_player_field_monsters > -1
                ? this.PlayerFieldMonsters
                : this.OpponentFieldMonsters;
            var index = Math.Max(index_of_opponent_field_monsters, index_of_player_field_monsters);
            // update the MonsterViewModel
            var previous_field_monster_vm = viewmodel_collection_ptr[index];
            var new_monster_vm = new MonsterViewModel(
                Monster: previous_field_monster_vm.Monster.Monster,
                IsFoe: previous_field_monster_vm.Monster.IsFoe,
                Status: monster_status);
            viewmodel_collection_ptr[index] = new FieldMonsterOuterViewModel(
                GameStateUpdator: previous_field_monster_vm.GameStateUpdator,
                Monster: new_monster_vm);
            // notify the adequate sub-component with its GameStateUpdator
            previous_field_monster_vm.GameStateUpdator?.Invoke(new_monster_vm);
        }

    }
}
