using System.Collections.Generic;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.orchestration.services.objects;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.front.model;
using System;
using System.Collections.Immutable;

namespace shad.games.waterblue.front.pages
{
    public enum BattlefieldEventType
    {
        /// <summary>
        /// A swap is required by the backend. 
        /// Is handled through events because it should take action after ongoing events. 
        /// Do not use this event for user choosing to swap, it is not meant for this.
        /// </summary>
        RequiredSwap,
        UpdateMonsters,
        NewTurn,
        SwapInAction,
        SwapOutAction,
        MoveAction,
        BattleEnded,
    }

    public record BattlefieldEvent(
        BattlefieldEventType Type,
        Battlefield BattlefieldState,
        string Log,
        SwapAction? SwapAction = null,
        MoveActionLog? MoveActionLog = null,
        BattleMonsterId? RequriedSwapOutgoing = null,
        ImmutableArray<BattleMonsterId>? RequiredSwapEligibleReplacements = null);

    public record FieldMonsterOuterViewModel(
        // current MonsterViewModel value
        MonsterViewModel Monster,
        // subcomponent hook used to update its game state; null until it is first set, then forever not-null
        Action<MonsterViewModel>? GameStateUpdator);

    public partial class BattlefieldPage
    {
        private const string DEFAULT_WAITING_MESSAGE = "Select an action";
 
        private RenderFragment? CenterAnimation { get; set; } = null;

        private List<RenderFragment> FullscreenAnimations { get; set; } = new();

        private ClientToken Token { get; set; }

        private Battlefield CurrentBattlefieldState{ get; set; }

        private List<FieldMonsterOuterViewModel> PlayerFieldMonsters { get; set; } = new();

        private List<FieldMonsterOuterViewModel> OpponentFieldMonsters { get; set; } = new();

        private List<MonsterViewModel> PlayerPartyMonsters { get; set; } = new();

        /// <summary>
        /// Will display the swap modal if not null.
        /// Set a value if the user wants to swap monsters during they turn.
        /// </summary>
        private BattleMonsterId? UserChoicePendingSwapOutId { get; set; } = null;

        /// <summary>
        /// Will display the swap modal if not null.
        /// Has priority over UserChoice equivalent.
        /// Set a value if the backend forces the user to swap out a monster, e.g., when a knockout occurs.
        /// Must not be cancallable by the user.
        /// </summary>
        private BattleMonsterId? RequiredSwapPendingSwapOutId { get; set; } = null;

        private MoveTargetSelection? PendingMoveTargetSelection { get; set; } = null;

        private List<SwapAction> RegisteredSwapActions { get; set; } = new();

        private List<MoveAction> RegisteredMoveActions { get; set; } = new();

        private string BattleLogMessage { get; set; } = string.Empty;

        /// <summary>
        /// Side effect: set to true if the front end currently animates an event.
        /// </summary>
        public bool IsEventBeingProcessed { get; set; }

        /// <summary>
        /// Queue of events to be consumed by the front end in order to sequentially update the battlefield,
        /// its animations, its log messages, etc.
        /// </summary>
        public Queue<BattlefieldEvent> EventQueue { get; set; } = new();
    }
}
