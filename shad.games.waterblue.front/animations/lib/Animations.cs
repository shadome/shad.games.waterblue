using shad.games.waterblue.front.model;

namespace shad.games.waterblue.front.animations
{
    public static class Animations
    {
        public static AnimationInfo GetDamagedByMoveAnimationInfo()
        {
            return new(
                WaitingTimeInMs: 500,
                TargetClass: "animationloader-blink");
        }

        public static AnimationInfo GetSwapInAnimationInfo()
        {
            return new(
                WaitingTimeInMs: 500,
                CasterClass: "animationloader-swapin");
        }

        public static AnimationInfo GetSwapOutAnimationInfo()
        {
            return new(
                WaitingTimeInMs: 500,
                CasterClass: "animationloader-swapout");
        }

        public static AnimationInfo Default => new(WaitingTimeInMs: 0);
    }
}