using shad.games.waterblue.front.model;

namespace shad.games.waterblue.front.animations.lib
{
    public static class ProjectileAnimationHelper
    {
        public static ((int x, int y) from, (int x, int y) to) GetLineCoordinates(
            BattlefieldPosition caster_position,
            BattlefieldPosition target_position)
        {
            (int x, int y) from, to;
            var bottom_left = (x: 150, y: 300);
            var bottom_right = (x: 330, y: 330);
            var top_left = (x: 480, y: 210);
            var top_right = (x: 650, y: 240);
            var leftmost_position = (caster: caster_position, target: target_position) switch
            {
                (caster: (Top: true, Left: _), target: (Top: false, Left: _)) => target_position,
                (caster: (Top: false, Left: _), target: (Top: true, Left: _)) => caster_position,
                // at this point, top variables are equal, caster and target are in the same team
                // self-targetting should not be allowed
                (caster: (Top: _, Left: true), target: (Top: _, Left: _)) => caster_position,
                _ => target_position,
            };
            from = leftmost_position switch
            {
                (Top: false, Left: true) => bottom_left,
                (Top: false, Left: false) => bottom_right,
                (Top: true, Left: true) => top_left,
                // self-targetting should not be allowed => leftmost should not be top-right
                _ => (0, 0),
            };
            var rightmost_position = leftmost_position == caster_position
                ? target_position
                : caster_position;
            to = rightmost_position switch
            {
                (Top: true, Left: true) => top_left,
                (Top: true, Left: false) => top_right,
                (Top: false, Left: false) => bottom_right,
                // self-targetting should not be allowed => rightmost should not be bottom-left
                _ => (0, 0),
            };
            // updating the from and the to variables regarding the caster's position (left or right)
            var is_caster_left = caster_position == leftmost_position;
            if (!is_caster_left)
            {
                (from, to) = (to, from);
            }
            // caster's X is offset towards the center of the screen
            // to make an animation where it does not come out of its body
            var offset = 30;
            from.x = is_caster_left
                ? from.x + offset
                : from.x - offset;
            return (from, to);
        }
    }
}