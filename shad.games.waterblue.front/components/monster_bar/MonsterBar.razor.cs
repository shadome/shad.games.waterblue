﻿using System;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;

namespace shad.games.waterblue.front.components
{
    public partial class MonsterBar
    {
        [Parameter]
        public BattleMonster Monster { get; set; }

        [Parameter]
        public bool IsSwapSelected { get; set; }

        [Parameter]
        public BattleMoveId? SelectedMoveId { get; set; }

        [Parameter]
        public Action<BattleMonsterId, BattleMoveId> OnMoveSelected { get; set; } = delegate (BattleMonsterId _1, BattleMoveId _2) { }; // initialisation makes the compiler happy

        [Parameter]
        public Action<BattleMonsterId> OnSwapClicked { get; set; } = delegate (BattleMonsterId _) { };

        private int MonsterHealthPercent => (int)((double)this.Monster.health / this.Monster.monster.species.basestats.health * 100);

        private string MonsterIconPath => $"../res/cards/{this.Monster.monster.name}_256.png";

        private string MonsterBarWrapperClass => this.Monster.IsKnockedOut() ? "hidden" : string.Empty;
    }
}

