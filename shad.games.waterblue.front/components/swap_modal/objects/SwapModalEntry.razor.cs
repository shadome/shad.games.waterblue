﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.front.model;
using shad.games.waterblue.lib.TextUtils;

namespace shad.games.waterblue.front.components
{
    public partial class SwapModalEntry
    {
        [Parameter]
        public MonsterViewModel Monster { get; set; } = new(Monster: default, IsFoe: default);

        [Parameter]
        public Action<BattleMonsterId> OnSwapOutSelection { get; set; } = delegate(BattleMonsterId _) {};

        private Action<BattleMonsterId> OnEntryClick => this.Monster.CanSwapIn
            ? this.OnSwapOutSelection
            : delegate (BattleMonsterId _) { };

        private string SwapModalEntryClass_Selectable => this.Monster.CanSwapIn ? "swapmodalentry-selectable" : "swapmodalentry-unselectable";

        private string SwapModalEntryClass => $"swapmodalentry {this.SwapModalEntryClass_Selectable}";

        private string MonsterIconPath 
            => $"../res/cards/{this.Monster.Monster.monster.name}_256.png";

        private int MonsterHealthPercent 
            => (int)((double)this.Monster.Monster.health / this.Monster.Monster.monster.species.basestats.health * 100);

        private static string GetMoveIconPath(BattleMove move) 
            => $"../res/spells/{move.move.name.ToLower().Replace(' ', '_')}.png";

        private List<MoveComponentInfo> Moves 
            => this.Monster.Monster.moveset
                .Select(_move => new MoveComponentInfo(
                    Id: _move.id,
                    Name: _move.move.name.ToTitleCase(),
                    Path: SwapModalEntry.GetMoveIconPath(_move)))
                .ToList();
    }
}

