﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.front.model;

namespace shad.games.waterblue.front.components
{
    public partial class SwapModal
    {
        [Parameter]
        public bool IsVisible { get; set; } = false;
        
        [Parameter]
        public List<MonsterViewModel> Field { get; set; } = new();
        
        [Parameter]
        public List<MonsterViewModel> Party { get; set; } = new();

        [Parameter]
        public Action<BattleMonsterId> OnSwapOutSelection { get; set; } = delegate (BattleMonsterId _) { };

        private string SwapModalHiddenClass => this.IsVisible ? string.Empty : "swapmodal-hidden";

        private static string CardPath(BattleMonster battleMonster) 
            => $"../res/cards/{battleMonster.monster.name}_256.png";

        private static int Percent(BattleMonster monster) 
            => (int)((double)monster.health / monster.monster.species.basestats.health * 100);

    }
}

