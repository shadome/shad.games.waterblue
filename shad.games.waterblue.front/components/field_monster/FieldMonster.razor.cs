﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.front.animations;
using shad.games.waterblue.front.model;

namespace shad.games.waterblue.front.components
{

    public enum FieldMonsterEvent
    {
        SwappingIn,
        SwappingOut,
        DamagedByMove,
    }

    public partial class FieldMonster
    {
        private const string HIDDEN_CLASS = "hidden";

        /* Component parameters */

        /// <summary>
        /// The caller passes a function that this component will call on its OnInitialised hook.
        /// The passed function registers this component's method for updating its game state.
        /// </summary>
        [Parameter]
        public Action<Action<MonsterViewModel>> GameStateUpdatorRegistrationHook { get; set; } = delegate (Action<MonsterViewModel> _) { };

        [Parameter]
        public MonsterViewModel MonsterViewModelInitialValue { get; set; } = new(Monster: default, IsFoe: default);

        [Parameter]
        public BattlefieldPosition BattlefieldPosition { get; set; } = new(Top: false, Left: false);

        [Parameter]
        public bool IsUnselectable { get; set; } = false;

        [Parameter]
        public Action<BattleMonsterId> OnMonsterSelection { get; set; } = delegate(BattleMonsterId _) {}; // initialisation makes the compiler happy

        /* Component state */

        private bool IsOnBattlefield { get; set; } = false;

        private MonsterViewModel MonsterViewModel { get; set; } = new(Monster: default, IsFoe: default);

        private bool IsEventBeingProcessed { get; set; } = false;

        private Queue<FieldMonsterEvent> EventQueue { get; set; } = new();

        /* Component methods */

        protected override void OnInitialized()
        {
            base.OnInitialized();
            this.GameStateUpdatorRegistrationHook(this.GameStateUpdator);
            this.MonsterViewModel = this.MonsterViewModelInitialValue;
        }

        protected void GameStateUpdator(MonsterViewModel monster_view_model)
        {
            this.MonsterViewModel = monster_view_model;
            switch (monster_view_model.Status)
            {
                case MonsterStatus.SwappingIn:
                    this.EventQueue.Enqueue(FieldMonsterEvent.SwappingIn);
                    break;
                case MonsterStatus.SwappingOut:
                    this.EventQueue.Enqueue(FieldMonsterEvent.SwappingOut);
                    break;
                case MonsterStatus.DamagedByMove:
                    this.EventQueue.Enqueue(FieldMonsterEvent.DamagedByMove);
                    break;
            }
            // TODO OVERRIDE THE BOOLEAN FOR RE_RENDER
            base.StateHasChanged();
        }

        private async Task ProcessEvent()
        {
            if (this.IsEventBeingProcessed || !this.EventQueue.Any())
            {
                return;
            }
            this.IsEventBeingProcessed = true;
            var _event = this.EventQueue.Dequeue();
            // TODO this is a workaround / bugfix
            // for some reason, this component's OnParametersSet is called more than once with the same MonsterStatus
            // and fsr again handling duplicates in the OnParametersSet (which would be preferred) wasn't triggering any ProcessEvent() calls
            while (this.EventQueue.Any() && this.EventQueue.Peek() == _event)
            {
                _ = this.EventQueue.Dequeue();
            }
            var invalid_event = 
                (_event == FieldMonsterEvent.SwappingOut && !this.IsOnBattlefield)
                || (_event == FieldMonsterEvent.SwappingIn && this.IsOnBattlefield);
            if (invalid_event)
            {
                goto FINALLY;
            }
            var animation_info = _event switch
            {
                FieldMonsterEvent.DamagedByMove => Animations.GetDamagedByMoveAnimationInfo(),
                FieldMonsterEvent.SwappingIn => Animations.GetSwapInAnimationInfo(),
                FieldMonsterEvent.SwappingOut => Animations.GetSwapOutAnimationInfo(),
                _ => Animations.Default,
            };
            // TODO this is a hack, should make this generic
            this.MonsterSpriteImgClass1 = animation_info.CasterClass + animation_info.TargetClass;
            // momentarily set to true in order to wait for the animation to run without hiding the monster
            this.IsOnBattlefield = true;
            // Blazor automatic rendering should occur here
            await Task.Delay(animation_info.WaitingTimeInMs);
            this.MonsterSpriteImgClass1 = string.Empty;
            this.IsOnBattlefield = _event switch
            {
                FieldMonsterEvent.SwappingIn => true,
                FieldMonsterEvent.SwappingOut => false,
                _ => this.IsOnBattlefield,
            };
        FINALLY:
            this.IsEventBeingProcessed = false;
            // Blazor automatic rendering should occur here
            // should occur here but does not??
            base.StateHasChanged();
        }

        /* CSS variables */

        /// <summary>
        /// Global visible / hidden css class variable to add to elements that should not be displayed when the monster is not supposed to appear
        /// </summary>
        // private string HiddenMonsterClass { get; set; } = FieldMonster.HIDDEN_CLASS;
        private string HiddenMonsterClass => this.IsOnBattlefield ? string.Empty : FieldMonster.HIDDEN_CLASS;

        private string MonsterSpriteImgClass1 { get; set; } = string.Empty;

        private string MonsterSpriteImgClass => $"{this.MonsterSpriteImgClass1} {this.HiddenMonsterClass}";

        private string FieldMonsterDivClass1 => this.BattlefieldPosition switch
        {
            // (top, left)
            (true,  true)   => "bf_monster_foe_left",
            (true,  false)  => "bf_monster_foe_right",
            (false, true)   => "bf_monster_pal_left",
            (false, false)  => "bf_monster_pal_right",
        };

        private string FieldMonsterDivClass2 => this.IsUnselectable switch
        {
            true => "bf_monster-unselectable",
            false => string.Empty,
        };

        private string FieldMonsterDivClass => $"{this.FieldMonsterDivClass1} {this.FieldMonsterDivClass2}";

        private string HpBarDivClass1 => this.BattlefieldPosition switch
        {
            // (top, left)
            (false, _)      => FieldMonster.HIDDEN_CLASS,
            (true,  true)   => "bf_hpbar_foe_left",
            (true,  false)  => "bf_hpbar_foe_right",
        };

        private string HpBarDivClass => $"{this.HpBarDivClass1} {this.HiddenMonsterClass}";

        private string MonsterSpriteDivClass => this.MonsterViewModel.IsFoe switch
        {
            true => "monstersprite monstersprite_foe",
            false => "monstersprite monstersprite_pal",
        };

        private string MonsterSpriteImgSrc => this.MonsterViewModel.IsFoe switch
        {
            true => $"../res/sprites_monster_front/{this.MonsterViewModel.Monster.monster.species.name}.gif",
            false => $"../res/sprites_monster_back/{this.MonsterViewModel.Monster.monster.species.name}.gif",
            //true => $"https://projectpokemon.org/images/normal-sprite/{this.BattleMonster.monster.monster.species.name}.gif",
        };
    }
}

