using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace shad.games.waterblue.front
{
    /*
     * TODOLIST
     * Use the state controller to udpate the log messages
     * Animate swaps (minimize sprites)
     * Implement swap choice through modal
     * Rework BattleLog, with battlefield_before and battlefield_after and with swap_action
     * CollectDeadBodies should occur after OnEndOfTurn callbacks-wise => input registration should start after that too
     */
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            await builder.Build().RunAsync();
        }
    }
}
