using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using shad.games.waterblue.battlecore.model;

namespace shad.games.waterblue.front.model
{
    public enum MonsterStatus
    {
        Idle,
        SwappingIn,
        SwappingOut,
        DamagedByMove,

    }
    public record MoveComponentInfo(
        BattleMoveId Id, 
        string Name, 
        string Path);

    public record MoveTargetSelection(
        BattleMoveId MoveId,
        Dictionary<BattleMonsterId, bool> IsMonsterIdSelectableDict,
        Action<BattleMonsterId> OnSelection);
        

    public record BattlefieldPosition(
        bool Top, 
        bool Left);

    public record AnimationInfo(
        int WaitingTimeInMs,
        RenderFragment? CenterAnimation = null, 
        List<RenderFragment>? FullscreenAnimations = null,
        string CasterClass = "",
        string TargetClass = "");

    public record MonsterViewModel(
        BattleMonster Monster,
        bool IsFoe,
        MonsterStatus Status = MonsterStatus.Idle,
        bool CanSwapIn = false);

}