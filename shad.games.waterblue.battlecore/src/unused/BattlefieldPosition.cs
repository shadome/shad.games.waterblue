﻿// using System;

// namespace shad.games.waterblue.battlecore.model
// {

//     // Technical note:
//     // This implementation ensures more compile-time safety than enums. 
//     // In C#, the compiler does not prevent a user to cast a value that is not
//     // part of an enum to the enum type, e.g., `(BattlefieldPosition)42`.
//     // Make sure:
//     // - to keep MaxValue and other redundant fields up to date,
//     // - that provided static fields' `value` starts at 0 and 
//     // increases one by one,
//     // - that `default(T)` equals one of the static fields.

//     /// <summary>
//     /// In-battle or in-world battlefield positions.
//     /// </summary>
//     public readonly struct BattlefieldPosition
//     {
//         // Keep up to date!
//         public static BattlefieldPosition Parse(int i) => i switch 
//         {
//             0 => BattlefieldPosition.Left,
//             1 => BattlefieldPosition.Right,
//             _ => throw new ArgumentOutOfRangeException(
//                 paramName: nameof(i), 
//                 message: $"expected value between {BattlefieldPosition.Left.value} and {BattlefieldPosition.MaxValue}."),
//         };
//         // Keep up to date!
//         public static readonly byte MaxValue = 1;

//         // Keep up to date!
//         public static readonly BattlefieldPosition Left = new(0);

//         // Keep up to date!
//         public static readonly BattlefieldPosition Right = new(1);

//         public readonly byte value;

//         private BattlefieldPosition(
//             byte value)
//         {
//             this.value = value;
//         }
//     }
// }
