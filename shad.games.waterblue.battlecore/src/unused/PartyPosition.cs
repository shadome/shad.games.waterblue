﻿// using System;

// namespace shad.games.waterblue.battlecore.model
// {
//     // Dev note:
//     // This implementation ensures more compile-time safety than enums. 
//     // In C#, the compiler does not prevent a user to cast a value that is not
//     // part of an enum to the enum type, e.g., `(PartyPosition)42`.
//     // Make sure:
//     // - to keep MaxValue and other redundant fields up to date,
//     // - that provided static fields' `value` starts at 0 and 
//     // increases one by one,
//     // - that `default(T)` equals one of the static fields.

//     /// <summary>
//     /// In-battle or in-world team positions.
//     /// </summary>
//     public readonly struct PartyPosition
//     {
//         // Keep up to date!
//         public static PartyPosition Parse(int i) => i switch {
//             0 => PartyPosition.First,
//             1 => PartyPosition.Second,
//             2 => PartyPosition.Third,
//             3 => PartyPosition.Fourth,
//             _ => throw new ArgumentOutOfRangeException(
//                 paramName: nameof(i), 
//                 message: $"expected value between {PartyPosition.First.value} and {PartyPosition.MaxValue}."),
//         };
        
//         // Keep up to date!
//         public static readonly byte MaxValue = 3; 

//         // Keep up to date!
//         public static readonly PartyPosition First = new(0);

//         // Keep up to date!
//         public static readonly PartyPosition Second = new(1);

//         // Keep up to date!
//         public static readonly PartyPosition Third = new(2);

//         // Keep up to date!
//         public static readonly PartyPosition Fourth = new(3);

//         public readonly byte value;

//         private PartyPosition(
//             byte value)
//         {
//             this.value = value;
//         }
//     }
// }
