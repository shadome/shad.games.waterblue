﻿namespace shad.games.waterblue.battlecore.model
{
            // TODO somehow should be different between 
            //      StatModifiers (which can have negative values) and
            //      monster stats (which must be positive)
    public readonly struct Stats
    {
        // TODO 4 no more health point stat, all monsters have the same health pool, which is altered differently depending on the following stats (usually defensive stats)
        // TODO 4 'memory' for moveset size (i.e., 3 to 5)

        // TODO 4 stamina? vitality?
        public readonly short health;

        // TODO 4 strength?
        // used for calculating direct damage dealt
        public readonly short attack;

        // TODO 4 armour?
        // used for calculating direct damage received
        public readonly short defense;

        // TODO 4 will? potency? use for calculating used support move power, or part of the offensive magic moves (distinction impact/magic)?
        // used for calculating special damage dealt
        public readonly short specialattack;

        // TODO 4 spirit? use for calculating received support move effectiveness? end of turn regeneration?
        // used for calculating special damage received
        public readonly short specialdefense;

        // TODO 4 agility? quickness? use for calculating critical strike chance and glancing strike change? like, 100 speed vs 50 speed = 50% chance of crit/glancing?
        // used for calculating move priority on the battlefield and chances of fleeing successfully in the world
        public readonly short speed;

        #region creators

        private Stats(
            short health,
            short attack,
            short defense,
            short specialattack,
            short specialdefense,
            short speed)
        {
            this.health = health;
            this.attack = attack;
            this.defense = defense;
            this.specialattack = specialattack;
            this.specialdefense = specialdefense;
            this.speed = speed;
        }

        public static Stats Create(
            short health,
            short attack,
            short defense,
            short specialattack,
            short specialdefense,
            short speed)
        {
            // TODO somehow should be different between 
            //      StatModifiers (which can have negative values) and
            //      monster stats (which must be positive)
            var stats = new Stats(
                health: health,
                attack: attack,
                defense: defense,
                specialattack: specialattack,
                specialdefense: specialdefense,
                speed: speed);
            return stats;
        }

        #endregion

    }
}