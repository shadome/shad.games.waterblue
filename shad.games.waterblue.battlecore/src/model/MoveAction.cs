﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct MoveAction
    {
        /// <summary>
        /// Identifier of the casting battle monster.
        /// </summary>
        public readonly BattleMonsterId caster_id;

        /// <summary>
        /// Identifier of the battle move to cast.
        /// </summary>
        public readonly BattleMoveId move_id;

        /// <summary>
        /// Monster targetted by the move. <br/>
        /// Provided target_id will be ignored when unnecessary, e.g., no 
        /// target_id is needed if the move's MoveTarget is not 
        /// "ChoiceXxx". <br/>
        /// See the MoveTargetType enum for more information.
        /// </summary>
        public readonly FieldPosition? target_position;

        /// <summary>
        /// Team targetted by the move. <br/>
        /// Provided team_target will be ignored when unnecessary. <br/>
        /// In most cases, this value can be set to null. </br>
        /// See the MoveTargetType enum for more information. </br>
        /// Note that a target_team but no target_position may be required
        /// for certain moves, so using target_position.team does not cover
        /// all cases.
        /// </summary>
        public readonly TeamColour? target_team;

        #region creators

        private MoveAction(
            BattleMoveId move_id,
            BattleMonsterId caster_id,
            TeamColour? target_team,
            FieldPosition? target_position)
        {
            this.move_id = move_id;
            this.caster_id = caster_id;
            this.target_team = target_team;
            this.target_position = target_position;
        }

        public static MoveAction Create(
            BattleMonsterId caster_id,
            BattleMoveId move_id,
            TeamColour? target_team = null,
            FieldPosition? target_position = null)
        {
            var move_action = new MoveAction(
                move_id: move_id,
                caster_id: caster_id,
                target_team: target_team,
                target_position: target_position);
            return move_action;
        }

        #endregion

    }
}