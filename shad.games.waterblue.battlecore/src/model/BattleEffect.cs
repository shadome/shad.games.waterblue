﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly partial struct BattleEffect
    {
        // TODO ORC calculte real-time priority effect after effect using battlecore services (monster speed, etc.)
        // priority is an orchestration decision, based on battlecore calculations

        public readonly BattleEffectId id;

        /// <summary>
        /// Priority of the effect, calculated from the turn during which it has been applied and then the priority of the move.
        /// </summary>
        // public readonly long priority;

        /// <summary>
        /// Iteration at which this effect has been applied.
        /// </summary>
        public readonly int iteration;

        /// <summary>
        /// Priority of the effect.
        /// </summary>
        public readonly long priority;

        /// <summary>
        /// Pointer towards the caster.<br/>
        /// The caster can be either a static monster image from a previous turn or
        /// the current state of a monster present on the battlefield at this turn.<br/>
        /// Can be set to null if the effect does not need caster data (e.g., rain).
        /// </summary>
        //public readonly Func<Battle, BattleMonster> caster;

        // Dev's note: Func to make it live on the heap to avoid circular 
        // reference which makes runtime fail with BattleMonster having an 
        // array of BattleEffects having each a BattleMonster caster.
        // TODO: use battlefield.iteration + create a new "point in state" 
        // battlefield integer and store in a battle each turn and each
        // successive battlefield state in the turn.
        public readonly Func<BattleMonster> caster_copy;

        // TODO 1 is it needed? the battleeffect will either be battlefield-wide or discrete over a specific monster
        //public readonly ImmutableHashSet<TeamPositionAbsolute> targets;
        /// <summary>
        /// Pointer towards the target.<br/>
        /// Set to null if the effect has no target (e.g., battlefield-wide).<br/>
        /// Note: effects with more than one target translate into as many battle effects as there are targets.
        /// </summary>
        //public readonly BattlePosition? target;

        /// <summary>
        /// Number of turns during which the effect remains active.<br/>
        /// Set to 0 for an instant effect without debuff.<br/>
        /// Set to null for an everlasting effect.<br/>
        /// An effect can still last less than this number of turns
        /// if its persistence is broken or if it is modified by another effect.
        /// </summary>
        // public readonly byte? remaining;

        /// <summary>
        /// Number of turns before the registered effect triggers.
        /// </summary>
        // public readonly byte waiting;

        // Dev's note: readonly struct should enforce flyweight design pattern
        public readonly Effect effect;

        #region creators

        private BattleEffect(
            BattleEffectId id,
            int iteration,
            Func<BattleMonster> caster_copy,
            Effect effect)
        {
            this.id = id;
            this.iteration = iteration;
            this.priority = DateTime.Now.Ticks;
            this.caster_copy = caster_copy;
            this.effect = effect;
        }

        internal static BattleEffect Create(
            Effect effect,
            Func<BattleMonster> caster_copy,
            int battlefield_iteration)
        {
            var id = BattleEffectId.Create();
            var battle_effect = new BattleEffect(
                id: id,
                iteration: battlefield_iteration,
                caster_copy: caster_copy,
                effect: effect);
            return battle_effect;
        }

        #endregion

    }
}