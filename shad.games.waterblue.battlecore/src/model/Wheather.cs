﻿namespace shad.games.waterblue.battlecore.model
{
    // Dev's note: using a struct with static provided fields and no 
    // constructors allows compile-time safety for the caller.
    // Encapsulating an enum allows the use of the switch syntaxes.
    // Be sure to handle the struct's default constructor as a non-critical
    // business logic value.

    public readonly struct Weather
    {
        public static readonly Weather None = new(WeatherEnum.None);

        public static readonly Weather Sun = new(WeatherEnum.Sun);

        public static readonly Weather Rain = new(WeatherEnum.Rain);

        public static readonly Weather Hail = new(WeatherEnum.Hail);

        public static readonly Weather Sandstorm = new(WeatherEnum.Sandstorm);
        
        #region creators / internal

        internal readonly WeatherEnum value;

        private Weather(
            WeatherEnum value)
        {
            this.value = value;
        }

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is Weather other) && other == this;

        public override int GetHashCode()
            => (byte)this.value;

        public static bool operator ==(Weather left, Weather right)
            => left.value == right.value;

        public static bool operator !=(Weather left, Weather right)
            => !(left == right);

        #endregion

    }
}
