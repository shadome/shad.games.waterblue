﻿namespace shad.games.waterblue.battlecore.model
{
    public enum EffectRecurrence : byte
    {
        /// <summary>
        /// Instant effect => becomes nullable
        /// </summary>
        // None,
        Preturn,
        // Midturn,
        Postturn,
    }
}
