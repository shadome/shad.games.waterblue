﻿namespace shad.games.waterblue.battlecore.model
{
    // Dev's note: using a struct with static provided fields and no 
    // constructors allows compile-time safety for the caller.
    // Encapsulating an enum allows the use of the switch syntaxes.
    // Be sure to handle the struct's default constructor as a non-critical
    // business logic value.

    /// <summary>
    /// Possible targets for a move. <br/>
    /// Semantics: 'fixed' means the player does not have a choice,
    /// 'choice' means the player must choose.
    /// </summary>
    public readonly partial struct Type
    {
        public static readonly Type Bug = new(TypeEnum.Bug);

        public static readonly Type Dark = new(TypeEnum.Dark);

        public static readonly Type Dragon = new(TypeEnum.Dragon);

        public static readonly Type Electric = new(TypeEnum.Electric);

        public static readonly Type Fightning = new(TypeEnum.Fightning);
        
        public static readonly Type Fire = new(TypeEnum.Fire);

        public static readonly Type Flying = new(TypeEnum.Flying);

        public static readonly Type Ghost = new(TypeEnum.Ghost);

        public static readonly Type Grass = new(TypeEnum.Grass);

        public static readonly Type Ground = new(TypeEnum.Ground);

        public static readonly Type Ice = new(TypeEnum.Ice);

        public static readonly Type Normal = new(TypeEnum.Normal);

        public static readonly Type Poison = new(TypeEnum.Poison);

        public static readonly Type Psychic = new(TypeEnum.Psychic);

        public static readonly Type Rock = new(TypeEnum.Rock);

        public static readonly Type Steel = new(TypeEnum.Steel);

        public static readonly Type Water = new(TypeEnum.Water);

        #region creators / internal

        internal readonly TypeEnum value;

        private Type(
            TypeEnum value)
        {
            this.value = value;
        }

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is Type other) && other == this;

        public override int GetHashCode()
            => (byte)this.value;

        public static bool operator ==(Type left, Type right)
            => left.value == right.value;

        public static bool operator !=(Type left, Type right)
            => !(left == right);

        #endregion

    }
}
