﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Encloses a move found in a moveset of a battling monster.
    /// </summary>
    public readonly struct BattleMove
    {
        // Dev note: this abstraction will be used to add functionalities
        // such as 'moves in cooldown', 'moves in warmup', etc.

        public readonly BattleMoveId id;

        // Dev's note: readonly struct should enforce flyweight design pattern
        public readonly Move move;

        #region creators

        private BattleMove(
            BattleMoveId id,
            Move move)
        {
            this.id = id;
            this.move = move;
        }

        internal static BattleMove Create(
            BattleMoveId id,
            Move move)
        {
            var battle_move = new BattleMove(
                id: id,
                move: move);
            return battle_move;
        }

        #endregion

    }
}