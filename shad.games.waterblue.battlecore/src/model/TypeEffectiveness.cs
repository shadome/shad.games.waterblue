﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    // Dev's note: using a struct with static provided fields and no 
    // constructors allows compile-time safety for the caller.
    // Encapsulating an enum allows the use of the switch syntaxes.
    // Be sure to handle the struct's default constructor as a non-critical
    // business logic value.

    /// <summary>
    /// Possible targets for a move. <br/>
    /// Semantics: 'fixed' means the player does not have a choice,
    /// 'choice' means the player must choose.
    /// </summary>
    public readonly struct TypeEffectiveness
    {
        public static readonly TypeEffectiveness Neutral = new(TypeEffectivenessEnum.Neutral);
        
        public static readonly TypeEffectiveness Effective = new(TypeEffectivenessEnum.Effective);

        public static readonly TypeEffectiveness Resistant = new(TypeEffectivenessEnum.Resistant);

        public static readonly TypeEffectiveness Immune = new(TypeEffectivenessEnum.Immune);

        #region creators / internal

        internal readonly TypeEffectivenessEnum value;

        internal readonly byte stacks;

        private TypeEffectiveness(
            TypeEffectivenessEnum value,
            byte stacks = 1)
        {
            this.value = value;
            this.stacks = stacks;
        }

        #endregion

        #region overrides

        // Note that this operator does NOT compare the 'stacks' field.
        public override bool Equals(object? obj)
            => (obj is TypeEffectiveness other) && other == this;

        public override int GetHashCode()
            => (byte)this.value;

        // Note that this operator does NOT compare the 'stacks' field.
        public static bool operator ==(TypeEffectiveness left, TypeEffectiveness right)
            => left.value == right.value;

        // Note that this operator does NOT compare the 'stacks' field.
        public static bool operator !=(TypeEffectiveness left, TypeEffectiveness right)
            => !(left == right);

        public static TypeEffectiveness operator +(TypeEffectiveness left, TypeEffectiveness right)
        {
            var left_byte = TypeEffectiveness.GetByteValue(left);
            var right_byte = TypeEffectiveness.GetByteValue(right);
            return 
                left_byte == null || right_byte == null  ? TypeEffectiveness.Immune :
                left_byte!.Value + right_byte!.Value > 0 ? new(TypeEffectivenessEnum.Effective, (byte)Math.Abs(left_byte!.Value + right_byte!.Value)) :
                left_byte!.Value + right_byte!.Value < 0 ? new(TypeEffectivenessEnum.Resistant, (byte)Math.Abs(left_byte!.Value + right_byte!.Value)) :
                TypeEffectiveness.Neutral;
        }

        private static sbyte? GetByteValue(TypeEffectiveness type_effectiveness)
            => type_effectiveness.value switch
            {
                TypeEffectivenessEnum.Immune    => null,
                TypeEffectivenessEnum.Resistant => -1,
                TypeEffectivenessEnum.Neutral   => 0,
                TypeEffectivenessEnum.Effective => 1,
                _ => throw new System.Exception(),
            };

        #endregion

    }
}
