﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Flag for effects having an immediate impact on the battlefield 
    /// e.g., direct damage, dispel, heal, etc., as opposed to debuffs,
    /// e.g., damage over time, etc.
    /// On resolution, all components relevant to immediate effects 
    /// will be taken into account.
    /// This component does not prevent the effet to also have an over-time
    /// effect that would also read parameter components such as damage,
    /// healing, etc. 
    /// </summary>
    public readonly struct ImmediateEffectComponent : IComponent
    {
        public static IComponent Create()
        {
            return new ImmediateEffectComponent();
        }
    }
}
