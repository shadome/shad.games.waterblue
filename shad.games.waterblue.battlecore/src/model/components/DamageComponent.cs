﻿
namespace shad.games.waterblue.battlecore.model
{
    public readonly struct DamageComponent : IComponent
    {
        public readonly Type type;

        public readonly byte power;

        private DamageComponent(
            Type type,
            byte power)
        {
            this.type = type;
            this.power = power;
        }

        public static IComponent Create(
            Type type,
            byte power)
        {
            return new DamageComponent(
                type: type, 
                power: power);
        }
    }
}
