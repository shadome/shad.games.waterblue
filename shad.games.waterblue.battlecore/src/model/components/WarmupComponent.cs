﻿namespace shad.games.waterblue.battlecore.model
{
    public readonly struct WarmupComponent : IComponent
    {
        public readonly byte warmup;

        public readonly byte waiting;

        private WarmupComponent(
            byte warmup,
            byte waiting)
        {
            this.warmup = warmup;
            this.waiting = waiting;
        }

        public static IComponent Create(
            byte warmup)
        {
            return new WarmupComponent(
                warmup: warmup,
                waiting: warmup);
        }

        internal static WarmupComponent Decrease(
            WarmupComponent component) 
        {
            return new WarmupComponent(
                warmup: component.warmup, 
                waiting: (byte)(component.waiting - 1));
        }
    }
}
