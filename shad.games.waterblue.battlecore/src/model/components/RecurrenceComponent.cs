﻿namespace shad.games.waterblue.battlecore.model
{
    public readonly struct RecurrenceComponent : IComponent
    {
        public readonly EffectRecurrence recurrence;

        private RecurrenceComponent(
            EffectRecurrence recurrence)
        {
            this.recurrence = recurrence;
        }

        public static IComponent Create(
            EffectRecurrence recurrence)
        {
            return new RecurrenceComponent(
                recurrence: recurrence);
        }
    }
}
