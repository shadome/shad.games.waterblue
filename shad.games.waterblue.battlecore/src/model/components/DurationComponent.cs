﻿namespace shad.games.waterblue.battlecore.model
{
    public readonly struct DurationComponent : IComponent
    {
        public readonly byte duration;

        public readonly byte remaining;

        private DurationComponent(
            byte duration,
            byte remaining)
        {
            this.duration = duration;
            this.remaining = remaining;
        }

        public static IComponent Create(
            byte duration)
        {
            return new DurationComponent(
                duration: duration,
                remaining: duration);
        }

        internal static DurationComponent Decrease(
            DurationComponent component) 
        {
            return new DurationComponent(
                duration: component.duration, 
                remaining: (byte)(component.remaining - 1));
        }
    }
}
