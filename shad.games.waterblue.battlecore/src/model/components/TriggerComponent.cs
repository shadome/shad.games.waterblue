﻿namespace shad.games.waterblue.battlecore.model
{
    public readonly struct TriggerComponent : IComponent
    {
        public static IComponent Create()
        {
            return new TriggerComponent();
        }
    }
}
