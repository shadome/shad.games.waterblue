﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Attach to debuff effects sticking to a monster. 
    /// Sticks to the target if it is swapping.
    /// </summary>
    public readonly struct TargetMonsterComponent : IComponent
    {
        public readonly BattleMonsterId target_id;

        private TargetMonsterComponent(
            BattleMonsterId target_id)
        {
            this.target_id = target_id;
        }

        public static IComponent Create()
        {
            return new TargetMonsterComponent();
        }

        /// <summary>
        /// Set the definitive target for this component. Will only work once.
        /// </summary>
        internal static TargetMonsterComponent SetTarget(
            TargetMonsterComponent component,
            BattleMonsterId target_id)
        {
            return component.target_id == default
                ? new TargetMonsterComponent(target_id: target_id)
                : component;
        }
    }
}
