﻿namespace shad.games.waterblue.battlecore.model
{

    // Technical note:
    // This implementation ensures more compile-time safety than enums. 
    // In C#, the compiler does not prevent a user to cast a value that is not
    // part of an enum to the enum type, e.g., `(PartyPosition)42`.
    // Make sure:
    // - to keep MaxValue and other redundant fields up to date,
    // - that provided static fields' `value` starts at 0 and 
    // increases one by one,
    // - that `default(T)` equals one of the static fields.

    /// <summary>
    /// Team colour used to differentiate battling players/robots.
    /// </summary>
    public readonly partial struct TeamColour
    {
        // Keep up to date!
        public static readonly TeamColour Blue = new(TeamColourEnum.Blue);

        // Keep up to date!
        public static readonly TeamColour Grey = new(TeamColourEnum.Grey);

        #region creators / internal

        internal readonly TeamColourEnum value;

        private TeamColour(
            TeamColourEnum value)
        {
            this.value = value;
        }

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is TeamColour other) && other.value == this.value;

        public override int GetHashCode()
            => (byte)this.value;

        public static bool operator ==(TeamColour left, TeamColour right)
            => left.value == right.value;

        public static bool operator !=(TeamColour left, TeamColour right) 
            => left.value != right.value;

        public override string ToString()
            => this.value.ToString();

        #endregion

    }
}
