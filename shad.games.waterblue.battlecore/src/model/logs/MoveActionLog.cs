﻿using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Log describing how the health of a monster has been impacted by a move, an effect, etc.</br>
    /// Can describe the outcome of a damaging move for its target, the lifesteal or recoil of a move on its caster, etc.
    /// </summary>
    public readonly struct MoveActionLog
    {
        public readonly MoveAction move_action;

        public readonly Battlefield battlefield_before;

        public readonly Battlefield battlefield_after;

        public readonly ImmutableArray<MitigationLog> mitigation_logs;

        private MoveActionLog(
            MoveAction move_action,
            Battlefield battlefield_before,
            Battlefield battlefield_after,
            ImmutableArray<MitigationLog> mitigation_logs)
        {
            this.move_action = move_action;
            this.battlefield_before = battlefield_before;
            this.battlefield_after = battlefield_after;
            this.mitigation_logs = mitigation_logs;
        }

        internal static MoveActionLog Create(
            MoveAction move_action,
            Battlefield battlefield_before)
        {
            return new(
                move_action: move_action,
                battlefield_before: battlefield_before,
                battlefield_after: battlefield_before,
                mitigation_logs: ImmutableArray<MitigationLog>.Empty);
        }

        internal static MoveActionLog AddMitigationLog(
            MoveActionLog previous,
            MitigationLog mitigation_log,
            Battlefield updated_battlefield)
        {
            var updated_mitigation_logs = previous.mitigation_logs.Add(mitigation_log);
            return new(
                move_action: previous.move_action,
                battlefield_before: previous.battlefield_before,
                battlefield_after: updated_battlefield,
                mitigation_logs: updated_mitigation_logs);
        }

        internal static MoveActionLog UpdateBattlefieldAfter(
            MoveActionLog previous,
            Battlefield updated_battlefield)
        {
            return new(
                move_action: previous.move_action,
                battlefield_before: previous.battlefield_before,
                battlefield_after: updated_battlefield,
                mitigation_logs: previous.mitigation_logs);
        }            
    }
}
