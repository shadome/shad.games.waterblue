﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Log describing how the health of a monster has been impacted by a move, an effect, etc.</br>
    /// Can describe the outcome of a damaging move for its target, the lifesteal or recoil of a move on its caster, etc.
    /// </summary>
    public readonly struct MitigationLog
    {
        /// <summary>
        /// Identifier of the monster on which this mitigation has been calculated / applied.
        /// </summary>
        public readonly BattleMonsterId monster_id;

        /// <summary>
        /// Type effectiveness of the mitigation, e.g., water is effective on fire.
        /// </summary>
        public readonly TypeEffectiveness type_effectiveness;

        /// <summary>
        /// Move action's consequence: positive for healing, negative for damage.
        /// </summary>
        public readonly short mitigation;

        /// <summary>
        /// Amount of the mitigation that has been absored by the target, i.e., 
        /// healing that was denied or damage that was absorbed by a shield.<br/>
        /// Always positive, even though mitigation is negative for damage.
        /// Always smaller that mitigation in absolute value.
        /// </summary>
        public readonly short mitigation_absorbed;

        /// <summary>
        /// Amount of mitigation which was uneffective, e.g., overkill, overheal, etc.<br/>
        /// Always positive, even though mitigation is negative for damage.<br/>
        /// Always smaller that mitigation in absolute value.
        /// </summary>
        public readonly short mitigation_overflow;

        /// <summary>
        /// Flag set to true if the target has been knocked out by the mitigation action.
        /// </summary>
        public readonly bool has_knocked_out_target;

        private MitigationLog(
            BattleMonsterId monster_id,
            TypeEffectiveness type_effectiveness,
            short mitigation,
            short mitigation_absorbed,
            short mitigation_overflow,
            bool has_knocked_out_target)
        {
            this.monster_id = monster_id;
            this.type_effectiveness = type_effectiveness;
            this.mitigation = mitigation;
            this.mitigation_absorbed = mitigation_absorbed;
            this.mitigation_overflow = mitigation_overflow;
            this.has_knocked_out_target = has_knocked_out_target;
        }

        internal static MitigationLog Create(
            BattleMonsterId monster_id,
            TypeEffectiveness type_effectiveness,
            short mitigation,
            short mitigation_absorbed,
            short mitigation_overflow,
            bool has_knocked_out_target)
        {
            return new(
                monster_id: monster_id,
                type_effectiveness: type_effectiveness,
                mitigation: mitigation,
                mitigation_absorbed: mitigation_absorbed,
                mitigation_overflow: mitigation_overflow,
                has_knocked_out_target: has_knocked_out_target);
        }
    }
}
