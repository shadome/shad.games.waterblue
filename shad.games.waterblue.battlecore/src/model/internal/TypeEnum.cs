﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Internal enum to enable switch syntax.<br/>
    /// Should match Type values.
    /// </summary>
    internal enum TypeEnum : byte
    {
        Normal = 0,
        Bug,
        Dark,
        Dragon,
        Electric,
        Fightning,
        Fire,
        Flying,
        Ghost,
        Grass,
        Ground,
        Ice,
        Poison,
        Psychic,
        Rock,
        Steel,
        Water,
    }
}
