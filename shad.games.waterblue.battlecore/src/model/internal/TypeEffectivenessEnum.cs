﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Internal enum to enable switch syntax.<br/>
    /// Should match Type values.
    /// </summary>
    internal enum TypeEffectivenessEnum : byte
    {
        Neutral = 0,
        Effective,
        Resistant,
        Immune,
    }
}
