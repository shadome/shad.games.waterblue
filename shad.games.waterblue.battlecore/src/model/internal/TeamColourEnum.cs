﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Internal enum to enable switch syntax.<br/>
    /// Should match TeamColour values.
    /// </summary>
    internal enum TeamColourEnum : byte
    {
        Blue = 0,

        Grey,
    }
}
