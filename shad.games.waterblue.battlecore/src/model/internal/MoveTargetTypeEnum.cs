﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Internal enum to enable switch syntax.<br/>
    /// Should match MoveTargetType values.
    /// </summary>
    internal enum MoveTargetTypeEnum : byte
    {
        NotApplicable = 0,
        // ChoiceBattlefieldSingle,
        // ChoiceBattlefieldSide,
        // ChoiceBattlefieldSingleFoe,
        // ChoiceBattlefieldSinglePal,
        ChoiceBattlefieldSingleExceptCaster,
        FixedSelf,
        FixedBattlefieldSideOpponent,
        FixedBattlefieldAllExceptCaster,
        FixedBattlefieldAll,
        // FixedDirectPal,
        // FixedBattlefieldSidePlayer,
        // FixedPartyPlayer,
        // FixedPartyOpponent,
        // FixedAllPlayer,
        // FixedAllOpponent,
    }
}
