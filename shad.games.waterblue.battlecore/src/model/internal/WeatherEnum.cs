﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Internal enum to enable switch syntax.<br/>
    /// Should match Weather values.
    /// </summary>
    internal enum WeatherEnum : byte
    {
        None = 0,
        Sun,
        Rain,
        Hail,
        Sandstorm,
    }
}
