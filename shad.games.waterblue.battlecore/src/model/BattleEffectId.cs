﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct BattleEffectId
    {
        private readonly Guid id;

        #region creators

        private BattleEffectId(
            Guid id)
        {
            this.id = id;
        }

        internal static BattleEffectId Create()
            => new(Guid.NewGuid());

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is BattleEffectId other) && other == this;

        public override int GetHashCode()
            => this.id.GetHashCode();

        public static bool operator ==(BattleEffectId left, BattleEffectId right)
            => left.id == right.id;

        public static bool operator !=(BattleEffectId left, BattleEffectId right)
            => left.id != right.id;

        public override string ToString()
            => id.ToString();

        #endregion
    }
}
