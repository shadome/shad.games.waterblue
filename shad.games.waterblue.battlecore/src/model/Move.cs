﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct Move
    {
        // serves as main identifier
        public readonly string name;

        /// <summary>
        /// Eligibles targets of the move.
        /// </summary>
        public readonly MoveTargetType move_target_type;

        // TODO replace list with fixed-size number of fields in order to use the stack and not the heap, and throw compile-time errors rather than runtime errors.
        /// <summary>
        /// Ordered effect key list to apply with this move.
        /// </summary>
        public readonly ImmutableArray<Effect> effects;

        #region creators

        private Move(
            string name,
            MoveTargetType move_target_type,
            ImmutableArray<Effect> effects)
        {
            this.name = name;
            this.move_target_type = move_target_type;
            this.effects = effects;
        }

        public static Move Create(
            MoveTargetType move_target_type,
            ImmutableArray<Effect> effects,
            string name = "")
        {
            var move = new Move(
                name: name,
                move_target_type: move_target_type,
                effects: effects);
            return move;
        }

        #endregion

    }
}