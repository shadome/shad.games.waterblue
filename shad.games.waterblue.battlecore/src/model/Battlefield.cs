﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.model
{
    // TODO 4 split between battle effect wrapper and effect metadata
    public readonly partial struct Battlefield
    {
        public readonly ImmutableDictionary<BattleEffectId, ImmutableArray<IComponent>> effects_components;

        public readonly ImmutableArray<BattleEffect> battle_effects;

        public const byte FIELD_MAX_NB = 2;

        public const byte PARTY_MAX_NB = 4;

        /// <summary>
        /// Starting at 0, incremented after each turn.
        /// </summary>
        public readonly int iteration;

        /// <summary>
        /// Chronologically ordered list of actions 
        /// performed by BattlecoreService layer calls 
        /// that can be used for client display.
        /// </summary>
        //public readonly IImmutableList<DamageLog> logs;

        /// <summary>
        /// State of the monsters on the battlefield (but no in the parties).
        /// Note that KNOCKED OUT monsters WILL BE FOUND IN this list, 
        /// the client should not display them if unnecessary.
        /// Can only be null if some player's team is incomplete.
        /// Indexing: array[TeamColour][BattlefieldPosition]
        /// </summary>
        // public readonly ImmutableArray<BattleMonster?> battlefield_monsters;
        public readonly ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> field_monsters;
        
        /// <summary>
        /// State of the monsters in the parties (but no on the battlefield).
        /// Note that KNOCKED OUT monsters WILL BE FOUND IN this list, 
        /// the client should not display them if unnecessary.
        /// Can only be null if some player's team is incomplete.
        /// Indexing: array[TeamColour][PartyPosition]
        /// </summary>
        public readonly ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> party_monsters;
        // public readonly ImmutableArray<BattleMonster?> party_monsters;

        #region creators

        public readonly BattleMonster this[FieldPosition field_position]
            => this.field_monsters[field_position.team][field_position.offset];

        public readonly BattleMonster this[PartyPosition party_position]
            => this.party_monsters[party_position.team][party_position.offset];

        private Battlefield(
            int iteration,
            ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> field_monsters,
            ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> party_monsters,
            ImmutableDictionary<BattleEffectId, ImmutableArray<IComponent>> effects_components,
            ImmutableArray<BattleEffect> battle_effects)
        {
            this.iteration = iteration;
            this.field_monsters = field_monsters;
            this.party_monsters = party_monsters;
            this.effects_components = effects_components;
            this.battle_effects = battle_effects;
        }

        public static Battlefield Create(
            ImmutableArray<(TeamColour colour, ImmutableArray<BattleMonster> monsters)> teams)
        {
            _CheckInput(teams);
            var field_monsters = _ExtractFieldMonsters(teams);
            var party_monsters = _ExtractPartyMonsters(teams);
            var battlefield = new Battlefield(
                iteration: 0,
                field_monsters: field_monsters,
                party_monsters: party_monsters,
                effects_components: ImmutableDictionary.Create<BattleEffectId, ImmutableArray<IComponent>>(),
                battle_effects: ImmutableArray.Create<BattleEffect>());
            return battlefield;

            static ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> _ExtractFieldMonsters(
                ImmutableArray<(TeamColour colour, ImmutableArray<BattleMonster> monsters)> teams)
            {
                var mutable_field_dict = ImmutableDictionary.CreateBuilder<TeamColour, ImmutableArray<BattleMonster>>();
                foreach (var (colour, monsters) in teams)
                {
                    mutable_field_dict.Add(
                        key: colour,
                        value: monsters.Length > Battlefield.FIELD_MAX_NB
                            ? ImmutableArray.CreateRange(monsters
                                .AsSpan()[..Battlefield.FIELD_MAX_NB]
                                .ToArray())
                            : ImmutableArray.CreateRange(monsters));
                }
                return mutable_field_dict.ToImmutable();
            }

            static ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> _ExtractPartyMonsters(
                ImmutableArray<(TeamColour colour, ImmutableArray<BattleMonster> monsters)> teams)
            {
                var mutable_dict = ImmutableDictionary.CreateBuilder<TeamColour, ImmutableArray<BattleMonster>>();
                foreach (var (colour, monsters) in teams)
                {
                    mutable_dict.Add(
                        key: colour,
                        value: monsters.Length > Battlefield.FIELD_MAX_NB
                            ? ImmutableArray.CreateRange(monsters
                                .AsSpan()[Battlefield.FIELD_MAX_NB..Math.Min(Battlefield.FIELD_MAX_NB + Battlefield.PARTY_MAX_NB, monsters.Length)]
                                .ToArray())
                            : ImmutableArray<BattleMonster>.Empty);
                }
                return mutable_dict.ToImmutable();
            }

            static void _CheckInput(ImmutableArray<(TeamColour colour, ImmutableArray<BattleMonster> monsters)> teams)
            {
                if (teams.Length != TeamColour.GetValues().Length)
                {
                    throw new ArgumentException($"There should be {TeamColour.GetValues().Length} different team colours in {nameof(teams)}.");
                }
                if (teams.Any(_tuple => _tuple.monsters.IsDefaultOrEmpty || _tuple.monsters.Any(_bm => _bm.Equals(default))))
                {
                    throw new ArgumentException("Unexpected empty array or default value of BattleMonster");
                }
            }
        }

        #endregion

    }
}