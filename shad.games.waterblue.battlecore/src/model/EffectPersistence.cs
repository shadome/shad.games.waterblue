﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Whether a move effect is linked to a battlefield position, the caster, the target, etc.
    /// These values will be read in case a monster is KO or switches out in order to determine which effects
    /// should end before their remaining turns are set to 0.
    /// E.g. "target" means the effect should stop prematurely if the target is KO or has switched out.
    /// </summary>
    public enum EffectPersistence : byte
    {
        /// <summary>
        /// Effect persists on the targetted monster unconditionally, even if it swaps out.
        /// </summary>
        TargetSticky,

        /// <summary>
        /// Effect persists on the targetted battlefield position unconditionally.
        /// </summary>
        BattlefieldPosition,

        /// <summary>
        /// Effect persists on the targetted battlefield position unless the caster swaps out.
        /// </summary>
        CasterChanneling,

        // TODO 4 should this defined whether the caster is actively or passively channeling 
        // (i.e., can or cannot user other moves during the duration of the effect)?
        /// <summary>
        /// Effect persists on the targetted monster until it swaps out.
        /// </summary>
        TargetUntilSwap,

        /// <summary>
        /// Technical value. Cannot be used by a client for configuration.
        /// </summary>
        NotApplicable,

        //[Description("until_caster_or_target_switch")]
        //CasterAndTargetUntilSwitch,
    }
}
