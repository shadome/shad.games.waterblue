﻿
namespace shad.games.waterblue.battlecore.model
{
    // Dev note: strong type enclosing team plus offset values to avoid mixing
    //  up field and party offsets.
    public readonly struct FieldPosition
    {
        public readonly TeamColour team;
        
        internal readonly byte offset;

        private FieldPosition(
            TeamColour team,
            byte offset)
        {
            this.team = team;
            this.offset = offset;
        }

        internal static FieldPosition Create(
            TeamColour team,
            byte offset)
        {
            var field_position = new FieldPosition(
                team: team,
                offset: offset);
            return field_position;
        }
    }
}