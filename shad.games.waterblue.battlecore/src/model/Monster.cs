﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

// TODO search for lib that allow to follow heap usage, e.g., https://www.codeproject.com/Articles/5269747/Using-Span-T-to-improve-performance-of-Csharp-code
namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Monster world instance, must be seen as a discrete species' member.<br/>
    /// Defines a monster's specific data.<br/>
    /// Also encloses the monster species' data.
    /// </summary>
    public readonly struct Monster
    {
        // specific name given by the player
        public readonly string name;

        // // Fixed-size number of fields in order to use the stack and not the heap, 
        // // and throw compile-time errors rather than runtime errors.
        // public readonly Move move_1;
        // public readonly Move? move_2;
        // public readonly Move? move_3;
        // public readonly Move? move_4;

        public readonly ImmutableArray<Move> moves;

        // level of the monster
        public readonly byte level;

        // flyweight design pattern
        public readonly Species species;

        #region creators

        /// <param name="moves">
        /// ordered list of moves,
        /// non-unique moves will be ignored,
        /// at least one move is required
        /// </param>
        /// <exception cref="ArgumentException">when the move list is null or empty</exception>
        private Monster(
            string name,
            Species species,
            byte level,
            ImmutableArray<Move> moves)
        {
            this.name = name;
            this.level = level;
            this.species = species;
            this.moves = moves;
        }

        public static Monster Create(
            string name,
            Species species,
            byte level,
            ImmutableArray<Move> moves)
        {
            _CheckInput(moves);
            var monster = new Monster(
                name: name,
                species: species,
                level: level,
                moves: moves);
            return monster;

            static void _CheckInput(ImmutableArray<Move> moves)
            {
                if (moves.Length == 0)
                {
                    throw new ArgumentException($"{nameof(moves)} should contain at least one element");
                }
            }
        }

        #endregion

    }
}