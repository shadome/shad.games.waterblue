﻿
using System;
using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct Effect
    {
        public readonly ImmutableArray<IComponent> default_components;

        /// <summary>
        /// Label of the effect, e.g., "burning".
        /// </summary>
        public readonly string name;

        // TODO 4 dual type moves?
        // public readonly model.Type type;

// TODO merge DURATION and RECURRENCE into one single nullable object field
        /// <summary>
        /// Initial duration of the effect.<br/>
        /// Conventions:<br/>
        /// Put to null if it is everlasting.<br/>
        /// Put to 0 for instantaneous effect triggers, i.e., triggering immediately when the enclosing move is used.
        /// </summary>
        // public readonly byte? duration;

        /// <summary>
        /// Number of turns to wait before the effect starts triggering (opposite of 'cooldown').<br/>
        /// Default: 0.
        /// </summary>
        // public readonly byte warmup;

        // TODO 4 dedicated formulas for each move?
        // public readonly byte power;

        /// <summary>
        /// Percentage of damage healed back to the caster.
        /// </summary>
        // public readonly byte? lifesteal;

        /// <summary>
        /// Percentage of damage recoiled to the caster.
        /// </summary>
        // public readonly byte? recoil;

        /// <summary>
        /// Implement if the effect should add or subtract some stats to its target.<br/>
        /// Those stat changes would apply before the damage/heals in case of a move
        /// having direct damage and an effect.<br/>
        /// If they should apply after, use a second dedicated effect.
        /// </summary>
        // public readonly Stats? target_stat_modifier;

        /// <summary>
        /// TODO function that takes output base damage and transforms it regarding buffs / debuffs effects.
        /// must be used after base damage calculation by the battlecore
        /// e.g., -5% physical damage taken by the monster this effect is attached onto
        /// TODO use functions or not? if yes, calculate everything with functions? if not, use an int "percentage" or something
        /// </summary>
        //public readonly Func<Effect, int, int> damageOutputModifier;

        /// <summary>
        /// TODO function that takes input base damage and transforms it regarding buffs / debuffs effects.
        /// must be used after base damage calculation by the battlecore
        /// e.g., +10% water damage dealt by the monster this effect is attached onto
        /// TODO use functions or not? if yes, calculate everything with functions? if not, use an int "percentage" or something
        /// </summary>
        //public readonly Func<Effect, int, int> damageInputModifier;

// TODO merge DURATION and RECURRENCE into one single nullable object field

        /// <summary>
        /// Expresses dependency upon caster and/or target 
        /// being present on the battlefield for the effect to remain active.<br/>
        /// An effect can break prematurely, considering its persistence,
        /// when a switch or a KO occurs.
        /// </summary>
        // public readonly EffectPersistence persistence;

        // public readonly EffectRecurrence? recurrence;

        #region creators

        private Effect(
            string name,
            ImmutableArray<IComponent> default_components)
        {
            this.default_components = default_components;
            this.name = name;
        }

        public static Effect Create(
            ImmutableArray<IComponent> default_components,
            string name = "")
        {
            var effect =  new Effect(
                name: name,
                default_components: default_components);
            return effect;
        }

        #endregion

    }
}