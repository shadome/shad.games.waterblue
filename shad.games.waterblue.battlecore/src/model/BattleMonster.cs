﻿using System;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.model
{
    public readonly partial struct BattleMonster
    {
        // TODO calculte real-time priority effect after effect using battlecore services (monster speed, etc.)
        // priority is an orchestration decision, based on battlecore calculations

        // generated at the start of a fight to keep track of a specific monster
        // TODO ORC
        public readonly BattleMonsterId id;

        /// <summary>
        /// Current health during the combat.
        /// </summary>
        public readonly ushort health;

        /// <summary>
        /// Moves available for this monster during this battle.
        /// </summary>
        public readonly ImmutableArray<BattleMove> moveset;

        // Dev's note: readonly struct should enforce flyweight design pattern
        public readonly Monster monster;

        /* boilerplate */

        private BattleMonster(
            BattleMonsterId id,
            ushort health,
            ImmutableArray<BattleMove> moveset,
            Monster monster)
        {
            this.id = id;
            this.health = health;
            this.monster = monster;
            this.moveset = moveset;
        }

        public static BattleMonster Create(
            Monster monster)
        {
            var battle_moves = _MovesToBattleMoves(monster.moves);
            var battle_monster_id = BattleMonsterId.Create();
            var starting_health = (ushort)monster.species.basestats.health;
            var battle_monster = new BattleMonster(
                id: battle_monster_id,
                health: starting_health,
                moveset: battle_moves,
                monster: monster);
            return battle_monster;

            static ImmutableArray<BattleMove> _MovesToBattleMoves(
                ImmutableArray<Move> moves)
            {
                if (moves.IsDefaultOrEmpty)
                {
                    return ImmutableArray<BattleMove>.Empty;
                }
                var mutable_battle_moves = ImmutableArray.CreateBuilder<BattleMove>();
                foreach (var _move in moves)
                {
                    var battle_move_id = BattleMoveId.Create();
                    var battle_move = BattleMove.Create(
                        id: battle_move_id,
                        move: _move);
                    mutable_battle_moves.Add(battle_move);
                }
                return mutable_battle_moves.ToImmutable();
            }
        }
        
        public override bool Equals(object? obj)
            => (obj is BattleMonster other)
            && other.id == this.id
            && other.health == this.health
            && other.moveset.SequenceEqual(this.moveset);

        public override int GetHashCode()
            => this.id.GetHashCode() + this.health + (this.moveset.Length << 8);

        public static bool operator ==(BattleMonster left, BattleMonster right)
            => left.Equals(right);

        public static bool operator !=(BattleMonster left, BattleMonster right)
            => !(left == right);

    }
}