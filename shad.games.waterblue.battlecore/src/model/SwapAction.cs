﻿
namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Structure enclosing information of a monster swap, i.e., 
    /// one player exchanging a monster on the battlefield and a monster in the team.
    /// </summary>
    public readonly struct SwapAction
    {
        /// <summary>
        /// Identifier of the battle monster going out of the battlefield.
        /// </summary>
        public readonly BattleMonsterId outgoing;

        /// <summary>
        /// Identifier of the battle monster coming in from the party.
        /// </summary>
        public readonly BattleMonsterId incoming;

        #region creators

        private SwapAction(
            BattleMonsterId outgoing,
            BattleMonsterId incoming)
        {
            this.outgoing = outgoing;
            this.incoming = incoming;
        }

        public static SwapAction Create(
            BattleMonsterId outgoing,
            BattleMonsterId incoming)
        {
            var swap_action = new SwapAction(
                outgoing: outgoing,
                incoming: incoming);
            return swap_action;
        }

        #endregion

    }
}