﻿namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Metadata of a monster; all monsters of the same species share some data.
    /// </summary>
    public readonly struct Species
    {
        // number of the species
        public readonly ushort number;

        // general name of the species
        public readonly string name;

        // TODO 4 make dependant on something like talents?
        // types of the species
        //public readonly IImmutableSet<Type> types;
        public readonly Type type_1;

        public readonly Type? type_2;

        // moves that have been learnt by the monster and can be selected in the moveset
        //public readonly IImmutableSet<Move> moves;

        // TODO 4 confirm usefulness
        //public readonly IImmutableSet<Move> unlockableMoves;

        // base stats of the species
        public readonly Stats basestats;

        #region creators

        private Species(
            ushort number,
            string name,
            Stats basestats,
            Type type_1,
            Type? type_2 = null)
        {
            this.number = number;
            this.name = name;
            this.basestats = basestats;
            this.type_1 = type_1;
            this.type_2 = type_2;
        }

        public static Species Create(
            ushort number,
            string name,
            Stats basestats,
            Type type_1,
            Type? type_2 = null)
        {
            var species = new Species(
                number: number,
                name: name,
                basestats: basestats,
                type_1: type_1,
                type_2: type_2);
            return species;
        }

        #endregion

    }
}