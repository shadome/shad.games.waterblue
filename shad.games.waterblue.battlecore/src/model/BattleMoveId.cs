﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct BattleMoveId
    {
        private readonly Guid id;

        #region creators

        private BattleMoveId(
            Guid id)
        {
            this.id = id;
        }

        internal static BattleMoveId Create()
            => new(Guid.NewGuid());

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is BattleMoveId other) && other == this;

        public override int GetHashCode()
            => this.id.GetHashCode();

        public static bool operator ==(BattleMoveId left, BattleMoveId right)
            => left.id == right.id;

        public static bool operator !=(BattleMoveId left, BattleMoveId right)
            => left.id != right.id;
     
        public override string ToString()
            => id.ToString();

        #endregion

    }
}
