﻿
namespace shad.games.waterblue.battlecore.model
{
    // Dev note: strong type enclosing team plus offset values to avoid mixing
    //  up field and party offsets.
    public readonly struct PartyPosition
    {
        public readonly TeamColour team;
        
        internal readonly byte offset;

        private PartyPosition(
            TeamColour team,
            byte offset)
        {
            this.team = team;
            this.offset = offset;
        }

        internal static PartyPosition Create(
            TeamColour team,
            byte offset)
        {
            var party_position = new PartyPosition(
                team: team,
                offset: offset);
            return party_position;
        }
    }
}