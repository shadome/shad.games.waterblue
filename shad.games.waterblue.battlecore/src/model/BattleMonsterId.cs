﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly struct BattleMonsterId
    {
        private readonly Guid id;

        #region creators

        private BattleMonsterId(
            Guid id)
        {
            this.id = id;
        }

        internal static BattleMonsterId Create()
            => new(Guid.NewGuid());

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is BattleMonsterId other) && other == this;

        public override int GetHashCode()
            => this.id.GetHashCode();

        public static bool operator ==(BattleMonsterId left, BattleMonsterId right)
            => left.id == right.id;

        public static bool operator !=(BattleMonsterId left, BattleMonsterId right)
            => left.id != right.id;

        public override string ToString() 
            => id.ToString();

        #endregion

    }
}
