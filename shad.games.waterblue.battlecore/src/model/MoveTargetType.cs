﻿namespace shad.games.waterblue.battlecore.model
{
    // Dev's note: using a struct with static provided fields and no 
    // constructors allows compile-time safety for the caller.
    // Encapsulating an enum allows the use of the switch syntaxes.
    // Be sure to handle the struct's default constructor as a non-critical
    // business logic value.

    /// <summary>
    /// Possible targets for a move. <br/>
    /// Semantics: 'fixed' means the player does not have a choice,
    /// 'choice' means the player must choose.
    /// </summary>
    public readonly struct MoveTargetType
    {
        /// <summary>
        /// Should be the default value for moves that do not have a target such as weather moves, etc. 
        /// </summary>
        public static readonly MoveTargetType NotApplicable = new(MoveTargetTypeEnum.NotApplicable, false); // keep this as default(MoveTarget)
        /// <summary>
        /// The player chooses any target on the battlefield.
        /// </summary>
        // public static readonly MoveTargetType ChoiceBattlefieldSingle = new(MoveTargetTypeEnum.ChoiceBattlefieldSingle, true);
        /// <summary>
        /// The player chooses any target on the battlefield except the caster.
        /// </summary>
        public static readonly MoveTargetType ChoiceBattlefieldSingleExceptCaster = new(MoveTargetTypeEnum.ChoiceBattlefieldSingleExceptCaster, true);
        /// <summary>
        /// The player chooses any side of the battlefield, the move will target all monsters on that side.
        /// </summary>
        // public static readonly MoveTargetType ChoiceBattlefieldSide = new(MoveTargetTypeEnum.ChoiceBattlefieldSide, true);
        /// <summary>
        /// The player chooses one foe on the battlefield.
        /// </summary>
        // public static readonly MoveTargetType ChoiceBattlefieldSingleFoe = new(MoveTargetTypeEnum.ChoiceBattlefieldSingleFoe, true);
        /// <summary>
        /// The player chooses one of his monsters on the battlefield (can be the casting one).
        /// </summary>
        // public static readonly MoveTargetType ChoiceBattlefieldSinglePal = new(MoveTargetTypeEnum.ChoiceBattlefieldSinglePal, true);
        /// <summary>
        /// Targets the caster of the move.
        /// </summary>
        public static readonly MoveTargetType FixedSelf = new(MoveTargetTypeEnum.FixedSelf, false);
        /// <summary>
        /// Targets the player's monster on the battlefield which has not casted the move.
        /// </summary>
        // public static readonly MoveTargetType FixedDirectPal = new(MoveTargetTypeEnum.FixedDirectPal, false);
        /// <summary>
        /// Targets all of the player's monsters on the battlefield.
        /// </summary>
        // public static readonly MoveTargetType FixedBattlefieldSidePlayer = new(MoveTargetTypeEnum.FixedBattlefieldSidePlayer, false);
        /// <summary>
        /// Targets all the opponent's monsters on the battlefield.
        /// </summary>
        public static readonly MoveTargetType FixedBattlefieldSideOpponent = new(MoveTargetTypeEnum.FixedBattlefieldSideOpponent, false);
        /// <summary>
        /// Targets all monsters on the battlefield except the caster.
        /// </summary>
        public static readonly MoveTargetType FixedBattlefieldAllExceptCaster = new(MoveTargetTypeEnum.FixedBattlefieldAllExceptCaster, false);
        /// <summary>
        /// Targets all monsters on the battlefield.
        /// </summary>
        public static readonly MoveTargetType FixedBattlefieldAll = new(MoveTargetTypeEnum.FixedBattlefieldAll, false);
        /// <summary>
        /// Targets all of the player's party monsters.
        /// </summary>
        // public static readonly MoveTargetType FixedPartyPlayer = new(MoveTargetTypeEnum.FixedPartyPlayer, false);
        /// <summary>
        /// Targets all of the opponent's party monsters.
        /// </summary>
        // public static readonly MoveTargetType FixedPartyOpponent = new(MoveTargetTypeEnum.FixedAllOpponent, false);
        /// <summary>
        /// Targets the battlefield and party monsters of the player.
        /// </summary>
        // public static readonly MoveTargetType FixedAllPlayer = new(MoveTargetTypeEnum.FixedAllPlayer, false);
        /// <summary>
        /// Targets the battlefield and party monsters of the opponent.
        /// </summary>
        // public static readonly MoveTargetType FixedAllOpponent = new(MoveTargetTypeEnum.FixedAllOpponent, false);

        #region creators / internal

        internal readonly MoveTargetTypeEnum value;

        internal readonly bool is_choice;

        private MoveTargetType(
            MoveTargetTypeEnum value,
            bool is_choice)
        {
            this.value = value;
            this.is_choice = is_choice;
        }

        #endregion

        #region overrides

        public override bool Equals(object? obj)
            => (obj is MoveTargetType other) && other == this;

        public override int GetHashCode()
            => (byte)this.value + (is_choice ? 255 : 0);

        public static bool operator ==(MoveTargetType left, MoveTargetType right)
            => (left.value, left.is_choice) == (right.value, right.is_choice);

        public static bool operator !=(MoveTargetType left, MoveTargetType right)
            => !(left == right);

        public override string ToString() => this.value.ToString();

        #endregion

    }
}
