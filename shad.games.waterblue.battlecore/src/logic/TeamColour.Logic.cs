﻿
using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.model
{
    public readonly partial struct TeamColour
    {
        // Keep up to date!
        // Dev's note: the following serves as a workaround since 
        // public static readonly Collection<TeamColour> Values; 
        // fails in various manners
        // * ImmutableArray<TeamColour> fails at execution
        // * TeamColour[] fails as the enumerator returns Blue for every element
        public static ImmutableArray<TeamColour> GetValues()
        {
            return ImmutableArray.CreateRange(
                new[]
                {
                    TeamColour.Blue,
                    TeamColour.Grey,
                });
        }

        // Keep up to date!
        public static TeamColour GetOpposite(TeamColour team)
        {
            return team.value switch
            {
                TeamColourEnum.Blue => TeamColour.Grey,
                TeamColourEnum.Grey => TeamColour.Blue,
                _ => throw new System.Exception($"unexpected value of TeamColourEnum"),
            };
        }
    }
}
