﻿using System.Collections.Immutable;
using System.Linq;
using shad.games.waterblue.lib;
using shad.games.waterblue.lib.pipelines;

namespace shad.games.waterblue.battlecore.model
{
    public static class BattlefieldStatic
    {

        internal static BattleEffect? TryGetBattleEffect(
            this Battlefield battlefield,
            BattleEffectId effect_id)
        {
            foreach (var _effect in battlefield.battle_effects)
            {
                if (_effect.id == effect_id)
                {
                    return _effect;
                }
            }
            return null;
        }

        internal static T? TryGetComponent<T>(
            this Battlefield battlefield, 
            BattleEffectId effect_id) 
            where T : struct, IComponent
        {
            return (T?)battlefield.effects_components[effect_id]
                .FirstOrDefault(_component => _component is T);
        }

        public static FieldPosition? TryGetMonsterFieldPosition(
            this Battlefield battlefield,
            BattleMonsterId monster_id)
        {
            foreach (var team in battlefield.field_monsters)
            {
                for (byte i = 0; i < team.Value.Length; i++)
                {
                    if (team.Value[i].id == monster_id)
                    {
                        return FieldPosition.Create(team: team.Key, offset: i);
                    }
                }
            }
            return null;
        }

        public static PartyPosition? TryGetMonsterPartyPosition(
            this Battlefield battlefield,
            BattleMonsterId monster_id)
        {
            foreach (var team in battlefield.party_monsters)
            {
                for (byte i = 0; i < team.Value.Length; i++)
                {
                    if (team.Value[i].id == monster_id)
                    {
                        return PartyPosition.Create(team: team.Key, offset: i);
                    }
                }
            }
            return null;
        }

    }

    public readonly partial struct Battlefield
    {

        internal static Battlefield RemoveBattleEffect(
            Battlefield battlefield,
            BattleEffectId effect_id)
        {
            var effects_components = battlefield.effects_components
                .Remove(effect_id);
            var effects = battlefield.battle_effects
                .Where(_effect => _effect.id != effect_id)
                .ToImmutableArray();
            battlefield = new Battlefield(
                iteration: battlefield.iteration,
                field_monsters: battlefield.field_monsters,
                party_monsters: battlefield.party_monsters,
                effects_components: effects_components,
                battle_effects: effects);
            return battlefield;
        }

        internal static Battlefield SetComponent(
            Battlefield battlefield,
            BattleEffectId effect_id,
            IComponent component)
        {
            var components = battlefield.effects_components[effect_id]
                .Where(_component => _component.GetType() != component.GetType())
                .Append(component);
            var effects_components = battlefield.effects_components.SetItem(
                key: effect_id, 
                value: ImmutableArray.CreateRange(components));
            battlefield = new Battlefield(
                iteration: battlefield.iteration,
                field_monsters: battlefield.field_monsters,
                party_monsters: battlefield.party_monsters,
                effects_components: effects_components,
                battle_effects: battlefield.battle_effects);
            return battlefield;
        }

        internal static Battlefield AddBattleEffect(
            Battlefield previous,
            FieldPosition target_position,
            BattleEffect effect)
        {
            var effects = previous.battle_effects.Add(effect);
            var components = effect.effect.default_components
                .Select(_component => _component is TargetMonsterComponent target_monster_component
                    ? TargetMonsterComponent.SetTarget(target_monster_component, previous[target_position].id)
                    : _component)
                .ToImmutableArray();
            var effects_components = previous.effects_components.SetItem(
                key: effect.id,
                value: components);
            var battlefield = new Battlefield(
                iteration: previous.iteration,
                field_monsters: previous.field_monsters,
                party_monsters: previous.party_monsters,
                effects_components: effects_components,
                battle_effects: effects);
            return battlefield;
        }

        internal static Battlefield UpdateBattleMonster(
            Battlefield previous,
            FieldPosition position,
            BattleMonster new_value)
        {
            var (success, updated_monsters) = Battlefield.UpdateDictMonsters(
                dict: previous.field_monsters,
                team: position.team,
                offset: position.offset,
                new_value: new_value);
            var battlefield = success
                ? new Battlefield(
                    iteration: previous.iteration,
                    field_monsters: updated_monsters,
                    party_monsters: previous.party_monsters,
                    effects_components: previous.effects_components,
                    battle_effects: previous.battle_effects)
                : previous;
            return battlefield;
        }

        internal static Battlefield UpdateBattleMonster(
            Battlefield previous,
            PartyPosition position,
            BattleMonster new_value)
        {
            var (success, updated_monsters) = Battlefield.UpdateDictMonsters(
                dict: previous.party_monsters,
                team: position.team,
                offset: position.offset,
                new_value: new_value);
            var battlefield = success
                ? new Battlefield(
                    iteration: previous.iteration,
                    field_monsters: previous.field_monsters,
                    party_monsters: updated_monsters,
                    effects_components: previous.effects_components,
                    battle_effects: previous.battle_effects)
                : previous;
            return battlefield;
        }

        /* private */

        private static (bool success, ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> updated) UpdateDictMonsters(
            ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> dict,
            TeamColour team,
            byte offset,
            BattleMonster new_value)
        {
            if (!dict.ContainsKey(team) || offset >=  dict[team].Length)
            {
                return (success: false, updated: dict);
            }
            var updated_array = dict[team].SetItem(
                index: offset,
                item: new_value);
            var updated_dict = dict.SetItem(
                key: team,
                value: updated_array);
            return (success: true, updated: updated_dict);
        }

        /// <summary>
        /// Returns a new instance of the battlefield with provided 
        /// battlemonsters updated (based on BattleMonsterId).<br/>
        /// Unprovided battle monsters are not modified not removed from the
        /// battlefield.
        /// </summary>
        // internal static Battlefield UpdateBattleMonsters(
        //     Battlefield previous,
        //     ImmutableArray<BattleMonster> new_values)
        // {
        //     var (field_remaining, updated_field_monsters) =
        //         _UpdateBattleMonsters(
        //             teams: previous.field_monsters,
        //             new_values: new_values,
        //             remaining: new_values.Length);
        //     var (remaining, updated_party_monsters) =
        //         _UpdateBattleMonsters(
        //             teams: previous.party_monsters,
        //             new_values: new_values,
        //             remaining: field_remaining);
        //     _HandleRemaining(remaining);
        //     var battlefield = new Battlefield(
        //         iteration: previous.iteration,
        //         field_monsters: updated_field_monsters,
        //         party_monsters: updated_party_monsters,
        //         effects_components: previous.effects_components,
        //             battle_effects: previous.battle_effects);
        //     return battlefield;

        //     // Dev note on asymptotic notations
        //     // The following code may seem like O(n3) but the arrays are
        //     // fix-sized or very small due to game logic.
        //     // Feels like performances would rather be improved by avoiding
        //     // heap allocations than creating dictionaries and sets.

        //     static (int remaining, ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> updated_field_monsters) _UpdateBattleMonsters(
        //         ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> teams,
        //         ImmutableArray<BattleMonster> new_values,
        //         int remaining)
        //     {
        //         if (remaining == 0)
        //         {
        //             return (remaining, teams);
        //         }
        //         var mutable_teams = teams.ToBuilder();
        //         foreach (var team in mutable_teams)
        //         {
        //             var mutable_team_monsters = team.Value.ToBuilder();
        //             for (int i = 0; i < mutable_team_monsters.Count && remaining > 0; i++)
        //             {   // asymptotic note: new_values.Count < 6
        //                 for (int j = 0; j < new_values.Length; j++)
        //                 {   // asymptotic note: new_values.Count < 6
        //                     if (new_values[j].id == mutable_team_monsters[i].id)
        //                     {
        //                         mutable_team_monsters[i] = new_values[j];
        //                         remaining--;
        //                         break;
        //                     }
        //                 }
        //             }
        //             mutable_teams[team.Key] = mutable_team_monsters.ToImmutable();
        //         }
        //         return (remaining, mutable_teams.ToImmutable());
        //     }

        //     static void _HandleRemaining(int remaining)
        //     {
        //         if (remaining > 0)
        //         {
        //             // silently accepts it as of now
        //             // throw new Exception();
        //         }
        //     }
        // }

        // internal static Battlefield RemoveEffectsFromBattlefield(
        //     Battlefield previous,
        //     BattleMonsterId? caster_id_filter = null,
        //     EffectPersistence? effect_persistence_filter = null)
        // {
        //     if (caster_id_filter == null && effect_persistence_filter == null)
        //     {
        //         return previous;
        //     }
        //     var updated_field_monsters = _RemoveEffectFromBattleMonsters(
        //         dict: previous.field_monsters,
        //         caster_id_filter: caster_id_filter,
        //         effect_persistence_filter: effect_persistence_filter);
        //     var updated_party_monsters = _RemoveEffectFromBattleMonsters(
        //         dict: previous.party_monsters,
        //         caster_id_filter: caster_id_filter,
        //         effect_persistence_filter: effect_persistence_filter);
        //     return new Battlefield(
        //         iteration: previous.iteration,
        //         field_monsters: updated_field_monsters,
        //         party_monsters: updated_party_monsters,
        //         effects_components: previous.effects_components,
        //             battle_effects: previous.battle_effects);

        //     // factorising the treatment which is the same for battlefield and party monsters
        //     static ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> _RemoveEffectFromBattleMonsters(
        //         ImmutableDictionary<TeamColour, ImmutableArray<BattleMonster>> dict,
        //         BattleMonsterId? caster_id_filter = null,
        //         EffectPersistence? effect_persistence_filter = null)
        //     {
        //         var monster_dict_builder = ImmutableDictionary.CreateBuilder<TeamColour, ImmutableArray<BattleMonster>>();
        //         foreach (var _teamcolour in dict.Keys)
        //         {
        //             var monster_array_builder = ImmutableArray.CreateBuilder<BattleMonster>();
        //             foreach (var _battlemonster in dict[_teamcolour])
        //             {
        //                 var beffect_builder = ImmutableArray.CreateBuilder<BattleEffect>();
        //                 foreach (var _battleeffect in _battlemonster.effects)
        //                 {
        //                     if (_battleeffect.effect.persistence != effect_persistence_filter
        //                         || _battleeffect.caster_copy.Invoke().id == caster_id_filter)
        //                     {
        //                         // keep the current battle effect if it does not match the removal criteria 
        //                         beffect_builder.Add(_battleeffect);
        //                     }
        //                 }
        //                 var updated_battle_monster = BattleMonster.UpdateBattleEffects(
        //                     previous: _battlemonster,
        //                     effects: beffect_builder.ToImmutable());
        //                 monster_array_builder.Add(updated_battle_monster);
        //             }
        //             var updated_monster_array = monster_array_builder.ToImmutable();
        //             monster_dict_builder.Add(key: _teamcolour, value: updated_monster_array);
        //         }
        //         var updated_dict = monster_dict_builder.ToImmutable();
        //         return updated_dict;
        //     };

        // }

    }
}




        // internal static Battlefield ExchangeBattleMonstersPositions(
        //     Battlefield previous,
        //     TeamColour team,
        //     byte battlefield_offset,
        //     byte party_offset)
        // {
        //     var battlefield_monster = previous.battlefield_monsters[team][battlefield_offset];
        //     var party_monster = previous.battlefield_monsters[team][party_offset];
        //     var updated_battlefield_team_monsters = previous.battlefield_monsters[team]
        //         .SetItem(battlefield_offset, party_monster);
        //     var updated_party_team_monsters = previous.party_monsters[team]
        //         .SetItem(party_offset, battlefield_monster);
        //     return new(
        //         iteration: previous.iteration,
        //         battlefield_monsters: previous.battlefield_monsters.SetItem(team, updated_battlefield_team_monsters),
        //         party_monsters: previous.party_monsters.SetItem(team, updated_party_team_monsters));

        // }


        // /// <summary>
        // /// Returns a new instance of the battlefield with provided 
        // /// battlemonsters updated (based on BattleMonsterId).<br/>
        // /// Unprovided battle monsters are not modified not removed from hte
        // /// battlefield.</br>
        // /// Notes: 
        // ///     - use new_value for a single battle monster update
        // ///     - use new_values for more than one update
        // ///     - in case of conflict, new_value has priority
        // /// </summary>
        // internal static Battlefield UpdateBattleMonsters(
        //     Battlefield previous,
        //     BattleMonster? new_value = null,
        //     ImmutableArray<BattleMonster>? new_values = null)
        // {

        //     if (new_value == null && new_values == null || new_values!.Value.IsDefaultOrEmpty)
        //     {
        //         return previous;
        //     }
        //     // Dev note on asymptotic notations
        //     // The following code may seem like O(n3) but the arrays are
        //     // fix-sized or very small due to game logic.
        //     // Feels like performances would rather be improved by avoiding
        //     // heap allocations than creating dictionaries and sets.

        //     int remaining = (new_values?.Length ?? 0) + (new_value != null ? 1 : 0);
        //     var updated_battlefield_monsters = previous.battlefield_monsters;
        //     var updated_party_monsters = previous.party_monsters;

        //     if (remaining > 0)
        //     {
        //         var mutable_battlefield_teams = previous.battlefield_monsters.ToBuilder();
        //         foreach (var team in mutable_battlefield_teams)
        //         {
        //             var mutable_battlefield_team_monsters = team.Value.ToBuilder();
        //             for (int i = 0; i < mutable_battlefield_team_monsters.Count && remaining > 0; i++)
        //             {   // asymptotic note: new_values.Count < 6
        //                 if (new_values != null)
        //                 {
        //                     for (int j = 0; j < new_values.Value.Length; j++)
        //                     {   // asymptotic note: new_values.Count < 6
        //                         if (new_values.Value[j].id == mutable_battlefield_team_monsters[i].id)
        //                         {
        //                             mutable_battlefield_team_monsters[i] = new_values.Value[j];
        //                             remaining--;
        //                             break;
        //                         }
        //                     }
        //                 }
        //                 if (new_value != null && new_value.Value.id == mutable_battlefield_team_monsters[i].id)
        //                 {
        //                     mutable_battlefield_team_monsters[i] = new_value.Value;
        //                     remaining--;
        //                 }
        //             }
        //             mutable_battlefield_teams[team.Key] = mutable_battlefield_team_monsters.ToImmutable();
        //         }
        //         updated_battlefield_monsters = mutable_battlefield_teams.ToImmutable();
        //     }
        //     if (remaining > 0)
        //     {
        //         var mutable_party_teams = previous.party_monsters.ToBuilder();
        //         foreach (var team in mutable_party_teams)
        //         {
        //             var mutable_party_team_monsters = team.Value.ToBuilder();
        //             for (int i = 0; i < mutable_party_team_monsters.Count && remaining > 0; i++)
        //             {   // asymptotic note: new_values.Count < 6
        //                 if (new_values != null)
        //                 {
        //                     for (int j = 0; j < new_values.Value.Length; j++)
        //                     {   // asymptotic note: new_values.Count < 6
        //                         if (new_values.Value[j].id == mutable_party_team_monsters[i].id)
        //                         {
        //                             mutable_party_team_monsters[i] = new_values.Value[j];
        //                             remaining--;
        //                             break;
        //                         }
        //                     }
        //                 }
        //                 if (new_value != null && new_value.Value.id == mutable_party_team_monsters[i].id)
        //                 {
        //                     mutable_party_team_monsters[i] = new_value.Value;
        //                     remaining--;
        //                 }
        //             }
        //             mutable_party_teams[team.Key] = mutable_party_team_monsters.ToImmutable();
        //         }
        //         updated_battlefield_monsters = mutable_party_teams.ToImmutable();
        //     }

        //     if (remaining > 0)
        //     {
        //         // TODO handle properly, invalid ID were provided
        //         throw new Exception();
        //     }

        //     return new Battlefield(
        //         iteration: previous.iteration,
        //         battlefield_monsters: updated_battlefield_monsters,
        //         party_monsters: updated_party_monsters);
        // }
