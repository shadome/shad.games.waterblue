namespace shad.games.waterblue.battlecore.model
{
    /// <summary>
    /// Class listing the implications a certain component could have for its enclosing effect,
    /// i.e., whether a specific given component's state "means" something specific for the enclosing effect. 
    /// </summary>
    internal static class EffectComponentPredicate
    {
        public static bool IsEffectImmediate(IComponent component)
            => component is ImmediateEffectComponent;

        /// <summary>
        /// Whether the associated effect had a duration which is now over.
        /// </summary>
        public static bool HasEffectOutlastedItsDuration(IComponent component) 
            => component is DurationComponent _durationcomp && _durationcomp.remaining < 1;

        /// <summary>
        /// WHether the associated effect is a debuff and must be attached to a target.
        /// </summary>
        public static bool MustEffectBeAttached(IComponent component)
            => component is TriggerComponent || component is RecurrenceComponent;

        /// <summary>
        /// Whether the given component defines what happens when the monster on which the effect is attached to (the 'attachee') swaps out
        /// </summary>
        public static bool HasEffectDefinedBehaviourWhenAttacheeSwapsOut(IComponent component)
            => component is TargetMonsterComponent;
    }
}