﻿using System;
using System.Collections.Immutable;
using System.Linq;
using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.lib.pipelines;

namespace shad.games.waterblue.battlecore.model
{
    public static class BattleMonsterStatic
    {
        internal static BattleMove? TryGetMove(
            this BattleMonster monster,
            BattleMoveId move_id)
        {
            for (int i = 0; i < monster.moveset.Length; i++)
            {
                if (monster.moveset[i].id == move_id)
                {
                    return monster.moveset[i];
                }
            }
            return null;
        }
    }
    public readonly partial struct BattleMonster
    {
        internal static (BattleMonster monster, MitigationLog log) DealDamage(
            BattleMonster previous,
            ushort damage,
            TypeEffectiveness? type_effectiveness = null)
        {
            var new_health = (ushort)Math.Max(0, previous.health - damage);
            var updated_monster = new BattleMonster(
                id: previous.id,
                health: new_health,
                moveset: previous.moveset,
                monster: previous.monster);
            var mitigation_log = MitigationLog.Create(
                monster_id: previous.id,
                type_effectiveness: type_effectiveness ?? TypeEffectiveness.Neutral,
                mitigation: (short)damage,
                mitigation_absorbed: 0,
                mitigation_overflow: (short)Math.Max(0, damage - previous.health),
                has_knocked_out_target: updated_monster.IsKnockedOut());
            return (monster: updated_monster, log: mitigation_log);
        }
    }
}