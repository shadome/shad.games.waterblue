﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly partial struct Type
{
        public static TypeEffectiveness GetTypeEffectiveness(
            Type offensive_type, 
            Type? defensive_type)
        {
            return (off: offensive_type.value, def: defensive_type?.value) switch
            {
                (off: _,                    def: null)              => TypeEffectiveness.Neutral,
                // normal
                (off: TypeEnum.Normal,      def: TypeEnum.Ghost)    => TypeEffectiveness.Immune,
                (off: TypeEnum.Normal,      def: TypeEnum.Rock)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Normal,      def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                // fire
                (off: TypeEnum.Fire,        def: TypeEnum.Fire)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fire,        def: TypeEnum.Water)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fire,        def: TypeEnum.Rock)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fire,        def: TypeEnum.Dragon)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fire,        def: TypeEnum.Grass)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Fire,        def: TypeEnum.Ice)      => TypeEffectiveness.Effective,
                (off: TypeEnum.Fire,        def: TypeEnum.Bug)      => TypeEffectiveness.Effective,
                (off: TypeEnum.Fire,        def: TypeEnum.Steel)    => TypeEffectiveness.Effective,
                // water
                (off: TypeEnum.Water,       def: TypeEnum.Water)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Water,       def: TypeEnum.Grass)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Water,       def: TypeEnum.Dragon)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Water,       def: TypeEnum.Fire)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Water,       def: TypeEnum.Ground)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Water,       def: TypeEnum.Rock)     => TypeEffectiveness.Effective,
                // electric
                (off: TypeEnum.Electric,    def: TypeEnum.Ground)   => TypeEffectiveness.Immune,
                (off: TypeEnum.Electric,    def: TypeEnum.Electric) => TypeEffectiveness.Resistant,
                (off: TypeEnum.Electric,    def: TypeEnum.Grass)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Electric,    def: TypeEnum.Dragon)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Electric,    def: TypeEnum.Water)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Electric,    def: TypeEnum.Flying)   => TypeEffectiveness.Effective,
                // grass
                (off: TypeEnum.Grass,       def: TypeEnum.Fire)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Grass)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Poison)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Flying)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Bug)      => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Dragon)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Grass,       def: TypeEnum.Water)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Grass,       def: TypeEnum.Ground)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Grass,       def: TypeEnum.Rock)     => TypeEffectiveness.Effective,
                // ice
                (off: TypeEnum.Ice,         def: TypeEnum.Fire)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ice,         def: TypeEnum.Water)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ice,         def: TypeEnum.Ice)      => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ice,         def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ice,         def: TypeEnum.Grass)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Ice,         def: TypeEnum.Ground)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Ice,         def: TypeEnum.Flying)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Ice,         def: TypeEnum.Dragon)   => TypeEffectiveness.Effective,
                // fighting
                (off: TypeEnum.Fightning,   def: TypeEnum.Ghost)    => TypeEffectiveness.Immune,
                (off: TypeEnum.Fightning,   def: TypeEnum.Poison)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fightning,   def: TypeEnum.Flying)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fightning,   def: TypeEnum.Psychic)  => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fightning,   def: TypeEnum.Bug)      => TypeEffectiveness.Resistant,
                (off: TypeEnum.Fightning,   def: TypeEnum.Normal)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Fightning,   def: TypeEnum.Ice)      => TypeEffectiveness.Effective,
                (off: TypeEnum.Fightning,   def: TypeEnum.Rock)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Fightning,   def: TypeEnum.Dark)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Fightning,   def: TypeEnum.Steel)    => TypeEffectiveness.Effective,
                // poison
                (off: TypeEnum.Poison,      def: TypeEnum.Steel)    => TypeEffectiveness.Immune,
                (off: TypeEnum.Poison,      def: TypeEnum.Poison)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Poison,      def: TypeEnum.Ground)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Poison,      def: TypeEnum.Rock)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Poison,      def: TypeEnum.Ghost)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Poison,      def: TypeEnum.Grass)    => TypeEffectiveness.Effective,
                // ground
                (off: TypeEnum.Ground,      def: TypeEnum.Flying)   => TypeEffectiveness.Immune,
                (off: TypeEnum.Ground,      def: TypeEnum.Bug)      => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ground,      def: TypeEnum.Grass)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ground,      def: TypeEnum.Fire)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Ground,      def: TypeEnum.Electric) => TypeEffectiveness.Effective,
                (off: TypeEnum.Ground,      def: TypeEnum.Poison)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Ground,      def: TypeEnum.Rock)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Ground,      def: TypeEnum.Steel)    => TypeEffectiveness.Effective,
                // flying
                (off: TypeEnum.Flying,      def: TypeEnum.Electric) => TypeEffectiveness.Resistant,
                (off: TypeEnum.Flying,      def: TypeEnum.Rock)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Flying,      def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Flying,      def: TypeEnum.Grass)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Flying,      def: TypeEnum.Fightning)=> TypeEffectiveness.Effective,
                (off: TypeEnum.Flying,      def: TypeEnum.Bug)      => TypeEffectiveness.Effective,
                // psychic
                (off: TypeEnum.Psychic,     def: TypeEnum.Dark)     => TypeEffectiveness.Immune,
                (off: TypeEnum.Psychic,     def: TypeEnum.Psychic)  => TypeEffectiveness.Resistant,
                (off: TypeEnum.Psychic,     def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Psychic,     def: TypeEnum.Fightning)=> TypeEffectiveness.Effective,
                (off: TypeEnum.Psychic,     def: TypeEnum.Poison)   => TypeEffectiveness.Effective,
                // bug
                (off: TypeEnum.Bug,         def: TypeEnum.Fire)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Fightning)=> TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Poison)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Flying)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Ghost)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Bug,         def: TypeEnum.Grass)    => TypeEffectiveness.Effective,
                (off: TypeEnum.Bug,         def: TypeEnum.Psychic)  => TypeEffectiveness.Effective,
                (off: TypeEnum.Bug,         def: TypeEnum.Dark)     => TypeEffectiveness.Effective,
                // rock
                (off: TypeEnum.Rock,        def: TypeEnum.Fightning)=> TypeEffectiveness.Resistant,
                (off: TypeEnum.Rock,        def: TypeEnum.Ground)   => TypeEffectiveness.Resistant,
                (off: TypeEnum.Rock,        def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Rock,        def: TypeEnum.Fire)     => TypeEffectiveness.Effective,
                (off: TypeEnum.Rock,        def: TypeEnum.Ice)      => TypeEffectiveness.Effective,
                (off: TypeEnum.Rock,        def: TypeEnum.Flying)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Rock,        def: TypeEnum.Bug)      => TypeEffectiveness.Effective,
                // ghost
                (off: TypeEnum.Ghost,       def: TypeEnum.Normal)   => TypeEffectiveness.Immune,
                (off: TypeEnum.Ghost,       def: TypeEnum.Dark)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Ghost,       def: TypeEnum.Psychic)  => TypeEffectiveness.Effective,
                (off: TypeEnum.Ghost,       def: TypeEnum.Ghost)    => TypeEffectiveness.Effective,
                // dragon
                (off: TypeEnum.Dragon,      def: TypeEnum.Dragon)   => TypeEffectiveness.Effective,
                (off: TypeEnum.Dragon,      def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                // dark
                (off: TypeEnum.Dark,        def: TypeEnum.Fightning)=> TypeEffectiveness.Resistant,
                (off: TypeEnum.Dark,        def: TypeEnum.Dark)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Dark,        def: TypeEnum.Psychic)  => TypeEffectiveness.Effective,
                (off: TypeEnum.Dark,        def: TypeEnum.Ghost)    => TypeEffectiveness.Effective,
                // steel
                (off: TypeEnum.Steel,       def: TypeEnum.Fire)     => TypeEffectiveness.Resistant,
                (off: TypeEnum.Steel,       def: TypeEnum.Water)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Steel,       def: TypeEnum.Electric) => TypeEffectiveness.Resistant,
                (off: TypeEnum.Steel,       def: TypeEnum.Steel)    => TypeEffectiveness.Resistant,
                (off: TypeEnum.Steel,       def: TypeEnum.Ice)      => TypeEffectiveness.Effective,
                (off: TypeEnum.Steel,       def: TypeEnum.Rock)     => TypeEffectiveness.Effective,
                // rest of the combinations
                (off: _,                    def: _)                 => TypeEffectiveness.Neutral,
            };
        }
    }
}