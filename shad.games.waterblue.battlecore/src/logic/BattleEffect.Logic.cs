﻿using System;

namespace shad.games.waterblue.battlecore.model
{
    public readonly partial struct BattleEffect
    {
        /// <summary>
        /// Updates the given battle effect to its new value after a turn
        /// has passed.<br/>
        /// Returns null if the battle effect has ended.
        /// </summary>
        // internal static BattleEffect? Tick(
        //     BattleEffect battle_effect)
        // {
        //     return (battle_effect.waiting, battle_effect.remaining) switch
        //     {
        //         // waiting: waiting--, remaining is untouched
        //         (_, _) when battle_effect.waiting > 0
        //             => new BattleEffect(
        //                 id: battle_effect.id,
        //                 iteration: battle_effect.iteration,
        //                 caster_copy: battle_effect.caster_copy,
        //                 remaining: battle_effect.remaining,
        //                 waiting: (byte)(battle_effect.waiting - 1),
        //                 effect: battle_effect.effect),
        //         // below this point, waiting = 0
        //         // infinite duration
        //         (_, null) => battle_effect,
        //         // should never happen: waiting = 0 and remaining = 0 means
        //         // the battleeffect should have been removed earlier
        //         (_, 0) => null, 
        //         // not waiting, remaining = 1: the battleeffect ends now
        //         (_, 1) => null,
        //         // not waiting, remaining > 1: remaining--
        //         // Dev note: adding 'when' clause because of the compiler
        //         (_, _) when battle_effect.remaining != null
        //             => new BattleEffect(
        //                 id: battle_effect.id,
        //                 iteration: battle_effect.iteration,
        //                 caster_copy: battle_effect.caster_copy,
        //                 remaining: (byte)(battle_effect.remaining - 1),
        //                 waiting: battle_effect.waiting,
        //                 effect: battle_effect.effect),
        //         // unreachable but makes the compiler happier
        //         (_, _) => null,
        //     };
        // }

    }
}




        /// <summary>
        /// Update the remaining and waiting fields of a BattleEffect.<br/>
        /// Note: 
        ///     all fields are required, `remaining` being nullable does not
        ///     mean it is optional, if a `null` value is provided the object
        ///     will be updated accordingly
        /// </summary>
        // private static BattleEffect UpdateTemporality(
        //     BattleEffect previous,
        //     byte? remaining,
        //     byte waiting)
        // {
        //     var updated = new BattleEffect(
        //         id: previous.id,
        //         iteration: previous.iteration,
        //         caster_copy: previous.caster_copy,
        //         remaining: remaining,
        //         waiting: waiting,
        //         effect: previous.effect);
        //     return updated;
        // }
        