﻿using shad.games.waterblue.lib;
using System;
using System.Linq;
using System.Collections.Immutable;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.lib.pipelines;

namespace shad.games.waterblue.battlecore.services
{

#pragma warning disable IDE0018 // inline 'out' declarations

    // Dev note:
    // This class handles the game logic whereas Model.BattlefieldExtensions
    // handles the boilerplate.
    // This class should propose methods handling functional data flows through
    // Model.BattlefieldExtensions 'functional blocks' methods, and should not
    // implement treatment based on System.Collections, System.Linq, etc.

    /// <summary>
    /// Proposes public methods for updating a Battlefield object when some 
    /// game logic is involved (else see BattlefieldExtensions).
    /// </summary>
    public static class Battlecore
    {
        public static bool CanSwapIn(
            this Battlefield battlefield,
            PartyPosition party_position)
        {
            return !battlefield[party_position].IsKnockedOut();
        }

        private static (ushort damage, TypeEffectiveness move_type_effectiveness) CalculateDamage(
            BattleMonster caster,
            BattleMonster target,
            DamageComponent damage_component)
        {
            var move_power = damage_component.power;
            var move_type = damage_component.type;
            var caster_level = caster.monster.level;
            var caster_attack = caster.monster.species.basestats.attack;
            var target_defense = target.monster.species.basestats.defense;
            var move_type_effectiveness = model.Type.GetTypeEffectiveness(
                offensive_type: move_type,
                defensive_type: target.monster.species.type_1);
            move_type_effectiveness += model.Type.GetTypeEffectiveness(
                offensive_type: move_type,
                defensive_type: target.monster.species.type_2);

            var damage = PipelineBuilder.Seed
                // base damage formula
                .Bind(() => (((2 * (float)caster_level / 5) + 2) * move_power * ((float)caster_attack / target_defense) / 50) + 2)
                // STAB
                .Bind(_damage => _ApplySameAttackTypeBonusModifier(
                    damage: _damage,
                    move_type: move_type,
                    caster: caster.monster))
                // type modifier
                .Bind(_damage => _ApplyTypeModifier(
                    damage: _damage,
                    type_effectiveness: move_type_effectiveness))
                // float to ushort - anything else than pure 0 (immunity) should be rounded to 1 or more
                .Bind(_floatdamage => (ushort)Math.Ceiling(_floatdamage))
                    // should never fail
                .Challenge();
            return (damage, move_type_effectiveness);

            static float _ApplySameAttackTypeBonusModifier(
                float damage,
                model.Type move_type, 
                Monster caster)
            {
                var is_move_stabbed = caster.species.type_1 == move_type || caster.species.type_2 == move_type;
                return is_move_stabbed
                    ? 1.25f * damage
                    : damage;
            }

            static float _ApplyTypeModifier(
                float damage,
                TypeEffectiveness type_effectiveness)
            {
                var type_effectiveness_factor = type_effectiveness.value switch
                {
                    TypeEffectivenessEnum.Immune    => 0.00f,
                    TypeEffectivenessEnum.Neutral   => 1.00f,
                    TypeEffectivenessEnum.Effective => 1.50f,
                    TypeEffectivenessEnum.Resistant => 0.66f,
                    _                               => throw new Exception(),
                };
                return damage * (float)Math.Pow(type_effectiveness_factor, type_effectiveness.stacks);
            }
        }

        internal static int GetPriority(
            this Battlefield battlefield,
            FieldPosition field_position)
        {
            return battlefield[field_position].monster.species.basestats.speed;
        }

        /// <summary>
        /// Returns the target(s) of the given move considering the given battlefield.</br>
        /// This function does not check that the move can be cast, use CanCast before.
        /// </summary>
        internal static ImmutableArray<FieldPosition> GetMoveTargets(
            this Battlefield battlefield,
            Move move,
            FieldPosition caster_position,
            FieldPosition? target_position)
        {
            return move.move_target_type.value switch
            {
                MoveTargetTypeEnum.ChoiceBattlefieldSingleExceptCaster
                    when target_position != null 
                    => _Enclose(target_position.Value),
                MoveTargetTypeEnum.FixedSelf
                    => _Enclose(caster_position),
                MoveTargetTypeEnum.FixedBattlefieldAllExceptCaster
                    => _GetFixedBattlefieldAllExceptCaster(battlefield, caster_position),
                MoveTargetTypeEnum.FixedBattlefieldSideOpponent
                    => _GetFixedBattlefieldSideOpponent(battlefield, caster_position),
                MoveTargetTypeEnum.FixedBattlefieldAll
                    => _GetFixedBattlefieldAll(battlefield),
                MoveTargetTypeEnum.NotApplicable
                    => ImmutableArray<FieldPosition>.Empty,
                _
                    => ImmutableArray<FieldPosition>.Empty,
            };

            static ImmutableArray<T> _Enclose<T>(T value)
            {
                return ImmutableArray.CreateRange(new[] { value });
            }

            static ImmutableArray<FieldPosition> _GetFixedBattlefieldAllExceptCaster(
                Battlefield battlefield,
                FieldPosition caster_position)
            {
                var builder = ImmutableArray.CreateBuilder<FieldPosition>();
                foreach (var _team in battlefield.field_monsters.Keys)
                {
                    for (byte i = 0; i < battlefield.field_monsters[_team].Length; i++)
                    {
                        var is_not_caster = _team != caster_position.team || i != caster_position.offset;
                        var is_not_ko = !battlefield.field_monsters[_team][i].IsKnockedOut();
                        if (is_not_caster && is_not_ko)
                        {
                            builder.Add(FieldPosition.Create(_team, i));
                        }
                    }
                }
                return builder.ToImmutable();
            }

            static ImmutableArray<FieldPosition> _GetFixedBattlefieldSideOpponent(
                Battlefield battlefield,
                FieldPosition caster_position)
            {
                var team = TeamColour.GetOpposite(caster_position.team);
                var builder = ImmutableArray.CreateBuilder<FieldPosition>();
                for (byte i = 0; i < battlefield.field_monsters[team].Length; i++)
                {
                    if (!battlefield.field_monsters[team][i].IsKnockedOut())
                    {
                        builder.Add(FieldPosition.Create(team, i));
                    }
                }
                return builder.ToImmutable();
            }

            static ImmutableArray<FieldPosition> _GetFixedBattlefieldAll(
                Battlefield battlefield)
            {
                var builder = ImmutableArray.CreateBuilder<FieldPosition>();
                foreach (var _team in battlefield.field_monsters.Keys)
                {
                    for (byte i = 0; i < battlefield.field_monsters[_team].Length; i++)
                    {
                        if (!battlefield.field_monsters[_team][i].IsKnockedOut())
                        {
                            builder.Add(FieldPosition.Create(_team, i));
                        }
                    }
                }
                return builder.ToImmutable();
            }
        }

        /* BattleMonster */

        public static bool IsKnockedOut(
            this BattleMonster monster)
        {
            return monster.health == 0;
        }

        /* Battlefield */

        // Checkers

        public static bool CanTrigger(
            this Battlefield battlefield,
            BattleEffectId effect_id)
        {
            var can_trigger = PipelineBuilder.Seed
                .Bind(() => battlefield.TryGetComponent<WarmupComponent>(effect_id))
                .Bind(warmup_component => warmup_component.waiting == 0)
                .Resolve(false);
            return can_trigger;
        }

        public static bool CanSwap(
            this Battlefield battlefield,
            SwapAction swap_action)
        {
            var outgoing_position_pipeline = PipelineBuilder.Seed
                .Bind(() => battlefield.TryGetMonsterFieldPosition(swap_action.outgoing));
            var incoming_position_pipeline = PipelineBuilder.Seed
                .Bind(() => battlefield.TryGetMonsterPartyPosition(swap_action.incoming));
            var can_swap = PipelineBuilder.Seed
                .Join(
                    selector1: () => outgoing_position_pipeline,
                    selector2: () => incoming_position_pipeline,
                    func: (outgoing_position, incoming_position) =>
                        outgoing_position.team == incoming_position.team
                        && battlefield.CanSwapIn(incoming_position))
                .Resolve(false);
            return can_swap;
        }

        public static bool CanCast(
            this Battlefield battlefield,
            MoveAction move_action)
        {
            var caster_position_pipeline = PipelineBuilder.Seed
                .Bind(() => battlefield.TryGetMonsterFieldPosition(move_action.caster_id))
                .Ensure(caster_position => !battlefield[caster_position].IsKnockedOut());

            var target = PipelineBuilder.Seed
                .Bind(() => move_action.target_position)
                .Bind(_targetposition => battlefield[_targetposition])
                .Ensure(_target => _target.id != move_action.caster_id)
                .Ensure(_target => !_target.IsKnockedOut())
                .Resolve(selector: _target => (BattleMonster?)_target, default_value: null);

            var move_pipeline = caster_position_pipeline
                .Bind(_caster_position => battlefield[_caster_position].TryGetMove(move_action.move_id));

            var can_cast = PipelineBuilder.Seed
                .Join(
                    selector1: () => caster_position_pipeline,
                    selector2: () => move_pipeline,
                    func: (_caster_position, _move) => _IsMoveActionValidForMoveTargetType(
                        battlefield: battlefield,
                        caster_position: _caster_position,
                        move: _move,
                        target: target))
                .Resolve(false);

            return can_cast;

            static bool _IsMoveActionValidForMoveTargetType(
                Battlefield battlefield,
                FieldPosition caster_position,
                BattleMove move,
                BattleMonster? target)
            {
                var caster = battlefield[caster_position];
                var caster_team = caster_position.team;
                var opponent_team = TeamColour.GetOpposite(caster_team);
                var target_type = move.move.move_target_type;
                // could be lazy
                var opponent_field_not_ko_monster_ids = battlefield
                    .field_monsters[opponent_team]
                    .Where(_monster => !_monster.IsKnockedOut())
                    .Select(_monster => _monster.id)
                    .ToImmutableArray();
                // could be lazy
                var player_field_not_ko_monster_ids_except_caster = battlefield
                    .field_monsters[caster_team]
                    .Where(_monster => _monster.id != caster.id)
                    .Where(_monster => !_monster.IsKnockedOut())
                    .Select(_monster => _monster.id)
                    .ToImmutableArray();
                var can_cast = target_type.value switch
                {
                    // global moves, e.g., terrain moves
                    MoveTargetTypeEnum.NotApplicable => true,
                    // the previous pipelines put the target variable to null if target == caster or target == knocked out
                    MoveTargetTypeEnum.ChoiceBattlefieldSingleExceptCaster => target != null,
                    // always true: the caster is not KO (checked earlier)
                    MoveTargetTypeEnum.FixedSelf => true,
                    // at least one monster on the opponent's field side should not be KO
                    MoveTargetTypeEnum.FixedBattlefieldSideOpponent =>
                        opponent_field_not_ko_monster_ids.Length > 0,
                    // at least one monster on the field should not be KO (should always be true, else the cast would have won)
                    MoveTargetTypeEnum.FixedBattlefieldAllExceptCaster =>
                        opponent_field_not_ko_monster_ids.Length > 0 || player_field_not_ko_monster_ids_except_caster.Length > 0,
                    // always true: at least the caster is not KO (checked earlier)
                    MoveTargetTypeEnum.FixedBattlefieldAll => true,
                    _ => throw new NotImplementedException("Unexpected MoveTargetTypeEnum value in Battlecore.CanCast()"),
                };
                return can_cast;
            }
        }

        // Getters

        public static ImmutableArray<BattleEffectId> GetPendingEffectsForTurnStage(
            this Battlefield battlefield,
            EffectRecurrence turn_stage)
        {
            var party_battlemonster_ids = battlefield.party_monsters.Values
                .SelectMany(x => x)
                .Select(_monster => (BattleMonsterId?)_monster.id)
                .ToHashSet();
            var effects = battlefield.battle_effects
                // filter on recurrences
                .Where(_effect => battlefield.TryGetComponent<RecurrenceComponent>(_effect.id)?.recurrence == turn_stage)
                // format
                .Select(_effect => (
                    target: battlefield.TryGetComponent<TargetMonsterComponent>(_effect.id)?.target_id,
                    effect: _effect.id))
                // remove entries with explicit targets that are not on the battlefield
                .Where(tuple => !party_battlemonster_ids.Contains(tuple.target))
                // format
                .Select(tuple => tuple.effect)
                .ToImmutableArray();
            return effects;
        }

        public static ImmutableArray<BattleEffect> GetBattleMonsterDebuffs(
            this Battlefield battlefield,
            BattleMonsterId battle_monster_id)
        {
            var debuffs = battlefield.battle_effects
                .Where(_effect => battlefield.TryGetComponent<TargetMonsterComponent>(_effect.id)?.target_id == battle_monster_id)
                .ToImmutableArray();
            return debuffs;
        }

        // Is this encapsulation relevant?
        public static ImmutableArray<BattleMonsterId> GetEligibleBattleMonsterIdsForSwapIn(
            this Battlefield battlefield,
            TeamColour team_colour)
        {
            return battlefield.party_monsters[team_colour]
                .Select((_, _i) => PartyPosition.Create(team_colour, (byte)_i))
                .Where(_party_position => battlefield.CanSwapIn(_party_position))
                .Select(_party_position => battlefield[_party_position].id)
                .ToImmutableArray();
        }

        public static ImmutableArray<TeamColour> GetLosers(
            this Battlefield battlefield)
        {
            var losers = TeamColour
                .GetValues()
                // keep teams for which all field monsters are KO
                .Where(_team => _AreAllMonstersKnockedOut(battlefield.field_monsters[_team]))
                // then keep teams for which all party monsters are KO
                .Where(_team => _AreAllMonstersKnockedOut(battlefield.party_monsters[_team]))
                .ToImmutableArray();
            return losers;

            static bool _AreAllMonstersKnockedOut(ImmutableArray<BattleMonster> monsters)
            {
                return monsters.IsDefaultOrEmpty || monsters.All(_monster => _monster.IsKnockedOut());
            }
        }

        /// <summary>
        /// Get the priority swap from the list considering the given 
        /// battlefield state. <br/>
        /// Returns null if 
        ///     - the given swap list is empty
        ///     - none of the given swaps are relevant
        /// </summary>
        public static SwapAction? GetPrioritySwap(
            this Battlefield battlefield,
            ImmutableArray<SwapAction> swaps)
        {
            var priority_swap = swaps
                .Select(_swap => _GetSwapPriorityTuple(
                    battlefield: battlefield, 
                    swap_action: _swap))
                .Where(_tuple => _tuple != null)
                .OrderByDescending(_tuple => _tuple!.Value.priority_value)
                .FirstOrDefault();

            return priority_swap?.priority_swap;
            
            static (SwapAction priority_swap, int priority_value)? _GetSwapPriorityTuple(
                Battlefield battlefield,
                SwapAction swap_action)
            {
                var priority_tuple = PipelineBuilder.Seed
                    .Bind(() => battlefield.TryGetMonsterFieldPosition(monster_id: swap_action.outgoing))
                    .Ensure(_ => battlefield.CanSwap(swap_action))
                    .Bind(_outgoing_position => (
                        priority_swap: swap_action,
                        priority_value: battlefield.GetPriority(_outgoing_position)))
                    .Resolve(selector: _tuple => ((SwapAction, int)?)_tuple, default_value: null);
                return priority_tuple;
            }
        }

        public static BattleEffectId? GetPriorityBattleEffect(
            this Battlefield battlefield,
            ImmutableArray<BattleEffectId> effect_ids)
        {
            var priority_effect = effect_ids
                .Select(_effect_id => _TryGetTriggerableEffect(battlefield, _effect_id))
                .Where(_effect => _effect != null)
                .OrderByDescending(_effect => _effect!.Value.priority)
                .FirstOrDefault();

            return priority_effect?.id;

            static BattleEffect? _TryGetTriggerableEffect(
                Battlefield battlefield,
                BattleEffectId effect_id)
            {
                var effect = PipelineBuilder.Seed
                    .Bind(() => battlefield.TryGetBattleEffect(effect_id))
                    .Ensure(_ => battlefield.CanTrigger(effect_id))
                    .Resolve(selector: _effect => (BattleEffect?)_effect, default_value: null);
                return effect;
            }
        }

        public static MoveAction? GetPriorityBattleMove(
            this Battlefield battlefield,
            ImmutableArray<MoveAction> move_actions)
        {
            var priority_move = move_actions
                .Select(_moveaction => _GetMovePriorityTuple(battlefield, _moveaction))
                .Where(_priority_tuple => _priority_tuple != null)
                .OrderByDescending(_priority_tuple => _priority_tuple!.Value.priority_value)
                .FirstOrDefault();

            return priority_move?.move_action;

            static (MoveAction move_action, int priority_value)? _GetMovePriorityTuple(
                Battlefield battlefield,
                MoveAction move_action)
            {
                var priority_tuple = PipelineBuilder.Seed
                    .Bind(() => battlefield.TryGetMonsterFieldPosition(move_action.caster_id))
                    .Ensure(_ => battlefield.CanCast(move_action))
                    .Bind(_fieldposition => (
                        move_action,
                        priority_value: battlefield.GetPriority(_fieldposition)))
                    .Resolve(selector: _tuple => ((MoveAction, int)?)_tuple, default_value: null);
                return priority_tuple;
            }
        }

        // Updators

        public static Battlefield DoTickEffect(
            this Battlefield battlefield,
            BattleEffectId effect_id)
        {
            /* Trigger the effect if relevant */

            var target_position_pipeline = PipelineBuilder.Seed
                .Ensure(() => battlefield.CanTrigger(effect_id))
                .Bind(() => battlefield.TryGetComponent<TargetMonsterComponent>(effect_id))
                .Bind(_target_component => battlefield.TryGetMonsterFieldPosition(_target_component.target_id));

            var attached_effect_pipeline = target_position_pipeline
                .Bind(_target_position => battlefield
                    .GetBattleMonsterDebuffs(battlefield[_target_position].id)
                    .Select(_effect => (BattleEffect?)_effect)
                    .FirstOrDefault(_effect => _effect!.Value.id == effect_id));

            var consolidated_caster_pipeline = PipelineBuilder.Seed
                .Join(() => attached_effect_pipeline, 
                    func: _attached_effect => _attached_effect.caster_copy.Invoke());

            var consolidated_target_pipeline = PipelineBuilder.Seed
                .Ensure(() => consolidated_caster_pipeline) 
                .Join(() => target_position_pipeline, 
                    func: _target_position => battlefield[_target_position]);

            /* Apply damage for any damage component */
            var updated_battlefield_pipeline = PipelineBuilder.Seed
                .Join(
                    selector1: () => attached_effect_pipeline,
                    func: (_attached_effect) => _attached_effect.effect.default_components
                        .FirstOrDefault(_component => _component is DamageComponent)
                        .ApplyFunc(_component => (DamageComponent?)_component))  
                .Ensure(_damagecomponent => _damagecomponent.power > 0)
                /* Calculate the mitigation */
                .Join(
                    selector1: (_) => consolidated_caster_pipeline,
                    selector2: (_) => consolidated_target_pipeline,
                    func: (_damagecomp, _consolidated_caster, _consolidated_target) => Battlecore
                        .CalculateDamage(
                            caster: _consolidated_caster,
                            target: _consolidated_target,
                            damage_component: _damagecomp)
                        .damage)
                /* Apply the mitigation */
                .Join(
                    selector1: (_) => target_position_pipeline, 
                    func: (_mitigation, _target_position) => _ApplyMitigation(
                        battlefield: battlefield,
                        target_position: _target_position,
                        mitigation: _mitigation));

            /* Tick the effect if needed whether a triggering occured previously or not */

            var final_battlefield = updated_battlefield_pipeline
                .Catch(battlefield)
                .Bind(_updated_battlefield => _TickEffect(
                    battlefield: _updated_battlefield,
                    battle_effect_id: effect_id))
                .Bind(_updated_battlefield => _RemoveObsoleteEffects(battlefield: _updated_battlefield))
                .Challenge();

            return final_battlefield;

            /* Functions */

            static Battlefield _ApplyMitigation(
                Battlefield battlefield,
                FieldPosition target_position,
                ushort mitigation)
            {
                var target_battle_monster = battlefield[target_position];
                target_battle_monster = BattleMonster.DealDamage(
                        previous: target_battle_monster, 
                        damage: mitigation)
                    .monster;
                battlefield = Battlefield.UpdateBattleMonster(
                    previous: battlefield,
                    position: target_position,
                    new_value: target_battle_monster);
                return battlefield;
            }

            static Battlefield _TickEffect(
                Battlefield battlefield,
                BattleEffectId battle_effect_id)
            {
                // returns a new battlefield with decreased warmup counter if relevant
                var battlefield_with_decreased_warmup_component_pipeline = PipelineBuilder.Seed
                    .Bind(() => battlefield.TryGetComponent<WarmupComponent>(battle_effect_id))
                    .Ensure(_warmup_comp => _warmup_comp.waiting > 0)
                    .Bind(_warmup_comp => WarmupComponent.Decrease(_warmup_comp))
                    .Bind(_warmup_comp => Battlefield.SetComponent(
                        battlefield: battlefield,
                        effect_id: battle_effect_id,
                        component: _warmup_comp));

                // returns a new battlefield with decreased duration counter if relevant (lower priority than the warmup pipeline: both should not decrease at the same time)
                var battlefield_with_decreased_duration_component_pipeline = PipelineBuilder.Seed
                    .Ensure(dependency: () => battlefield_with_decreased_warmup_component_pipeline, is_error_expected: true)
                    .Bind(() => battlefield.TryGetComponent<DurationComponent>(battle_effect_id))
                    .Ensure(_duration_comp => _duration_comp.remaining > 0)
                    .Bind(_duration_comp => DurationComponent.Decrease(_duration_comp))
                    .Bind(_duration_comp => Battlefield.SetComponent(
                        battlefield: battlefield,
                        effect_id: battle_effect_id,
                        component: _duration_comp));

                // decreases the warmup counter if any, else decreases the duration counter if any, else do nothing
                var final_battlefield = 
                    battlefield_with_decreased_warmup_component_pipeline
                    .Catch(() => battlefield_with_decreased_duration_component_pipeline)
                    .Resolve(battlefield);
                return final_battlefield;
            }

            static Battlefield _RemoveObsoleteEffects(
                Battlefield battlefield)
            {
                return PipelineBuilder.Seed
                    .Loop(
                        initial_value_selector: () => battlefield,
                        collection_selector: () => battlefield.effects_components,
                        func: (_battlefield, _components_kvp) => PipelineBuilder.Seed
                            .Bind(() => (DurationComponent?)_components_kvp.Value.FirstOrDefault(_component => _component is DurationComponent))
                            .Ensure(_duration_comp => _duration_comp.remaining == 0)
                            .Bind(_duration_comp => Battlefield.RemoveBattleEffect(
                                battlefield: _battlefield, 
                                effect_id: _components_kvp.Key))
                            .Resolve(_battlefield))
                    .Challenge();
            }
        }

        public static MoveActionLog DoCast(
            this Battlefield battlefield,
            MoveAction move_action)
        {
            var caster_position_pipeline = PipelineBuilder.Seed
                .Bind(() => battlefield.TryGetMonsterFieldPosition(move_action.caster_id));
            var consolidated_caster_pipeline = caster_position_pipeline
                .Bind(_position => battlefield[_position]);
            var casted_move_pipeline = consolidated_caster_pipeline
                .Bind(_caster => _caster.TryGetMove(move_action.move_id));
            var move_targets_pipeline = PipelineBuilder.Seed
                .Join(
                    selector1: () => caster_position_pipeline,
                    selector2: () => casted_move_pipeline,
                    func: (_caster_position, _casted_move) => PipelineBuilder.Seed
                        .Bind(() => battlefield.GetMoveTargets(
                            move: _casted_move.move,
                            caster_position: _caster_position,
                            target_position: move_action.target_position))
                        .Ensure(_target_positions => !_target_positions.IsDefaultOrEmpty));
            var move_action_log = PipelineBuilder.Seed
                .Ensure(() => consolidated_caster_pipeline)
                .Ensure(() => move_targets_pipeline)
                // for each target
                .Loop(
                    initial_value_selector: () => MoveActionLog.Create(
                        move_action: move_action,
                        battlefield_before: battlefield),
                    collection_selector: () => move_targets_pipeline.Challenge(),
                    func: (_log_for_target, _targetposition) => PipelineBuilder.Seed
                        .Ensure(() => casted_move_pipeline)
                        // for each effect: apply the effect on the target
                        .Loop(
                            initial_value_selector: () => _log_for_target,
                            collection_selector: () => casted_move_pipeline.Challenge().move.effects, 
                            func: (_log_for_effect, _effect) => PipelineBuilder.Seed
                                // direct damage / healing / shielding / etc. pipeline
                                .Bind(() => _ApplyImmediateMitigationToTargetIfRelevant(
                                    current_move_action_log: _log_for_effect,
                                    caster: consolidated_caster_pipeline.Challenge(),
                                    target_position: _targetposition,
                                    effect: _effect))
                                // debuff application (poison, burn, etc.) pipeline
                                .Bind(_log => _AttachDebuffToTargetIfRelevant(
                                    current_move_action_log: _log,
                                    caster: consolidated_caster_pipeline.Challenge(),
                                    target_position: _targetposition,
                                    effect: _effect))
                                .Challenge())
                        .Challenge())
                .Resolve(MoveActionLog.Create(
                    move_action: move_action,
                    battlefield_before: battlefield));

            return move_action_log;

            static MoveActionLog _AttachDebuffToTargetIfRelevant(
                MoveActionLog current_move_action_log,
                BattleMonster caster,
                FieldPosition target_position,
                Effect effect)
            {
                var updated_move_action_log = PipelineBuilder.Seed
                    .EnsureMultiple(
                        collection_selector: () => effect.default_components,
                        required_true_for_all: new Predicate<IComponent>[]
                        {
                            _component => !EffectComponentPredicate.HasEffectOutlastedItsDuration(_component),
                        },
                        required_true_for_any: new Predicate<IComponent>[]
                        {
                            EffectComponentPredicate.MustEffectBeAttached,
                            EffectComponentPredicate.HasEffectDefinedBehaviourWhenAttacheeSwapsOut,
                        })
                    .Bind(() =>
                    {
                        var effect_to_attach = BattleEffect.Create(
                            effect: effect,
                            caster_copy: () => caster,
                            battlefield_iteration: current_move_action_log.battlefield_after.iteration);
                        var updated_battlefield = Battlefield.AddBattleEffect(
                            previous: current_move_action_log.battlefield_after,
                            target_position: target_position,
                            effect: effect_to_attach);
                        return MoveActionLog.UpdateBattlefieldAfter(
                            previous: current_move_action_log,
                            updated_battlefield: updated_battlefield);
                    })
                    .Resolve(current_move_action_log);
                return updated_move_action_log;
            }

            static MoveActionLog _ApplyImmediateMitigationToTargetIfRelevant(
                MoveActionLog current_move_action_log,
                BattleMonster caster,
                FieldPosition target_position,
                Effect effect)
            {
                var battlefield = current_move_action_log.battlefield_after;
                var target = battlefield[target_position];
                var updated_move_action_log = PipelineBuilder.Seed
                    .EnsureAny(collection_selector: () => effect.default_components, EffectComponentPredicate.IsEffectImmediate)
                    .Bind(() => effect.default_components
                        .FirstOrDefault(_component => _component is DamageComponent)
                        .ApplyFunc(_component => (DamageComponent?)_component))
                    .Ensure(_damagecomponent => _damagecomponent.power > 0)
                    .Bind(_damagecomponent => Battlecore.CalculateDamage(
                        caster: caster,
                        target: target,
                        damage_component: _damagecomponent))
                    .Bind(_damage_and_effectiveness => BattleMonster.DealDamage(
                        previous: battlefield[target_position],
                        type_effectiveness: _damage_and_effectiveness.move_type_effectiveness,
                        damage: _damage_and_effectiveness.damage))
                    .Bind(_updated_target_tuple =>
                    {
                        var updated_battlefield = Battlefield.UpdateBattleMonster(
                            previous: battlefield,
                            position: target_position,
                            new_value: _updated_target_tuple.monster);
                        return MoveActionLog.AddMitigationLog(
                            previous: current_move_action_log,
                            mitigation_log: _updated_target_tuple.log,
                            updated_battlefield: updated_battlefield);
                    })
                    .Resolve(current_move_action_log);
                return updated_move_action_log;
            }
        }

        public static Battlefield DoSwap(
            this Battlefield battlefield,
            SwapAction swap_action)
        {
            var outgoing_position_pipeline = PipelineBuilder.Seed
                .Ensure(() => Battlecore.CanSwap(
                    battlefield: battlefield,
                    swap_action: swap_action))
                .Bind(() => battlefield.TryGetMonsterFieldPosition(swap_action.outgoing));

            var incoming_position_pipeline = PipelineBuilder.Seed
                .Ensure(() => outgoing_position_pipeline)
                .Bind(() => battlefield.TryGetMonsterPartyPosition(swap_action.incoming));

            var final_battlefield = PipelineBuilder.Seed

                // TODO update the monster's effects depending on what should go away when swapping out, what should stick, what should apply when swapping in, etc.
                // TODO trigger whatever should happen when monsters swap out and/or swap in other than effects attachment (spikes damage, etc.)

                // set the monster's value at the outgoing party monster's position to the copied incoming monster value
                .Join(
                    selector1: () => outgoing_position_pipeline,
                    selector2: () => incoming_position_pipeline,
                    func: (_outgoing_position, _incoming_position) => Battlefield.UpdateBattleMonster(
                        previous: battlefield,
                        position: _outgoing_position,
                        new_value: battlefield[_incoming_position]))
                .Join(
                    selector1: _ => outgoing_position_pipeline,
                    selector2: _ => incoming_position_pipeline,
                    func: (_updated_battlefield, _outgoing_position, _incoming_position) => Battlefield.UpdateBattleMonster(
                        previous: _updated_battlefield,
                        position: _incoming_position,
                        new_value: battlefield[_outgoing_position]))
                .Resolve(battlefield);

            return final_battlefield;
        }
    }
}