﻿using System.Globalization;

namespace shad.games.waterblue.lib.TextUtils
{
    public static class TextUtils
    {
        public static string ToTitleCase(this string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str);
        }
    }
}
