using System;

namespace shad.games.waterblue.lib.pipelines
{

    public readonly struct PipelineResult
    {
        internal readonly int total_depth;

        internal readonly int last_success_depth;

        internal readonly PipelineError? error;

        private PipelineResult(
            int total_depth,
            int last_success_depth,
            PipelineError? error)
        {
            this.total_depth = total_depth;
            this.last_success_depth = last_success_depth;
            this.error = error;
        }

        internal static PipelineResult PushSuccess(
            PipelineResult previous)
        {
            return new(
                total_depth: previous.total_depth + 1,
                last_success_depth: previous.last_success_depth + 1,
                error: null);
        }

        /// <summary>
        /// Building a failure
        /// </summary>
        internal static PipelineResult PushFailure(
            PipelineResult previous,
            PipelineError error = PipelineError.Error)
        {
            return new(
                total_depth: previous.total_depth + 1,
                last_success_depth: previous.last_success_depth,
                error: error);
        }

        /// <summary>
        /// Propagating a failure
        /// </summary>
        internal static PipelineResult PropagateFailure(
            PipelineResult previous)
        {
            if (previous.error == null)
            {
                throw new ArgumentException("Trying to propagate a failure from a successful pipeline");
            }
            return new(
                total_depth: previous.total_depth + 1,
                last_success_depth: previous.last_success_depth,
                error: previous.error);
        }
        
    }

    public readonly struct PipelineResult<T> where T : struct
    {
        internal readonly PipelineResult info;

        internal readonly T value;

        internal PipelineResult(
            PipelineResult info,
            T value = default)
        {
            this.info = info;
            this.value = value;
        }

        internal static PipelineResult<T> InitFromNullable(
            T? nullable_value)
        {
            var info = nullable_value == null
                ? PipelineResult.PushFailure(new PipelineResult())
                : PipelineResult.PushSuccess(new PipelineResult());
            return new(
                info: info, 
                value: nullable_value ?? default);
        }

        internal static PipelineResult<T> PushSuccess(
            PipelineResult<T> previous,
            T value)
        {
            var updated_info = PipelineResult.PushSuccess(previous: previous.info);
            return new(
                info: updated_info,
                value: value);
        }

        internal static PipelineResult<T> PushFailure(
            PipelineResult<T> previous,
            PipelineError error = PipelineError.Error)
        {
            var updated_info = PipelineResult.PushFailure(previous: previous.info, error: error);
            return new(
                info: updated_info,
                value: default);
        }

        internal static PipelineResult<T> PropagateSuccess(
            PipelineResult<T> previous)
        {
            if (previous.info.error != null)
            {
                throw new ArgumentException("Trying to propagate a success from a failing pipeline");
            }
            var updated_info = PipelineResult.PushSuccess(previous: previous.info);
            return new(
                info: updated_info,
                value: previous.value);
        }

        internal static PipelineResult<T> PropagateFailure(
            PipelineResult<T> previous)
        {
            if (previous.info.error == null)
            {
                throw new ArgumentException("Trying to propagate a failure from a successful pipeline");
            }
            var updated_info = PipelineResult.PropagateFailure(previous: previous.info);
            return new(
                info: updated_info,
                value: default);
        }
    }
}