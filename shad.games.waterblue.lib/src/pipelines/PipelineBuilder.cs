using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

// do not distinguish bind/fold and join/mesh?
// pipeline allwos ensurances without a type, into seed, to rework battlecore.tickeffect

namespace shad.games.waterblue.lib.pipelines
{
    /// <summary>
    /// <para>Utility class to build up pipelines.</para>
    /// <para>A pipeline can be seen as sequence of unitary treatments which may fail,
    /// and only execute as long as the pipeline is not failing. It holds a result as long as it is not failing, else holds an error.</para>
    /// <para>
    /// The main objective of pipelines is to slice a big treatment into understandable steps and typically avoid choosing between:
    /// <list type="bullet">
    ///     <item>writing optimised but harder to read code whose data flow is shattered with <c>if</c> statements handling early returns or blocks of unexecuted code</item>
    ///     <item>writing unoptimised but easier to read code which would execute unnecessery instructions to enhance readability</item>
    /// </list>
    /// </para>
    /// <para>
    ///     Pipeline treatments can fail and make the pipeline unsuccessful either by returning a PipelineResult failure or by returning a null value when a Pipeline of type T
    ///     applies a treatment which returns a type T?.
    /// </para>
    /// <para>
    ///     For greater interoperability with existing code, functions which would fail if a given PipelineResult<T> fails are overloaded with Nullable<T> equivalents, null meaning failure.
    /// </para>
    /// <para>
    /// This utility class provides different families of functions:
    /// <list type="bullet">
    /// <item>
    ///     <term>Ensurance</term>
    ///     <description>makes the pipeline fail if the given condition is not satisfied</description>
    /// </item>
    /// <item>
    ///     <term>Binding</term>
    ///     <description>binds a treatment which modifies the value of a successful pipeline</description>
    /// </item>
    /// <item>
    ///     <term>Joining</term>
    ///     <description>similar to a binding, using external successful pipelines</description>
    /// </item>
    /// <item>
    ///     <term>Forking</term>
    ///     <description>executes a side-effect treatment in a successful pipeline without altering its value (can still make the pipeline fail in some cases)</description>
    /// </item>
    /// <item>
    ///     <term>Catching</term>
    ///     <description>similar to a binding, but on a failing pipeline to try to make it successful again</description>
    /// </item>
    /// <item>
    ///     <term>Resolution, Handling, Challenging, etc.</term>
    ///     <description>proposes different ways to end a pipeline and get its value when relevant</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    public static class PipelineBuilder
    {
        // TODO This class' performances could be enhanced by avoiding systematic encapsulations of non-typed non-'from pipelineresult' functions
        // but for now encapsulation is fine

        public static readonly PipelineResult Seed = new();

        /* Ensurances - fail the pipeline when conditions are unmatched */

        // untyped, from nullable
        /// <summary>
        /// Dereferences the given dependency if the pipeline is successfull
        /// then fails if its nullable state does not match the given expectation.
        /// </summary>
        public static PipelineResult Ensure<T1>(
            this PipelineResult input,
            [NotNull] Func<T1?> dependency,
            bool is_error_expected = false)
            where T1 : struct
        {
            var result = PipelineBuilder.Ensure(
                input: new PipelineResult<bool>(input),
                dependency: _ => PipelineResult<T1>.InitFromNullable(dependency.Invoke()),
                is_error_expected_func: _ => is_error_expected);
            return result.info;
        }

        // untyped, from pipelineresult
        /// <summary>
        /// Dereferences the given dependency if the pipeline is successfull
        /// then fails if its error state does not match the given expectation.
        /// </summary>
        public static PipelineResult Ensure<T1>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> dependency,
            bool is_error_expected = false)
            where T1 : struct
        {
            var result = PipelineBuilder.Ensure(
                input: new PipelineResult<bool>(input),
                dependency: _ => dependency.Invoke(),
                is_error_expected_func: _ => is_error_expected);
            return result.info;
        }

        // untyped, from boolean
        /// <summary>
        /// Fails if the given function returns false.
        /// </summary>
        public static PipelineResult Ensure(
            this PipelineResult input,
            [NotNull] Func<bool> func,
            bool is_false_expected = false)
        {
            var result = PipelineBuilder.Ensure(
                input: new PipelineResult<bool>(input),
                predicate: (_) => func.Invoke(),
                is_false_expected_func: (_) => is_false_expected);
            return result.info;
        }

        // typed, from nullable
        /// <summary>
        /// Dereferences the given dependency if the pipeline is successfull
        /// then fails if its null state does not match the given expectation.
        /// </summary>
        public static PipelineResult<TInOut> Ensure<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] Func<TInOut, T1?> dependency,
            Func<TInOut, bool>? is_error_expected_func = null)
            where TInOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Ensure(
                input: input,
                predicate: _tinout => dependency.Invoke(_tinout) != null,
                is_false_expected_func: is_error_expected_func);
            return result;
        }

        // typed, from pipelineresult
        /// <summary>
        /// Dereferences the given dependency if the pipeline is successfull
        /// then fails if its error state does not match the given expectation.
        /// </summary>
        public static PipelineResult<TInOut> Ensure<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] Func<TInOut, PipelineResult<T1>> dependency,
            Func<TInOut, bool>? is_error_expected_func = null)
            where TInOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Ensure(
                input: input,
                predicate: _tinout => dependency.Invoke(_tinout).info.error == null,
                is_false_expected_func: is_error_expected_func);
            return result;
        }

        // typed, from boolean
        /// <summary>
        /// Invokes the given predicate if the pipeline is successfull
        /// then fails if the predicate returns false.
        /// </summary>
        public static PipelineResult<TInOut> Ensure<TInOut>(
            this PipelineResult<TInOut> input,
            [NotNull] Predicate<TInOut> predicate,
            Func<TInOut, bool>? is_false_expected_func = null)
            where TInOut : struct
        {
            if (input.info.error != null)
            {
                return PipelineResult<TInOut>.PropagateFailure(previous: input);
            }
            var is_predicate_false = !predicate.Invoke(input.value);
            var is_false_expected = is_false_expected_func?.Invoke(input.value) ?? false;
            return is_predicate_false ^ is_false_expected
                ? PipelineResult<TInOut>.PushFailure(previous: input)
                : PipelineResult<TInOut>.PropagateSuccess(previous: input);
        }

        // untyped, from boolean
        /// <summary>
        /// Dereferences the given collection if the pipeline is successfull
        /// then fails if any of the predicates returns false for all of the collection's elements.
        /// </summary>
        public static PipelineResult EnsureAny<T1>(
            this PipelineResult input,
            [NotNull] Func<IEnumerable<T1>> collection_selector,
            params Predicate<T1>[] required_true_for_any)
        {
            var result = PipelineBuilder.EnsureAny(
                input: new PipelineResult<bool>(input),
                collection_selector: _ => collection_selector.Invoke(),
                required_true_for_any: required_true_for_any);
            return result.info;
        }

        // typed, from boolean
        /// <summary>
        /// Dereferences the given collection if the pipeline is successfull
        /// then fails as soon as possible if any of the predicates
        /// returns false for all of the collection's elements.
        /// </summary>
        public static PipelineResult<TInOut> EnsureAny<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] Func<TInOut, IEnumerable<T1>> collection_selector,
            params Predicate<T1>[] required_true_for_any)
            where TInOut : struct
        {
            var result = PipelineBuilder.EnsureMultiple(
                input: input,
                collection_selector: collection_selector,
                required_true_for_any: required_true_for_any);
            return result;
        }

        // untyped, from boolean
        /// <summary>
        /// Dereferences the given collection if the pipeline is successfull
        /// then fails as soon as possible if any of the following occurs:
        /// <list>
        /// <item>any of the required_true_for_all predicates returns false for any of the collection's elements</item>
        /// <item>any of the required_true_for_any predicates returns false for all of the collection's elements</item>
        /// </list>
        /// </summary>
        public static PipelineResult EnsureMultiple<T1>(
            this PipelineResult input,
            [NotNull] Func<IEnumerable<T1>> collection_selector,
            IEnumerable<Predicate<T1>>? required_true_for_all = null,
            IEnumerable<Predicate<T1>>? required_true_for_any = null)
        {
            var result = PipelineBuilder.EnsureMultiple(
                input: new PipelineResult<bool>(input),
                collection_selector: _ => collection_selector.Invoke(),
                required_true_for_all: required_true_for_all,
                required_true_for_any: required_true_for_any);
            return result.info;
        }

        // typed, from boolean
        /// <summary>
        /// Dereferences the given collection if the pipeline is successfull
        /// then fails as soon as possible if any of the following occurs:
        /// <list>
        /// <item>any of the required_true_for_all predicates returns false for any of the collection's elements</item>
        /// <item>any of the required_true_for_any predicates returns false for all of the collection's elements</item>
        /// </list>
        /// </summary>
        public static PipelineResult<TInOut> EnsureMultiple<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] Func<TInOut, IEnumerable<T1>> collection_selector,
            IEnumerable<Predicate<T1>>? required_true_for_all = null,
            IEnumerable<Predicate<T1>>? required_true_for_any = null)
            where TInOut : struct
        {
            if (input.info.error != null)
            {
                return PipelineResult<TInOut>.PropagateFailure(previous: input);
            }
            if ((required_true_for_all?.Any() ?? false) || (required_true_for_any?.Any() ?? false))
            {
                var remaining_required_true_for_any = required_true_for_any ?? Enumerable.Empty<Predicate<T1>>();
                foreach (var _element in collection_selector.Invoke(input.value))
                {
                    var check_required_true_for_all = required_true_for_all?.All(_checker => _checker(_element)) ?? true;
                    if (!check_required_true_for_all)
                    {
                        return PipelineResult<TInOut>.PushFailure(previous: input);
                    }
                    remaining_required_true_for_any = remaining_required_true_for_any
                        .Where(_checker => !_checker(_element));
                }
                if (remaining_required_true_for_any.Any())
                {
                    return PipelineResult<TInOut>.PushFailure(previous: input);
                }
            }
            return PipelineResult<TInOut>.PropagateSuccess(previous: input);
        }

        /* Bindings from untyped input - apply treatments on a successful pipeline which may make it fail */

        // safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Cannot fail.
        /// </summary>
        public static PipelineResult<TOut> Bind<TOut>(
            this PipelineResult input,
            [NotNull] Func<TOut> func)
            where TOut : struct
        {
            var result = PipelineBuilder.Bind(
                input: new PipelineResult<TOut>(input),
                func: _ => func.Invoke());
            return result;
        }

        // unsafe from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Fails if the given value is null.
        /// </summary>
        public static PipelineResult<TOut> Bind<TOut>(
            this PipelineResult input,
            [NotNull]Func<TOut?> func)
            where TOut : struct
        {
            var result = PipelineBuilder.Bind(
                input: new PipelineResult<TOut>(input),
                func: _ => func.Invoke());
            return result;
        }

        // unsafe from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Fails if the given pipeline is in failure.
        /// </summary>
        public static PipelineResult<TOut> Bind<TOut>(
            this PipelineResult input,
            [NotNull]Func<PipelineResult<TOut>> func)
            where TOut : struct
        {
            var result = PipelineBuilder.Bind(
                input: new PipelineResult<TOut>(input),
                func: _ => func.Invoke());
            return result;
        }

        /* Bindings from typed input - apply treatments on a successful pipeline which may make it fail */

        // safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Cannot fail.
        /// </summary>
        public static PipelineResult<TOut> Bind<TIn, TOut>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, TOut> func)
            where TIn : struct
            where TOut : struct
        {
            var result = PipelineBuilder.Bind(
                input: input,
                func: _tin => (TOut?)func.Invoke(_tin));
            return result;
        }

        // unsafe from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Fails if the given value is null.
        /// </summary>
        public static PipelineResult<TOut> Bind<TIn, TOut>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, TOut?> func)
            where TIn : struct
            where TOut : struct
        {
            var result = PipelineBuilder.Bind(
                input: input,
                func: _tin => PipelineResult<TOut>.InitFromNullable(func.Invoke(_tin)));
            return result;
        }

        // unsafe from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given value and updates the pipeline's value.<br/>
        /// Fails if the given pipeline is in failure.
        /// </summary>
        public static PipelineResult<TOut> Bind<TIn, TOut>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<TOut>> func)
            where TIn : struct
            where TOut : struct
        {
            var input_new_type = new PipelineResult<TOut>(input.info);
            if (input.info.error != null)
            {
                return PipelineResult<TOut>.PropagateFailure(previous: input_new_type);
            }
            var func_result = func.Invoke(input.value);
            return func_result.info.error != null
                ? PipelineResult<TOut>.PushFailure(previous: input_new_type, error: func_result.info.error.Value)
                : PipelineResult<TOut>.PushSuccess(previous: input_new_type, value: func_result.value);
        }

        /* Joins with 1 selector - using external pipelines, apply treatments on the current pipeline which may make it fail */

        // untyped input, safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Cannot fail from treatment.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<T1, TOut> func)
            where TOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                func: (_, _t1) => func.Invoke(_t1));
            return result;
        }

        // untyped input, from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Fails if the function returns null.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<T1, TOut?> func)
            where TOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                func: (_, _t1) => func.Invoke(_t1));
            return result;
        }

        // untyped input, from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Fails if the function fails.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<T1, PipelineResult<TOut>> func)
            where TOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                func: (_, _t1) => func.Invoke(_t1));
            return result;
        }

        // typed input, safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Cannot fail from treatment.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, T1, TOut> func)
            where TOut : struct
            where TIn : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: input,
                selector1: selector1,
                func: (_tin, _t1) => (TOut?)func.Invoke(_tin, _t1));
            return result;
        }

        // typed input, from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Fails if the function returns null.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, T1, TOut?> func)
            where TIn : struct
            where TOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: input,
                selector1: selector1,
                func: (_tin, _t1) => PipelineResult<TOut>.InitFromNullable(func.Invoke(_tin, _t1)));
            return result;
        }

        // typed input, from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipeline and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if the given secondary pipeline is in failure.<br/>
        /// Fails if the function fails.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, T1, PipelineResult<TOut>> func)
            where TIn : struct
            where TOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Join(
                input: input,
                selector1: selector1,
                selector2: (_) => new PipelineResult<bool>(new PipelineResult()),
                func: (_tin, _t1, _) => func.Invoke(_tin, _t1));
            return result;
        }

        /* Joins with 2 selectors - using external pipelines, apply treatments on the current pipeline which may make it fail */

        // untyped input, safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Cannot fail from treatment.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1, T2>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<PipelineResult<T2>> selector2,
            [NotNull] Func<T1, T2, TOut> func)
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                selector2: _ => selector2.Invoke(),
                func: (_, _t1, _t2) => func.Invoke(_t1, _t2));
            return result;
        }

        // untyped input, from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Fails if the function returns null.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1, T2>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<PipelineResult<T2>> selector2,
            [NotNull] Func<T1, T2, TOut?> func)
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                selector2: _ => selector2.Invoke(),
                func: (_, _t1, _t2) => func.Invoke(_t1, _t2));
            return result;
        }

        // untyped input, from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Fails if the function fails.
        /// </summary>
        public static PipelineResult<TOut> Join<TOut, T1, T2>(
            this PipelineResult input,
            [NotNull] Func<PipelineResult<T1>> selector1,
            [NotNull] Func<PipelineResult<T2>> selector2,
            [NotNull] Func<T1, T2, PipelineResult<TOut>> func)
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var result = PipelineBuilder.Join(
                input: new PipelineResult<TOut>(input),
                selector1: _ => selector1.Invoke(),
                selector2: _ => selector2.Invoke(),
                func: (_, _t1, _t2) => func.Invoke(_t1, _t2));
            return result;
        }

        // typed input, safe
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Cannot fail from treatment.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1, T2>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, PipelineResult<T2>> selector2,
            [NotNull] Func<TIn, T1, T2, TOut> func)
            where TIn : struct
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var result = PipelineBuilder.Join(
                input: input,
                selector1: selector1,
                selector2: selector2,
                func: (_tin, _t1, _t2) => (TOut?)func.Invoke(_tin, _t1, _t2));
            return result;
        }

        // typed input, from nullable
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Fails if the function returns null.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1, T2>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, PipelineResult<T2>> selector2,
            [NotNull] Func<TIn, T1, T2, TOut?> func)
            where TIn : struct
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var result = PipelineBuilder.Join(
                input: input,
                selector1: selector1,
                selector2: selector2,
                func: (_tin, _t1, _t2) => PipelineResult<TOut>.InitFromNullable(func.Invoke(_tin, _t1, _t2)));
            return result;
        }

        // typed input, from pipelineresult
        /// <summary>
        /// If the pipeline is successful, dereferences the given secondary pipelines and updates the main pipeline's value with the given function's result.<br/>
        /// Fails if any of the given secondary pipelines are in failure.<br/>
        /// Fails if the function fails.
        /// </summary>
        public static PipelineResult<TOut> Join<TIn, TOut, T1, T2>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, PipelineResult<T1>> selector1,
            [NotNull] Func<TIn, PipelineResult<T2>> selector2,
            [NotNull] Func<TIn, T1, T2, PipelineResult<TOut>> func)
            where TIn : struct
            where TOut : struct
            where T1 : struct
            where T2 : struct
        {
            var input_retyped = new PipelineResult<TOut>(input.info);
            if (input.info.error != null)
            {
                return PipelineResult<TOut>.PropagateFailure(previous: input_retyped);
            }
            var pipeline1 = selector1.Invoke(input.value);
            if (pipeline1.info.error != null)
            {
                return PipelineResult<TOut>.PushFailure(
                    previous: input_retyped,
                    error: pipeline1.info.error.Value);
            }
            var pipeline2 = selector2.Invoke(input.value);
            if (pipeline2.info.error != null)
            {
                return PipelineResult<TOut>.PushFailure(
                    previous: input_retyped,
                    error: pipeline2.info.error.Value);
            }
            var result = func.Invoke(input.value, pipeline1.value, pipeline2.value);
            return result.info.error != null
                ? PipelineResult<TOut>.PushFailure(
                    previous: input_retyped,
                    error: result.info.error.Value)
                : PipelineResult<TOut>.PushSuccess(
                    previous: input_retyped,
                    value: result.value);
        }

        /* Forking - applying a side-effect treatment */

        /// <summary>
        /// Calls the given function if the pipeline is successful. <br/>
        /// Cannot fail.
        /// </summary>
        public static PipelineResult<TInOut> Fork<TInOut>(
            this PipelineResult<TInOut> input,
            [NotNull] Action<TInOut> func)
            where TInOut : struct
        {
            if (input.info.error == null)
            {
                func.Invoke(input.value);
            }
            return input.info.error != null
                ? PipelineResult<TInOut>.PropagateFailure(previous: input)
                : PipelineResult<TInOut>.PropagateSuccess(previous: input);
        }

        /// <summary>
        /// Sets the given out argument using the given value if it is not null, otherwise to default. <br/>
        /// Fails if the given value is null.
        /// </summary>
        public static PipelineResult<TInOut> Fork<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] out T1 forked_value,
            [NotNull] Func<TInOut, T1?> selector)
            where TInOut : struct
            where T1 : struct
        {
            var result = PipelineBuilder.Fork(
                input: input,
                forked_value: out forked_value,
                selector: _tinout => PipelineResult<T1>.InitFromNullable(selector.Invoke(_tinout)));
            return result;
        }

        /// <summary>
        /// Sets the given out argument using the given pipeline's value if it is successful, otherwise to default. <br/>
        /// Fails if the given pipeline is in failure.
        /// </summary>
        public static PipelineResult<TInOut> Fork<TInOut, T1>(
            this PipelineResult<TInOut> input,
            [NotNull] out T1 forked_value,
            [NotNull] Func<TInOut, PipelineResult<T1>> selector)
            where TInOut : struct
            where T1 : struct
        {
            if (input.info.error != null)
            {
                forked_value = default;
                return PipelineResult<TInOut>.PropagateFailure(previous: input);
            }
            var selector_result = selector.Invoke(input.value);
            if (selector_result.info.error != null)
            {
                forked_value = default;
                return PipelineResult<TInOut>.PushFailure(
                    previous: input,
                    error: selector_result.info.error.Value);
            }
            else
            {
                forked_value = selector_result.value;
            }
            return PipelineResult<TInOut>.PropagateSuccess(previous: input);
        }

        /* Looping - applying a treatment to the pipeline for each element of a collection */

        /// <summary>
        /// If the pipeline is successful, loops over the given collection and applies the given function on the pipeline for each element.<br/>
        /// Cannot fail.
        /// </summary>
        public static PipelineResult<TOut> Loop<T1, TOut>(
            this PipelineResult input,
            [NotNull] Func<TOut> initial_value_selector,
            [NotNull] Func<IEnumerable<T1>> collection_selector,
            [NotNull] Func<TOut, T1, TOut> func)
            where TOut : struct
        {
            var result = PipelineBuilder.Loop(
                input: new PipelineResult<bool>(input),
                initial_value_selector: (_) => initial_value_selector.Invoke(),
                collection_selector: (_) => collection_selector.Invoke(),
                func: func);
            return result;
        }

        /// <summary>
        /// If the pipeline is successful, loops over the given collection and applies the given function on the pipeline for each element.<br/>
        /// Cannot fail.
        /// </summary>
        public static PipelineResult<TOut> Loop<TIn, T1, TOut>(
            this PipelineResult<TIn> input,
            [NotNull] Func<TIn, TOut> initial_value_selector,
            [NotNull] Func<TIn, IEnumerable<T1>> collection_selector,
            [NotNull] Func<TOut, T1, TOut> func)
            where TIn : struct
            where TOut : struct
        {
            if (input.info.error != null)
            {
                return PipelineResult<TOut>.PropagateFailure(previous: new PipelineResult<TOut>(input.info));
            }
            var initial_value = initial_value_selector.Invoke(input.value);
            var new_type_pipeline = new PipelineResult<TOut>(
                info: input.info,
                value: initial_value);
            var collection = collection_selector.Invoke(input.value);
            var updated_value = collection.Aggregate(
                seed: initial_value,
                func: func);
            return PipelineResult<TOut>.PushSuccess(
                previous: new_type_pipeline,
                value: updated_value);
        }

        /* Catching - apply treatments on a failing pipeline to try to make it successful again */

        /// <summary>
        /// Catches a failing pipeline back to success with the given value. <br/>
        /// Does nothing if the pipeline is already successful.
        /// </summary>
        public static PipelineResult<TInOut> Catch<TInOut>(
            this PipelineResult<TInOut> input,
            TInOut catch_value)
            where TInOut : struct
        {
            var result = PipelineBuilder.Catch(
                input: input,
                func: () => new PipelineResult<TInOut>(info: new(), value: catch_value));
            return result;
        }

        /// <summary>
        /// Catches a failing pipeline back with the given pipeline of the same type. <br/>
        /// Does nothing if the pipeline is already successful.<br/>
        /// Keeps failing if the given pipeline fails.
        /// </summary>
        public static PipelineResult<TInOut> Catch<TInOut>(
            this PipelineResult<TInOut> input,
            [NotNull] Func<PipelineResult<TInOut>> func)
            where TInOut : struct
        {
            if (input.info.error == null)
            {
                return PipelineResult<TInOut>.PropagateSuccess(previous: input);
            }
            var catching_pipeline = func.Invoke();
            return catching_pipeline.info.error != null
                ? PipelineResult<TInOut>.PushFailure(
                    previous: input,
                    error: catching_pipeline.info.error.Value)
                : PipelineResult<TInOut>.PushSuccess(
                    previous: input,
                    value: catching_pipeline.value);
        }

        /* Pipeline resolution functions */

        /// <summary>
        /// Ends a pipeline by returning whether the given pipeline is successful or not.
        /// </summary>
        public static bool Resolve(
            this PipelineResult input)
        {
            return input.error == null;
        }

        /// <summary>
        /// Ends a pipeline by returning either its value if it is successful or the provided default value if it has failed.
        /// </summary>
        public static TInOut Resolve<TInOut>(
            this PipelineResult<TInOut> input,
            TInOut default_value = default)
            where TInOut : struct
        {
            return input.info.error != null
                ? default_value
                : input.value;
        }

        /// <summary>
        /// Ends a pipeline by returning either its value if it is successful or the provided default value if it has failed.
        /// </summary>
        public static TOut Resolve<TIn, TOut>(
            this PipelineResult<TIn> input,
            Func<TIn, TOut> selector,
            TOut default_value)
            where TIn : struct
        {
            return input.info.error != null
                ? default_value
                : selector.Invoke(input.value);
        }

        /// <summary>
        /// Ends a pipeline by either returning its value if it is successful or throwing an exception if it has failed.
        /// </summary>
        public static TInOut Challenge<TInOut>(
            this PipelineResult<TInOut> input)
            where TInOut : struct
        {
            if (input.info.error != null)
            {
                throw new Exception();
            }
            else
            {
                return input.value;
            }
        }

    }
}