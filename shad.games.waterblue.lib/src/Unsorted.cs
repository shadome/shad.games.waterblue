using System;
using System.Collections.Generic;
using System.Linq;

namespace shad.games.waterblue.lib
{
    public static class Unsorted
    {
        public static TResult ApplyFunc<TParameter, TResult>(
            this TParameter parameter,
            Func<TParameter, TResult> func)
        {
            return func.Invoke(parameter) ?? throw new Exception();
        }
    }
}
