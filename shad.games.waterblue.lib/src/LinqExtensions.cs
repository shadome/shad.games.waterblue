using System;
using System.Collections.Generic;
using System.Linq;

namespace shad.games.waterblue.lib
{
    public static class LinqExtensions
    {
        public static IEnumerable<T> ConcatHead<T>(
            this IEnumerable<T> sequence,
            IEnumerable<T> sequenceHead)
        {
            return sequenceHead?.Concat(sequence) ?? sequence;
        }

        public static int IndexOf<T>(
            this IEnumerable<T> sequence,
            Func<T, bool> condition)
        {
            // int variable + enumerator to avoid using sequence.Count() and sequence[i]
            int i = 0;
            foreach (var item in sequence)
            {
                if (condition.Invoke(item))
                {
                    return i;
                }
                i++;
            }
            return -1;
        }
    }
}
