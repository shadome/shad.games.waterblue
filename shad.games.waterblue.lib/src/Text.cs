﻿using System;

namespace shad.games.waterblue.lib
{
    public readonly ref struct Text
    {
        private readonly ReadOnlySpan<char> value;

        public Text(string text)
        {
            this.value = text.AsSpan();
        }

        public override string ToString()
        {
            return new string(value);
        }
    }
}
