﻿using shad.games.waterblue.battlecore.model;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace shad.games.waterblue.data
{
    public static class MonsterDictionary
    {
        public static readonly Monster n026_raichu = Monster.Create(
            name: "raichu",
            level: 50,
            moves: ImmutableArray.CreateRange(new[]
            {
                MoveDictionary.thundershock,
                MoveDictionary.thunderbolt,
                MoveDictionary.thunder,
                MoveDictionary.tail_whip,
            }),
            species: Species.Create(
                number: 26,
                name: "raichu",
                type_1: Type.Electric,
                basestats: Stats.Create(100, 100, 100, 100, 100, 150)));

        public static readonly Monster n063_abra = Monster.Create(
            name: "abra",
            level: 50,
            moves: ImmutableArray.CreateRange(new[] 
            { 
                MoveDictionary.confusion,
            }),
            species: Species.Create(
                number: 63,
                name: "abra",
                type_1: Type.Psychic,
                basestats: Stats.Create(70, 100, 100, 100, 100, 250)));

        public static readonly Monster n091_cloyster = Monster.Create(
            name: "cloyster",
            level: 50,
            moves: ImmutableArray.CreateRange(new[] 
            {
                MoveDictionary.ice_beam,
            }),
            species: Species.Create(
                number: 91,
                name: "cloyster",
                type_1: Type.Water, 
                type_2: Type.Ice,
                basestats: Stats.Create(100, 100, 100, 100, 100, 75)));

        public static readonly Monster n094_gengar = Monster.Create(
            name: "gengar",
            level: 50,
            moves: ImmutableArray.CreateRange(new[] 
            { 
                MoveDictionary.shadow_ball, 
                MoveDictionary.sludge_bomb, 
            }),
            species: Species.Create(
                number: 94,
                name: "gengar",
                type_1: Type.Ghost, 
                type_2: Type.Poison,
                basestats: Stats.Create(100, 100, 100, 100, 100, 125)));
    }
}
