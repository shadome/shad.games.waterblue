﻿using shad.games.waterblue.battlecore.model;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace shad.games.waterblue.data
{
    public static class MoveDictionary
    {
        public const string TAIL_WHIP_NAME = "tail whip";
        public static readonly Move tail_whip = Move.Create(
            name: MoveDictionary.TAIL_WHIP_NAME,
            move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Normal, 20)
            }));

        public const string THUNDERSHOCK_NAME = "thundershock";
        public static readonly Move thundershock = Move.Create(
            name: MoveDictionary.THUNDERSHOCK_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Electric, 40)
            }));

        public const string THUNDER_NAME = "thunder";
        public static readonly Move thunder = Move.Create(
            name: MoveDictionary.THUNDER_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Electric, 120)
            }));

        public const string THUNDERBOLT_NAME = "thunderbolt";
        public static readonly Move thunderbolt = Move.Create(
            name: MoveDictionary.THUNDERBOLT_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Electric, 90)
            }));

        public const string SHADOW_BALL_NAME = "shadow ball";
        public static readonly Move shadow_ball = Move.Create(
            name: MoveDictionary.SHADOW_BALL_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Ghost, 90)
            }));

        public const string SLUDGE_BOMB_NAME = "sludge bomb";
        public static readonly Move sludge_bomb = Move.Create(
            name: MoveDictionary.SLUDGE_BOMB_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Poison, 90)
            }));

        public const string CONFUSION_NAME = "confusion";
        public static readonly Move confusion = Move.Create(
            name: MoveDictionary.CONFUSION_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Psychic, 50)
            }));

        public const string ICE_BEAM_NAME = "ice beam";
        public static readonly Move ice_beam = Move.Create(
            name: MoveDictionary.ICE_BEAM_NAME,
            move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
            effects: ImmutableArray.CreateRange(new[]
            {
                EffectDictionary.CreateDirectDamage(Type.Ice, 90)
            }));

    }
}
