﻿using System.Collections.Immutable;
using shad.games.waterblue.battlecore.model;

namespace shad.games.waterblue.data
{
    public static class EffectDictionary
    {
        public static Effect CreateDirectDamage(
            Type type,
            byte power)
        {
            return Effect.Create(
                default_components: ImmutableArray.CreateRange(new [] {
                    ImmediateEffectComponent.Create(),
                    DamageComponent.Create(type, power),
                }),
                name: "direct damage");
        }
    }
}
