﻿using shad.games.waterblue.battlecore.model;
using System;

namespace shad.games.waterblue.orchestration.services.objects
{
    /// <summary>
    /// Private token sent to one client for identification.
    /// </summary>
    public readonly struct ClientToken
    {
        /// <summary>
        /// Public token sent to all clients participating in the same battle.
        /// </summary>
        public readonly Guid battle_id;

        /// <summary>
        /// Private token only known by one client for identification.
        /// </summary>
        public readonly Guid client_id;

        /// <summary>
        /// Team colour of the client.
        /// </summary>
        public readonly TeamColour team_colour;

        #region constructors

        /// <summary>
        /// Create a new token allowing a client to join a game.
        /// </summary>
        /// <param name="battle_id">identifier of the game to join, use Guid.NewId() otherwise</param>
        /// <param name="team_colour">client's colour</param>
        /// <returns></returns>
        public static ClientToken Create(
            Guid battle_id,
            TeamColour team_colour)
        {
            return new ClientToken(
                value: Guid.NewGuid(), 
                battle_id: battle_id, 
                team_colour: team_colour);
        }

        private ClientToken(
            Guid value,
            Guid battle_id,
            TeamColour team_colour)
        {
            this.client_id = value;
            this.team_colour = team_colour;
            this.battle_id = battle_id;
        }

        #endregion
    }
}
