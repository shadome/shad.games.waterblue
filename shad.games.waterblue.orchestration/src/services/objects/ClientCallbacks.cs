﻿using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.orchestration.model;
using System;
using System.Collections.Immutable;
using System.Threading.Tasks;

namespace shad.games.waterblue.orchestration.services.objects
{
    public readonly struct ClientCallbacks
    {
        // TODO give to the client info about which monster must be replaced (it is not always obvious, e.g., two KO in one turn)
        /// <summary>
        /// Asks the client to prompt the user for a monster swap.
        /// </summary>
        /// <param name="battlefield">current state of the battlefield</param>
        /// <returns>the position of the monster that will be swapped in, which must be eligible for swaps (existing, not out of combat, not currently on the battlefield, etc.)</returns>
        public readonly Action<Battlefield, BattleMonsterId, ImmutableArray<BattleMonsterId>> OnRequiredSwap;

        public readonly Action<ClientToken, Battlefield> OnNewBattle;

        /// <summary>
        /// Asks the client to prompt the user for actions for the upcoming turn.
        /// </summary>
        /// <param name="battlefield">current state of the battlefield</param>
        public readonly Action<Battlefield> OnNewTurn;

        public readonly Action<MoveActionLog> OnMoveAction;

        public readonly Action<SwapAction, Battlefield, Battlefield> OnSwapAction;

        /// <summary>
        /// Tells the client that the battle is over and a win/loss/draw has been decided.
        /// </summary>
        /// <param name="end_state">whether the client has won, lost or drawn</param>
        public readonly Action<Battlefield, BattleResult> OnBattleEnd;


        /// <summary>
        /// Tells the client that the current turn has ended.
        /// </summary>
        /// <param name="battlefield">new state of the battlefield</param>
        /// <param name="battle_turn_log">what happened during the complete turn</param>
        // public readonly Action<Battlefield, BattleTurnLog> OnEndOfTurn;

        /// <summary>
        /// Sets the server-side chosen team value to the client.
        /// This function will be called by the server prior to any other.
        /// The team associated with the client will not change during the battle.
        /// </summary>
        // public readonly Action<ClientToken> OnTokenReceived;


        public ClientCallbacks(
            Action<ClientToken, Battlefield> onNewBattle,
            Action<Battlefield> onNewTurn,
            Action<MoveActionLog> onMoveAction,
            Action<SwapAction, Battlefield, Battlefield> onSwapAction,
            Action<Battlefield, BattleMonsterId, ImmutableArray<BattleMonsterId>> onRequiredSwap,
            Action<Battlefield, BattleResult> onBattleEnd)
        {
            this.OnNewBattle = onNewBattle;
            this.OnNewTurn = onNewTurn;
            this.OnMoveAction = onMoveAction;
            this.OnSwapAction = onSwapAction;
            this.OnBattleEnd = onBattleEnd;
            this.OnRequiredSwap = onRequiredSwap;
        }

    }
}
