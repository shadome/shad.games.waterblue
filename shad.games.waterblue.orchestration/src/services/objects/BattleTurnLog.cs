﻿using System.Collections.Immutable;
using shad.games.waterblue.battlecore.model;

namespace shad.games.waterblue.orchestration.services.objects
{
    /// <summary>
    /// Logged events for the previous turn. <br/>
    /// The client should use this object to display animations about the previous turn.
    /// </summary>
    public readonly struct BattleTurnLog
    {
        private readonly byte max_order;

        public readonly ImmutableArray<(byte order, Battlefield battlefield, MoveAction move_action)> move_actions;
        
        public readonly ImmutableArray<(byte order, Battlefield battlefield, SwapAction swap_action)> swap_actions;

        private BattleTurnLog(
            byte max_order,
            ImmutableArray<(byte order, Battlefield battlefield, MoveAction move_action)> move_actions,
            ImmutableArray<(byte order, Battlefield battlefield, SwapAction swap_action)> swap_actions)
        {
            this.max_order = max_order;
            this.move_actions = move_actions;
            this.swap_actions = swap_actions;
        }

        internal static BattleTurnLog Create()
        {
            var log = new BattleTurnLog(
                max_order: 0,
                move_actions: ImmutableArray<(byte, Battlefield, MoveAction)>.Empty,
                swap_actions: ImmutableArray<(byte, Battlefield, SwapAction)>.Empty);
            return log;
        }

        internal static BattleTurnLog AddMoveAction(
            BattleTurnLog previous,
            MoveAction move_action,
            Battlefield battlefield_after_action)
        {
            var tuple = (
                order: (byte)(previous.max_order + 1), 
                battlefield: battlefield_after_action, 
                move_action: move_action);
            var log = new BattleTurnLog(
                max_order: (byte)(previous.max_order + 1),
                move_actions: previous.move_actions.Add(tuple),
                swap_actions: previous.swap_actions);
            return log;
        }

        internal static BattleTurnLog AddSwapAction(
            BattleTurnLog previous,
            SwapAction swap_action,
            Battlefield battlefield_after_action)
        {
            var tuple = (
                order: (byte)(previous.max_order + 1), 
                battlefield: battlefield_after_action, 
                swap_action: swap_action);
            var log = new BattleTurnLog(
                max_order: (byte)(previous.max_order + 1),
                move_actions: previous.move_actions,
                swap_actions: previous.swap_actions.Add(tuple));
            return log;
        }
    }
}
