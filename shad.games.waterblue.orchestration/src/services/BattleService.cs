﻿using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.orchestration.model;
using shad.games.waterblue.orchestration.services.objects;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using shad.games.waterblue.lib.pipelines;

namespace shad.games.waterblue.orchestration.services
{


// TODOS
//  ensure no Ids are default


    //TODOLIST do not think about structs and reimplementation just yet
    //use readonlyspan whenever possible ReadOnlySpan<(TeamColour, Monster)> / test its comparison?
    public static class BattleService
    {
        /// <summary>
        /// Initiates a battle with a robot.
        /// This function will not return until the battle is finished and will invoke callbacks.
        /// Run it in a dedicated thread.
        /// </summary>
        public static async Task RunBattleWithRobot(
            ImmutableArray<Monster> player_team,
            ImmutableArray<Monster> robot_team,
            ClientCallbacks player_callbacks,
            ClientCallbacks robot_callbacks)
        {
            var player_colour = TeamColour.Blue;
            var robot_colour = TeamColour.Grey;
            /* battle creation */
            var battle = Battle.Create(
                Battlefield.Create(ImmutableArray.CreateRange(new[]
                {
                    (player_colour, player_team.Select(_m => BattleMonster.Create(_m)).ToImmutableArray()),
                    (robot_colour, robot_team.Select(_m => BattleMonster.Create(_m)).ToImmutableArray()),
                })));
            /* orchestration side effects / game creation initialisation */
            var player_token = ClientToken.Create(battle.battle_id, player_colour);
            var robot_token = ClientToken.Create(battle.battle_id, robot_colour);
            BattleService.running_clients.Add(player_token.client_id, player_token);
            BattleService.running_clients.Add(robot_token.client_id, robot_token);
            BattleService.battleid_to_context.Add(battle.battle_id, TurnContext.Reset());
            /* client initialisation callback calls */
            player_callbacks.OnNewBattle(player_token, battle.turns[0]);
            robot_callbacks.OnNewBattle(robot_token, battle.turns[0]);
            var callback_dict = ImmutableDictionary.CreateRange(new[]
            {
                KeyValuePair.Create(player_colour, player_callbacks),
                KeyValuePair.Create(robot_colour, robot_callbacks),
            });
            await Moulinette.GameLoop(
                battle: battle,
                callbacks: callback_dict);
        }

        public static (ImmutableArray<SwapAction> pending_swaps, ImmutableArray<MoveAction> pending_moves) UnregisterAction(
            ClientToken token,
            BattleMonsterId monster_id)
        {
            BattleService.CheckClientToken(token);
            var context = BattleService.GetTurnContext(token.battle_id);
            var updated_context = TurnContext.UnregisterAction(
                previous: context,
                team: token.team_colour,
                monster_id: monster_id);
            BattleService.SetTurnContext(
                battle_id: token.battle_id, 
                updated: updated_context);
            var registered_actions_for_team = TurnContext.GetRegistrationsForTeam(
                team: token.team_colour,
                context: updated_context);

            return registered_actions_for_team;
        }

        /// <summary>
        /// Register the next move for the given monster on the battlefield. </br>
        /// Note: only one action can be registered for one monster.
        /// Any attempt to register a second action for a monster would be ignored. </br>
        /// Use this function in conjunction with the OnNewTurn callback.
        /// </summary>
        public static (ImmutableArray<SwapAction> pending_swaps, ImmutableArray<MoveAction> pending_moves) RegisterMove(
            ClientToken token,
            MoveAction move_action)
        {
            BattleService.CheckClientToken(token);
            var previous_context = BattleService.GetTurnContext(battle_id: token.battle_id);

            var final_context = PipelineBuilder.Seed
                .Bind(() => previous_context)
                .Ensure(_context => _context.battlefield_snapshot != null)
                // ensure as soon as possible that the move action is valid from the context's point of view
                .Ensure(_context => TurnContext.IsMoveActionValid(
                    context: _context,
                    team: token.team_colour,
                    move_action: move_action))
                .Ensure(_context => Battlecore.CanCast(
                    battlefield: _context.battlefield_snapshot!.Value,
                    move_action: move_action))
                .Bind(_context => TurnContext.RegisterPendingBattleMove(
                    previous: _context,
                    team: token.team_colour,
                    pending_move: move_action))
                .Fork(_updatedcontext => BattleService.SetTurnContext(
                    battle_id: token.battle_id, 
                    updated: _updatedcontext))
                .Resolve(previous_context);

            var registered_actions_for_team = TurnContext.GetRegistrationsForTeam(
                team: token.team_colour,
                context: final_context);
                
            return registered_actions_for_team;
        }

        /// <summary>
        /// Register a swap-out for the given monster on the battlefield. </br>
        /// Note: only one action can be registered for one monster.
        /// Any attempt to register a second action for a monster would be ignored. </br>
        /// Use this function in conjunction with the OnNewTurn or OnRequiredSwap callbacks.
        /// </summary>
        public static (ImmutableArray<SwapAction> pending_swaps, ImmutableArray<MoveAction> pending_moves) RegisterSwap(
            ClientToken token,
            SwapAction swap_action)
        {
            BattleService.CheckClientToken(token);
            var context = BattleService.GetTurnContext(token.battle_id);

            var registered_actions_for_team = PipelineBuilder.Seed
                .Ensure(() => context.battlefield_snapshot != null)
                // ensure the provided outgoing id exists on the field and is of the right team colour
                .Ensure(() => _IsMonsterOnFieldForTeam(
                    battlefield: context.battlefield_snapshot!.Value,
                    team: token.team_colour,
                    monster_id:  swap_action.outgoing))
                // ensure the provided incoming id exists in the party and is of the right team colour
                .Ensure(() => _IsMonsterInPartyForTeam(
                    battlefield: context.battlefield_snapshot!.Value,
                    team: token.team_colour,
                    monster_id:  swap_action.incoming))
                .Ensure(() => TurnContext.IsSwapActionValid(
                    context: context,
                    team: token.team_colour,
                    swap_action: swap_action))
                .Ensure(() => Battlecore.CanSwap(
                    battlefield: context.battlefield_snapshot!.Value,
                    swap_action: swap_action))
                .Bind(() => TurnContext.RegisterPendingBattleSwap(
                    previous: context,
                    team: token.team_colour,
                    pending_swap: swap_action))
                .Fork(_updatedcontext => BattleService.SetTurnContext(
                    battle_id: token.battle_id,
                    updated: _updatedcontext))
                .Catch(context)
                .Bind(_updatedcontext => TurnContext.GetRegistrationsForTeam(
                    team: token.team_colour,
                    context: _updatedcontext))
                .Resolve();

            return registered_actions_for_team;

            static bool _IsMonsterOnFieldForTeam(
                Battlefield battlefield,
                TeamColour team,
                BattleMonsterId monster_id)
            {
                var monster_position = battlefield.TryGetMonsterFieldPosition(monster_id);
                return team == monster_position?.team;
            }

            static bool _IsMonsterInPartyForTeam(
                Battlefield battlefield,
                TeamColour team,
                BattleMonsterId monster_id)
            {
                var monster_position = battlefield.TryGetMonsterPartyPosition(monster_id);
                return team == monster_position?.team;
            }
        }

        /// <summary>
        /// Signal the server that the client associated with the given token
        /// is ready to play the upcoming turn. 
        /// </summary>
        public static void LockRegistrations(
            ClientToken token)
        {
            BattleService.CheckClientToken(token);
            var context = BattleService.GetTurnContext(token.battle_id);
            var updated_context = TurnContext.LockTeam(
                previous: context,
                team: token.team_colour);
            BattleService.SetTurnContext(
                battle_id: token.battle_id, 
                updated: updated_context);
        }

        public static ImmutableArray<BattleMonsterId> GetEligibleIncomingMonstersForSwap(
            ClientToken token,
            BattleMonsterId swapping_out)
        {
            BattleService.CheckClientToken(token);
            var monster_ids = PipelineBuilder.Seed
                .Bind(() => BattleService.GetTurnContext(battle_id: token.battle_id))
                .Ensure(_context => _context.battlefield_snapshot != null)
                .Ensure(_context => TurnContext.AreRegistrationsOpen(
                    context: _context,
                    team: token.team_colour))
                .Bind(_context => _GetEligibleIncomingMonstersForSwap(
                    context: _context,
                    team: token.team_colour,
                    outgoing: swapping_out))
                .Resolve(ImmutableArray<BattleMonsterId>.Empty);

            return monster_ids;

            static ImmutableArray<BattleMonsterId> _GetEligibleIncomingMonstersForSwap(
                TurnContext context,
                TeamColour team,
                BattleMonsterId outgoing)
            {
                var battlefield = context.battlefield_snapshot!.Value;
                var result = battlefield.party_monsters[team]
                    .Select(_monster => _monster.id)
                    .Where(_monsterid =>
                    {
                        var swap_action = SwapAction.Create(
                            outgoing: outgoing,
                            incoming: _monsterid);
                        var can_swap = Battlecore.CanSwap(
                            battlefield: battlefield,
                            swap_action: swap_action);
                        var is_swap_action_valid = TurnContext.IsSwapActionValid(
                            context: context,
                            team: team,
                            swap_action: swap_action);
                        return can_swap && is_swap_action_valid;
                    })
                    .ToImmutableArray();
                return result;
            }
        }

        /* Internal */

        internal static TurnContext GetTurnContext(
            Guid battle_id)
        {
            var battle_context = BattleService.battleid_to_context[battle_id];
            return battle_context;
        }

        internal static void SetTurnContext(
            Guid battle_id,
            TurnContext updated)
        {
            BattleService.battleid_to_context[battle_id] = updated;
        }

        /* Private */

        // TODO The following dictionaries do not support multi-threading.
        // In order to avoid locking the whole dictionary for a single game-loop, 
        // a list on battles undergoing active treatments (i.e., not waiting for player inputs) should be maintained.
        // This list should be thread-safe and it should also determine thread-safety: 
        // if a battle_id is under treatment in a certain thread, no other thread should access it.

        /// <summary>
        /// Running client tokens. Used to check if a caller tries to cheat.
        /// Key: ClientToken.clientId. Value: associated token created by the server, must match the one given by a client.
        /// </summary>
        private static readonly IDictionary<Guid, ClientToken> running_clients = new Dictionary<Guid, ClientToken>();

        /// <summary>
        /// Get the currently running BattleContext from a battleId.
        /// Key: ClientToken.battleId. Value: running BattleContext.
        /// </summary>
        private static readonly IDictionary<Guid, TurnContext> battleid_to_context = new Dictionary<Guid, TurnContext>();

        /// <summary>
        /// Check that the provided token is legit.
        /// </summary>
        private static void CheckClientToken(
            ClientToken token)
        {
            if (!BattleService.running_clients.ContainsKey(token.client_id) 
                || !BattleService.running_clients[token.client_id].Equals(token))
            {
                throw new Exception(); // cheating / ddos
            }
        }
    }
}
