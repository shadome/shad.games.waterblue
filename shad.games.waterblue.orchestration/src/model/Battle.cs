﻿using shad.games.waterblue.battlecore.model;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace shad.games.waterblue.orchestration.model
{
    public readonly struct Battle
    {
        public readonly Guid battle_id;

        // TODO use ArrayPool later on
        public readonly ImmutableArray<Battlefield> turns;

        private Battle(
            Guid battleid,
            IEnumerable<Battlefield> turns)
        {
            this.battle_id = battleid;
            this.turns = turns.ToImmutableArray();
        }

        public static Battle Create(
            Battlefield initial_state)
        {
            return new Battle(
                battleid: Guid.NewGuid(), 
                turns: new[] { initial_state });
        }

        public static Battle AddTurn(
            Battle previous,
            Battlefield new_turn)
        {
            return new Battle(
                battleid: previous.battle_id,
                turns: previous.turns.Add(new_turn));
        }
    }
}