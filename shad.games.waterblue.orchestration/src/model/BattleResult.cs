﻿namespace shad.games.waterblue.orchestration.model
{   
    /// <summary>
    /// Values that can be returned when a battle has ended.
    /// </summary>
    public enum BattleResult : byte
    {
        Win,
        Loss,
        Draw,
    }
}
