﻿using shad.games.waterblue.battlecore.model;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.model.battle
{
    // TODO 4 split between battle effect wrapper and effect metadata
    public readonly ref struct BattlefieldHead
    {
        // public readonly int iteration;

        // private readonly Span<BattleMonster?> monsters;

        // private BattlefieldHead(
        //     int iteration,
        //     IImmutableDictionary<TeamColour, IImmutableList<BattleMonster>> battlefield_monsters,
        //     IImmutableDictionary<TeamColour, IImmutableList<BattleMonster>> party_monsters)
        // {
        //     this.iteration = iteration;
        //     this.monsters = new Span<BattleMonster?>(Enumerable
        //         .Repeat(
        //             element: (BattleMonster?)null, 
        //             count: Enum.GetValues(typeof(TeamColour)).Length * Enum.GetValues(typeof(TeamPosition)).Length)
        //         .ToArray());
        //     this.monsters[0] = battlefield_monsters[0].First();
        //     // TODO
        // }

        // public static BattlefieldHead New(
        //     IEnumerable<(TeamColour colour, IEnumerable<BattleMonster> monsters)> teams)
        // {
        //     // TODO check all input for all public methods
        //     //if ((teams?.Count() ?? 0) < 2)
        //     //{
        //     //    throw new ArgumentException($"There should be two different team colours in {nameof(teams)}.");
        //     //}
        //     //if (teams.Any(_team => _team.monster.Equals(default(BattleMonster))))
        //     //{
        //     //    throw new ArgumentException("Unexpected ");
        //     //}

        //     return new BattlefieldHead(
        //         iteration: 0,
        //         battlefield_monsters: teams
        //             .Select(_team =>
        //                 (colour: _team.colour,
        //                 monsters: (IImmutableList<BattleMonster>)_team.monsters
        //                     .Take(Math.Min(2, _team.monsters.Count()))
        //                     .ToImmutableList()))
        //             .ToImmutableDictionary(_tuple => _tuple.colour, _tuple => _tuple.monsters),
        //         party_monsters: teams
        //             .Select(_team =>
        //                 (colour: _team.colour,
        //                 monsters: (IImmutableList<BattleMonster>)_team.monsters
        //                     .Skip(Math.Min(2, _team.monsters.Count()))
        //                     .ToImmutableList()))
        //             .ToImmutableDictionary(_tuple => _tuple.colour, _tuple => _tuple.monsters));

        // }

        //public static Battlefield UpdateTeams(
        //    Battlefield previous,
        //    IDictionary<BattlePosition, BattleMonster> teams)
        //{
        //    return new Battlefield(
        //        iteration: previous.iteration,
        //        //logs: previous.logs,
        //        teams: teams.ToImmutableDictionary());
        //}

        //public static Battlefield UpdateMonster(
        //    Battlefield previous,
        //    BattlePosition position,
        //    BattleMonster monster)
        //{
        //    var updatedTeams = previous.teams
        //        .Select(_kvp => (
        //            key: _kvp.Key,
        //            value: _kvp.Key.Equals(position) ? monster : _kvp.Value))
        //        .ToDictionary(_tuple => _tuple.key, _tuple => _tuple.value);
        //    return UpdateTeams(previous: previous, teams: updatedTeams);
        //}

        //public static Battlefield AddLogEntry(
        //    Battlefield previous
        //    //,
        //    //DamageLog log
        //    )
        //{
        //    return new Battlefield(
        //        iteration: previous.iteration,
        //        //logs: previous.logs.Add(log),
        //        teams: previous.teams);
        //}

        //public static Battlefield ClearLogs(
        //    Battlefield previous)
        //{
        //    return new Battlefield(
        //        iteration: previous.iteration,
        //        //logs: ImmutableList<DamageLog>.Empty,
        //        teams: previous.teams);
        //}
    }
}