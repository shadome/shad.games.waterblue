﻿using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.orchestration.model;
using shad.games.waterblue.orchestration.services;
using shad.games.waterblue.orchestration.services.objects;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System;
using shad.games.waterblue.lib.pipelines;

namespace shad.games.waterblue.orchestration
{
    /// <summary>
    /// The 'moulinette' contains the game's main loop code and some dedicated utilities.
    /// </summary>
    internal static class Moulinette
    {
        internal const byte NB_OF_TURNS_LIMIT = 100;

        // TODO this has no good reason to be implemented as a loop (unless maybe to handle waiting timeouts?).
        // Each segment of code between two Sleep/Delay calls could be a dedicated function, called by the orchestration when conditions are met (regarding player/robot's registrations).
        // Not implementing the following as a nearly-endless loop would probably make multi-threading more accessible (?)

        internal async static Task GameLoop(
            Battle battle,
            ImmutableDictionary<TeamColour, ClientCallbacks> callbacks)
        {
            var team_colours = ImmutableArray.CreateRange(callbacks.Keys);
            var battlefield = battle.turns[^1];
            var previous_battlefield = battlefield;
            var turn_context = new TurnContext();
            var losers = battlefield.GetLosers();
            var game_loop_counter = 0;
            var is_waiting = false;
            while (losers.Length == 0 && game_loop_counter++ < Moulinette.NB_OF_TURNS_LIMIT)
            {
                /* turn initialisation - listening for client actions */

                turn_context = TurnContext.OpenRegistrations(
                    battlefield: battlefield,
                    team_colours: team_colours);
                BattleService.SetTurnContext(
                    battle_id: battle.battle_id, 
                    updated: turn_context);
                foreach (var _callback in callbacks.Values)
                {
                    _callback.OnNewTurn(battlefield);
                }

                do 
                {
                    await Task.Delay(1000);
                    turn_context = BattleService.GetTurnContext(battle.battle_id);
                    is_waiting = !turn_context.waiting_for.IsDefaultOrEmpty;
                } 
                while (is_waiting);
                BattleService.SetTurnContext(
                    battle_id: battle.battle_id, 
                    updated: TurnContext.Reset());

                /* playing the turn */

                battlefield = Moulinette.PlayTurn(
                    battlefield: battlefield,
                    callbacks: callbacks,
                    pending_moves: turn_context.pending_moves,
                    pending_swaps: turn_context.pending_swaps);

                /* turn finalisation */

                battlefield = await Moulinette.ReplaceKnockedOut(
                    battle_id: battle.battle_id,
                    battlefield: battlefield,
                    callbacks: callbacks);
                losers = battlefield.GetLosers();
                battle = Battle.AddTurn(battle, battlefield);
            }
            foreach (var _team in callbacks.Keys)
            {
                var has_everyone_lost = losers.Length == callbacks.Keys.Count();
                var is_no_endless_battle_limit_reached = losers.Length == 0;
                if (has_everyone_lost || is_no_endless_battle_limit_reached)
                {
                    callbacks[_team].OnBattleEnd(battlefield, BattleResult.Draw);
                }
                else if (losers.Contains(_team))
                {   // not a draw and this team has lost
                    callbacks[_team].OnBattleEnd(battlefield, BattleResult.Loss);
                }
                else if (!losers.Contains(_team))
                {   // not a draw and this team has NOT lost
                    callbacks[_team].OnBattleEnd(battlefield, BattleResult.Win);    
                }
            }
        }

        private static Battlefield PlayTurn(
           Battlefield battlefield,
           ImmutableDictionary<TeamColour, ClientCallbacks> callbacks,
           ImmutableArray<MoveAction> pending_moves,
           ImmutableArray<SwapAction> pending_swaps)
        {
            Battlefield previous_battlefield;
            var priority_swap = battlefield.GetPrioritySwap(pending_swaps);
            while (priority_swap != null)
            {
                previous_battlefield = battlefield;
                battlefield = battlefield.DoSwap(priority_swap.Value);
                foreach (var _callback in callbacks.Values)
                {
                    _callback.OnSwapAction(
                        priority_swap.Value,
                        previous_battlefield,
                        battlefield);
                }
                pending_swaps = pending_swaps.Remove(priority_swap.Value);
                priority_swap = battlefield.GetPrioritySwap(pending_swaps);
            }

            /* 2. Preturn battle effects */

            var pending_effects = battlefield.GetPendingEffectsForTurnStage(EffectRecurrence.Preturn);
            var priority_effect = battlefield.GetPriorityBattleEffect(pending_effects);
            while (priority_effect != null)
            {
                battlefield = battlefield.DoTickEffect(effect_id: priority_effect.Value);
                pending_effects = pending_effects.Remove(priority_effect.Value);
                priority_effect = battlefield.GetPriorityBattleEffect(pending_effects);
            }

            /* 3. Pending moves */

            var priority_move = battlefield.GetPriorityBattleMove(pending_moves);
            while (priority_move != null)
            {
                var move_action_log = Battlecore.DoCast(
                    battlefield: battlefield,
                    move_action: priority_move.Value);
                battlefield = move_action_log.battlefield_after;
                foreach (var _callback in callbacks.Values)
                {
                    _callback.OnMoveAction(move_action_log);
                }
                pending_moves = pending_moves.Remove(priority_move.Value);
                priority_move = battlefield.GetPriorityBattleMove(pending_moves);
            }

            /* 4. Postturn battle effects */

            pending_effects = battlefield.GetPendingEffectsForTurnStage(EffectRecurrence.Postturn);
            priority_effect = battlefield.GetPriorityBattleEffect(pending_effects);
            while (priority_effect != null)
            {
                battlefield = battlefield.DoTickEffect(priority_effect.Value);
                pending_effects = pending_effects.Remove(priority_effect.Value);
                priority_effect = battlefield.GetPriorityBattleEffect(pending_effects);
            }
            return battlefield;
        }

        /// <summary>
        /// Replace battle monsters that were knocked out during this turn
        /// if possible.
        /// </summary>
        private static async Task<Battlefield> ReplaceKnockedOut(
            Guid battle_id,
            Battlefield battlefield,
            ImmutableDictionary<TeamColour, ClientCallbacks> callbacks)
        {
            // 1. for each team: get monsters that should be replaced, if any
            // 2. for each team: check if there are monsters that could swap in, if so
            // 3. for each team: prompt for swap (concurrently) with timeout
            //      if times out: choose randomly server-side
            //      Note: the battlefield object cannot be sequentially updated
            //          client 2 must not wait for client 1 to finish
            //          client 2 must not know the choices of client 1
            // 4. check the answers are relevant, else throw an error
            // 5. aggregate and apply the swaps (BattleCore)
            // 6. loop back to 1.: swapping in/out could lead to new KOs

            // var ko_field_monsters = _GetMonstersToReplace(battlefield);
            var is_waiting = true;
            var required_swaps = _GetMonstersToReplace(battlefield);
            while (!required_swaps.IsEmpty)
            {
                /* loop variables */
                var required_swaps_team_max_count = required_swaps.Max(_kvp => _kvp.Value.Length);
                var swap_buffer = ImmutableArray.CreateBuilder<SwapAction>(8);
                /* send concurrent callbacks to register swaps */
                for (byte i = 0; i < required_swaps_team_max_count; i++)
                {
                    /* open registrations */
                    var teams = required_swaps
                        .Where(_kvp => _kvp.Value.Length > i)
                        .Select(_kvp => _kvp.Key)
                        .ToImmutableArray();
                    var context = TurnContext.OpenRegistrations(
                        battlefield: battlefield,
                        team_colours: teams);
                    BattleService.SetTurnContext(
                        battle_id: battle_id,
                        updated: context);
                    /* send callbacks (up to one per team at the same time) */
                    var already_incoming_set = swap_buffer
                        .Select(_swap => _swap.incoming)
                        .ToHashSet();
                    var globally_eligible_incoming = ImmutableArray.CreateBuilder<BattleMonsterId>(8);
                    var parallelLoopResult = Parallel.ForEach(
                        source: teams,
                        body: _team =>
                        {
                            if (!required_swaps.ContainsKey(_team) || i >= required_swaps[_team].Length)
                            {
                                return;
                            }
                            var ko_monster_id = required_swaps[_team][i];
                            var eligible_swapin_positions = Battlecore
                                .GetEligibleBattleMonsterIdsForSwapIn(
                                    battlefield: battlefield,
                                    team_colour: _team)
                                .RemoveAll(_id => already_incoming_set.Contains(_id));
                            // side-effect: add the computed eligible swap-ins in a global list for later eligibility checks
                            globally_eligible_incoming.AddRange(eligible_swapin_positions);
                            if (eligible_swapin_positions.IsDefaultOrEmpty)
                            {
                                throw new Exception();
                            }
                            callbacks[_team].OnRequiredSwap(
                                battlefield,
                                ko_monster_id,
                                eligible_swapin_positions);
                        });
                    /* wait for all the registrations */
                    do
                    {
                        await Task.Delay(1000);
                        context = BattleService.GetTurnContext(battle_id);
                        is_waiting = !parallelLoopResult.IsCompleted || !context.waiting_for.IsDefaultOrEmpty;
                    } 
                    while (is_waiting);
                    // TODO HANDLE TIMEOUTS WITH DEFAULT CHOICE
                    /* Checking registrations integrity */
                    // note that some of those requirements are structurally ensured by the BattleService registration logic, 
                    // but this will be reworked altogether when the GameLoop will be deconstructed into smaller asynchronous orchestration pipelines
                    // called by the service (supposedly)
                    var required_outgoing_monsters = required_swaps.Values.SelectMany(x => x);
                    var requirements = PipelineBuilder.Seed
                        // number of registrations matches the required swapouts
                        .Ensure(() => required_swaps.Count == context.pending_swaps.Length)
                        // all of the required outgoing monster ids are registered as swapouts
                        .Ensure(() => context.pending_swaps.All(_pending => required_outgoing_monsters.Contains(_pending.outgoing)))
                        // all of the incoming monster ids are eligible
                        .Ensure(() => context.pending_swaps.All(_pending => globally_eligible_incoming.Contains(_pending.incoming)))
                        // all of the incoming monster ids are unique
                        .Ensure(() => context.pending_swaps.Select(_pending => _pending.incoming).Distinct().Count() == context.pending_swaps.Length)
                        .Resolve();
                    if (!requirements)
                    {
                        throw new OrchestrationException();
                    }

                    /* close registrations */
                    swap_buffer.AddRange(context.pending_swaps);
                    BattleService.SetTurnContext(
                        battle_id: battle_id,
                        updated: TurnContext.Reset());
                }
                /* update the pending battlefield */
                foreach (var _pendingswap in swap_buffer)
                {
                    var battlefield_before = battlefield;
                    battlefield = Battlecore.DoSwap(
                        battlefield: battlefield, 
                        swap_action: _pendingswap);
                    foreach (var _callback in callbacks)
                    {
                        _callback.Value.OnSwapAction(_pendingswap, battlefield_before, battlefield);
                    }
                }
                /* check if the previous swaps lead to more KO monsters to be replaced */
                required_swaps = _GetMonstersToReplace(battlefield);
            }
            return battlefield;

            static ImmutableDictionary<TeamColour, ImmutableArray<BattleMonsterId>>
                _GetMonstersToReplace(Battlefield battlefield)
            {
                var result = ImmutableDictionary.CreateBuilder<TeamColour, ImmutableArray<BattleMonsterId>>();
                foreach (var _kvp in battlefield.field_monsters)
                {
                    var team_swapouts = ImmutableArray.CreateBuilder<BattleMonsterId>(8);
                    foreach (var _monster in _kvp.Value)
                    {
                        if (_monster.IsKnockedOut())
                        {
                            var eligible_replacements = Battlecore.GetEligibleBattleMonsterIdsForSwapIn(
                                battlefield: battlefield,
                                team_colour: _kvp.Key);
                            // the number of eligible replacements will lower for each monster to swap out before this one
                            var eligible_replacements_count = eligible_replacements.Length - team_swapouts.Count;
                            if (eligible_replacements_count > 0)
                            {
                                team_swapouts.Add(_monster.id);
                            }
                        }
                    }
                    if (team_swapouts.Count > 0)
                    {
                        result.Add(
                            key: _kvp.Key, 
                            value: team_swapouts.ToImmutable());
                    }
                }
                return result.ToImmutable();
            }
        }
    }
}
