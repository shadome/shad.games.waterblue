﻿using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.lib.pipelines;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.orchestration
{
    /// <summary>
    /// This layer uses side-effects in order to provide proper understandable and distinct web services for the client to call.
    /// Each client battle instance is stored and accessed with a token. It sometimes need to be accessed in real-time to see
    /// what pending actions the client has registered during a timeout.
    /// Another way of handling those registrations in a functional manner would be using callbacks provided to the client, 
    /// but this approach might turn out to be difficult to implement.
    /// </summary>
    internal struct TurnContext
    {
        /// <summary> 
        /// State of the battlefield to be considered for registrations.
        /// Nullable because there are moments when the battlefield
        /// should not be accessed (obsolete state, more safety. 
        /// </summary>
        internal readonly Battlefield? battlefield_snapshot;

        /// <summary> 
        /// Teams for which the game loop is waiting for action registrations.
        /// Invariants: 
        ///     this list is empty when the server is not listening
        ///     this list must not be empty when the server is listening
        /// </summary>
        internal readonly ImmutableArray<TeamColour> waiting_for;

        // Dev note: ImmutableList because the number of elements will often evolve.
        // TODO PERF use an oversized mutable collection to avoid reallocating;
        // OR use a FRUGAL COLLECTION design pattern with ~10 or 20 discrete elements
        /// <summary> 
        /// List of the pending swaps registered at a
        /// specific point in time for the current turn. 
        /// </summary>
        internal readonly ImmutableArray<SwapAction> pending_swaps;

        // Dev note: ImmutableList because the number of elements will often evolve.
        // TODO PERF use an oversized mutable collection to avoid reallocating;
        // OR use a FRUGAL COLLECTION design pattern with ~10 or 20 discrete elements
        /// <summary>
        /// List of the pending moves registered at a
        /// specific point in time for the current turn. 
        /// </summary>
        internal readonly ImmutableArray<MoveAction> pending_moves;

        private TurnContext(
            Battlefield? battlefield_snapshot,
            ImmutableArray<TeamColour> waiting_for,
            ImmutableArray<SwapAction> pending_swaps,
            ImmutableArray<MoveAction> pending_moves)
        {
            this.battlefield_snapshot = battlefield_snapshot;
            this.waiting_for = waiting_for;
            this.pending_swaps = pending_swaps;
            this.pending_moves = pending_moves;
        }

        /// <summary>
        /// Create a new context or reset an existing one 
        /// to refuse action registrations and flush pending actions.
        /// </summary>
        internal static TurnContext Reset()
        {
            return new TurnContext(
                battlefield_snapshot: null,
                waiting_for: ImmutableArray<TeamColour>.Empty,
                pending_moves: ImmutableArray<MoveAction>.Empty,
                pending_swaps: ImmutableArray<SwapAction>.Empty);
        }

        /// <summary>
        /// Update the turn context to accept action registrations.
        /// </summary>
        internal static TurnContext OpenRegistrations(
            Battlefield battlefield,
            ImmutableArray<TeamColour> team_colours)
        {
            return new TurnContext(
                battlefield_snapshot: battlefield,
                waiting_for: team_colours,
                pending_moves: ImmutableArray<MoveAction>.Empty,
                pending_swaps: ImmutableArray<SwapAction>.Empty);
        }

        /// <summary>
        /// Remove the given team from the waiting_for list and block upcoming
        /// registrations of pending actions.
        /// </summary>
        internal static TurnContext LockTeam(
            TurnContext previous,
            TeamColour team)
        {
            return new TurnContext(
                battlefield_snapshot: previous.battlefield_snapshot,
                waiting_for: previous.waiting_for.Remove(team),
                pending_moves: previous.pending_moves,
                pending_swaps: previous.pending_swaps);
        }

        internal static TurnContext UnregisterAction(
            TurnContext previous,
            TeamColour team,
            BattleMonsterId monster_id)
        {
            var upated_turn_context = PipelineBuilder.Seed
                .Ensure(() => TurnContext.AreRegistrationsOpen(
                    context: previous,
                    team: team))
                .Bind(() => _GetRegisteredActionIndexesForMonster(
                    context: previous,
                    monster_id: monster_id))
                .Bind(_indexes => _UpdateTurnContext(
                    indexes: _indexes,
                    previous: previous))
                .Resolve(previous);

            return upated_turn_context;

            static (short swap_index, short move_index)? _GetRegisteredActionIndexesForMonster(
                TurnContext context, 
                BattleMonsterId monster_id)
            {
                var indexes = TurnContext.GetMonsterActionIndexes(
                    context: context,
                    battle_monster_id: monster_id);
                var has_monster_registrations = indexes.swap_index != -1 || indexes.move_index != -1;
                return has_monster_registrations
                    ? indexes
                    : null;
            }

            static TurnContext _UpdateTurnContext(
                (short swap_index, short move_index) indexes, 
                TurnContext previous)
            {
                var updated_pending_moves = indexes.move_index == -1
                    ? previous.pending_moves
                    : previous.pending_moves.RemoveAt(indexes.move_index);
                var updated_pending_swaps = indexes.swap_index == -1
                    ? previous.pending_swaps
                    : previous.pending_swaps.RemoveAt(indexes.swap_index);
                return new TurnContext(
                    battlefield_snapshot: previous.battlefield_snapshot,
                    waiting_for: previous.waiting_for,
                    pending_moves: updated_pending_moves,
                    pending_swaps: updated_pending_swaps);
            }
        }

        /// <summary>
        /// Add or replace the given pending swap to the list of pending 
        /// swaps to perform on the upcoming turn.<br/>
        /// If the caster already has a move/swap registered it is 
        /// removed/replaced. <br/>
        /// Does nothing if the given team has been locked earlier.
        /// </summary>
        internal static TurnContext RegisterPendingBattleSwap(
            TurnContext previous,
            TeamColour team,
            SwapAction pending_swap)
        {
            var updated = PipelineBuilder.Seed
                .Ensure(() => TurnContext.IsSwapActionValid(
                    context: previous,
                    team: team,
                    swap_action: pending_swap))
                .Bind(() => new TurnContext(
                    battlefield_snapshot: previous.battlefield_snapshot,
                    waiting_for: previous.waiting_for,
                    pending_moves: previous.pending_moves,
                    pending_swaps: previous.pending_swaps.Add(pending_swap)))
                .Resolve(previous);

            return updated;
        }

        /// <summary>
        /// Add or replace the given pending move to the list of pending 
        /// moves to cast on the upcoming turn.<br/>
        /// If the caster already has a move/swap registered it is 
        /// replaced/removed. <br/>
        /// Does nothing if the given team has been locked earlier.
        /// </summary>
        internal static TurnContext RegisterPendingBattleMove(
            TurnContext previous,
            TeamColour team,
            MoveAction pending_move)
        {
            var updated = PipelineBuilder.Seed
                .Ensure(() => TurnContext.IsMoveActionValid(
                    context: previous, 
                    team: team, 
                    move_action: pending_move))
                .Bind(() => new TurnContext(
                    battlefield_snapshot: previous.battlefield_snapshot,
                    waiting_for: previous.waiting_for,
                    pending_moves: previous.pending_moves.Add(pending_move),
                    pending_swaps: previous.pending_swaps))
                .Resolve(previous);

            return updated;
        }

        internal static bool IsMoveActionValid(
            TurnContext context,
            TeamColour team,
            MoveAction move_action)
        {
            var is_move_action_valid = PipelineBuilder.Seed
                .Ensure(() => _EnsureRegistrationsAreOpen(context, team))
                .Ensure(() => _EnsureCasterHasNotRegisteredMoves(context, move_action))
                .Ensure(() => _EnsureCasterIsNotSwappingOut(context, move_action))
                .Resolve();

            return is_move_action_valid;

            static bool _EnsureRegistrationsAreOpen(TurnContext context, TeamColour team)
            { 
                return TurnContext.AreRegistrationsOpen(context: context, team: team); 
            }

            static bool _EnsureCasterHasNotRegisteredMoves(TurnContext context, MoveAction move_action)
            {
                return !context.pending_moves.Any(_pending => _pending.caster_id == move_action.caster_id);
            }

            static bool _EnsureCasterIsNotSwappingOut(TurnContext context, MoveAction move_action)
            {
                return !context.pending_swaps.Any(_pending => _pending.outgoing == move_action.caster_id);
            }
        }

        /// <summary>
        /// Returns whether the given action is valid regarding current turn context state.
        /// Will typically return false when:
        /// * registrations are not open
        /// * this swap's incoming id is already registered for another swap
        /// </summary>
        internal static bool IsSwapActionValid(
            TurnContext context,
            TeamColour team,
            SwapAction swap_action)
        {
            var is_swap_action_valid = PipelineBuilder.Seed
                .Ensure(() => _EnsureRegistrationsAreOpen(context, team))
                .Ensure(() => _EnsureIncomingIsNotAlreadySwappingIn(context, swap_action))
                .Ensure(() => _EnsureOutgoingIsNotAlreadySwappingOut(context, swap_action))
                .Ensure(() => _EnsureOutgoingHasNotRegisteredMoves(context, swap_action))
                .Resolve();

            return is_swap_action_valid;

            static bool _EnsureRegistrationsAreOpen(TurnContext context, TeamColour team)
            { 
                return TurnContext.AreRegistrationsOpen(context: context, team: team); 
            }

            static bool _EnsureIncomingIsNotAlreadySwappingIn(TurnContext context, SwapAction swap_action)
            {
                return !context.pending_swaps.Any(_pending => _pending.incoming == swap_action.incoming);
            }

            static bool _EnsureOutgoingIsNotAlreadySwappingOut(TurnContext context, SwapAction swap_action)
            {
                return !context.pending_swaps.Any(_pending => _pending.outgoing == swap_action.outgoing);
            }

            static bool _EnsureOutgoingHasNotRegisteredMoves(TurnContext context, SwapAction swap_action)
            {
                return !context.pending_moves.Any(_pending => _pending.caster_id == swap_action.outgoing);
            }
        }

        internal static bool AreRegistrationsOpen(
            TurnContext context,
            TeamColour team)
        {
            return
                context.battlefield_snapshot != null
                && context.waiting_for.Contains(team);
        }

        internal static (ImmutableArray<SwapAction> registered_swaps, ImmutableArray<MoveAction> registered_moves) GetRegistrationsForTeam(
            TurnContext context,
            TeamColour team)
        {
            if (context.battlefield_snapshot == null)
            {
                return new();
            }
            var monster_ids_for_team = context.battlefield_snapshot.Value.field_monsters[team]
                .Select(_monster => _monster.id)
                .ToList();
            var registered_moves_for_team = context.pending_moves
                .Where(_pending => monster_ids_for_team.Contains(_pending.caster_id))
                .ToImmutableArray();
            var registered_swaps_for_team = context.pending_swaps
                .Where(_pending => monster_ids_for_team.Contains(_pending.outgoing))
                .ToImmutableArray();
            return (
                registered_swaps: registered_swaps_for_team, 
                registered_moves: registered_moves_for_team);
        }

        #region private static

        private static (short swap_index, short move_index) GetMonsterActionIndexes(
            TurnContext context,
            BattleMonsterId battle_monster_id)
        {
            // index of the swap already registered for this monster, if any
            short swap_idx = -1;
            for (byte i = 0; i < context.pending_swaps.Length; i++)
            {
                if (context.pending_swaps[i].outgoing == battle_monster_id)
                {
                    swap_idx = i;
                    break;
                }
            }
            // index of the move already registered for this monster, if any
            short move_idx = -1;
            for (byte i = 0; i < context.pending_moves.Length; i++)
            {
                if (context.pending_moves[i].caster_id == battle_monster_id)
                {
                    move_idx = i;
                    break;
                }
            }
            return (swap_index: swap_idx, move_index: move_idx);
        }

        #endregion
    }
}
