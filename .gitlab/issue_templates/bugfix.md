<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "type::bugfix" label:

- https://gitlab.com/shadome/shad.games.waterblue/issues?label_name%5B%5D=type::bugfix

and verify the issue you're about to submit isn't a duplicate.
--->

<!-- Feel free to delete any sections that are not applicable. -->

### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### What is the current _bug_ behavior?

<!-- Describe what actually happens. -->

### What is the expected _correct_ behavior?

<!-- Describe what you should see instead. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~"type::bugfix"
/milestone %"0.1.0"
