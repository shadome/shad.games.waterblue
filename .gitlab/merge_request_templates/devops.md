<!-- See Pipelines for the project: https://gitlab.com/shadome/shad.games.waterblue/-/pipelines -->

<!-- Feel free to delete any sections that are not applicable. -->

## Related issues

<!-- Link related issues below. -->

## What does this MR do?

<!-- Describe in detail what your merge request does and why. -->

/label ~"type::devops"
/milestone %"0.1.0"
