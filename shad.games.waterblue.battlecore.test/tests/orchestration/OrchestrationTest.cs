
using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using shad.games.waterblue.data;
using shad.games.waterblue.orchestration.model;
using shad.games.waterblue.orchestration.services;
using shad.games.waterblue.orchestration.services.objects;
using shad.games.waterblue.robot.services;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Threading;

namespace shad.games.waterblue.battlecore.test
{
    public partial class OrchestrationTest
    {

        [Fact]
        public async void TestBattle1()
        {
            var robot_callbacks = RobotService.GetRobotCallbacks();
            var robot_team = new[] {
                MonsterDictionary.n026_raichu,
                MonsterDictionary.n063_abra,
            }.ToImmutableArray();
            ClientToken token = default;
            var player_callbacks = new ClientCallbacks(
                onNewBattle: (_token, _) => { token = _token; },
                onNewTurn: (_battlefield) => _OnNewTurn(_battlefield, token),
                onSwapAction: (_, _, _) => {},
                onMoveAction: (_) => {},
                onRequiredSwap: (_, _, _) => {},
                onBattleEnd: (_, _battleresult) => _OnBattleEnd(_battleresult)
            );
            var player_team = new[] {
                MonsterDictionary.n091_cloyster,
                MonsterDictionary.n094_gengar,
            }.ToImmutableArray();
            await BattleService
                .RunBattleWithRobot(
                    player_team: player_team,
                    robot_team: robot_team,
                    player_callbacks: player_callbacks,
                    robot_callbacks: robot_callbacks);


            static void _OnBattleEnd(
                BattleResult result)
            {
                Assert.True(result == BattleResult.Win);
            }
            static void _OnNewTurn(
                Battlefield battlefield,
                ClientToken token)
            {
                var team = battlefield.field_monsters[token.team_colour]
                    .Where(_bmonster => !_bmonster.IsKnockedOut())
                    .ToImmutableArray();
                var target = battlefield.field_monsters[TeamColour.GetOpposite(token.team_colour)]
                    .FirstOrDefault(_bmonster => !_bmonster.IsKnockedOut());
                if (target != default)
                {
                    var target_position = battlefield.TryGetMonsterFieldPosition(target.id);
                    foreach (var _bmonster in team)
                    {
                        var move = _bmonster.moveset[0];
                        var move_action = MoveAction.Create(
                            caster_id: _bmonster.id,
                            move_id: move.id,
                            target_team: TeamColour.GetOpposite(token.team_colour),
                            target_position: target_position);
                        BattleService.RegisterMove(
                            token: token,
                            move_action: move_action);
                    }
                }
                BattleService.LockRegistrations(token);
            }
        }

        [Fact]
        public async void TestBattle_TwoTurnsWithSwaps()
        {
            var iteration = 0;
            var robot_callbacks = RobotService.GetRobotCallbacks();
            var robot_team = new[] {
                BattlecoreTest.QuickLowDmgWeakMonster("weak1"),
                BattlecoreTest.QuickLowDmgWeakMonster("weak2"),
                BattlecoreTest.QuickLowDmgWeakMonster("weak3"),
            }.ToImmutableArray();
            ClientToken token = default;
            var player_callbacks = new ClientCallbacks(
                onNewBattle: (_token, _) => { token = _token; },
                onNewTurn: (_battlefield) => _OnNewTurn(++iteration, _battlefield, token),
                onSwapAction: (_swapaction, _battlefieldbefore, _) => { 
                    // BattlecoreTest.PrintSwapAction(_battlefieldbefore, _swapaction); 
                    }, 
                onMoveAction: (_moveactionlog) => { 
                    // BattlecoreTest.PrintMoveAction(_battlefieldbefore, _moveaction); 
                    },
                onRequiredSwap: (_, _, _) => {},
                onBattleEnd: (_battlefield, _battleresult) => {
                    // BattlecoreTest.PrintBattlefield(_battlefield);
                    _OnBattleEnd(iteration, _battlefield, _battleresult);
                }
            );
            var player_team = new[] {
                BattlecoreTest.SlowBigDmgResistantMonster("strong1"),
                BattlecoreTest.SlowBigDmgResistantMonster("strong2"),
                BattlecoreTest.SlowBigDmgResistantMonster("strong3"),
            }.ToImmutableArray();
            // check how to write async test methods
            await BattleService
                .RunBattleWithRobot(
                    player_team: player_team, 
                    robot_team: robot_team, 
                    player_callbacks: player_callbacks, 
                    robot_callbacks: robot_callbacks);

            // System.Console.WriteLine("[TestBattle1] ended");

            static void _OnBattleEnd(
                int iteration,
                Battlefield battlefield,
                BattleResult result)
            {
                Assert.True(result == BattleResult.Win);
                Assert.True(iteration == 2);
            }
            static void _OnNewTurn(
                int iteration,
                Battlefield battlefield,
                ClientToken token)
            {
                // BattlecoreTest.PrintBattlefield(battlefield);
                switch (iteration)
                {
                    // turn 1: 
                    // * player's left monster (1) swaps with 3
                    // * player's 2 OHKOs robot's 1
                    // * at the end of the turn, robot's 1 swaps out with robot's 3
                    case 1:
                        var players_1 = battlefield.field_monsters[token.team_colour][0];
                        var players_2 = battlefield.field_monsters[token.team_colour][1];
                        var players_3 = battlefield.party_monsters[token.team_colour][0];
                        var swap_action = SwapAction.Create(outgoing: players_1.id, incoming: players_3.id);
                        BattleService.RegisterSwap(token, swap_action);
                        var robot_1 = battlefield.field_monsters[TeamColour.GetOpposite(token.team_colour)][0];
                        var move_action = MoveAction.Create(
                            caster_id: players_2.id,
                            move_id: players_2.moveset[0].id,
                            target_position: battlefield.TryGetMonsterFieldPosition(robot_1.id)!.Value);
                        BattleService.RegisterMove(token: token, move_action: move_action);
                        BattleService.LockRegistrations(token);
                        break;
                    // turn 2: 
                    // * player's new 1 OHKOs robot's 2
                    // * player's 2 OHKOs robot's new 1
                    // * at the end of the turn, player should win
                    case 2:
                        players_1 = battlefield.field_monsters[token.team_colour][0];
                        players_2 = battlefield.field_monsters[token.team_colour][1];
                        robot_1 = battlefield.field_monsters[TeamColour.GetOpposite(token.team_colour)][0];
                        var robot_2 = battlefield.field_monsters[TeamColour.GetOpposite(token.team_colour)][1];
                        move_action = MoveAction.Create(
                            caster_id: players_1.id,
                            move_id: players_1.moveset[0].id,
                            target_position: battlefield.TryGetMonsterFieldPosition(robot_2.id)!.Value);
                        BattleService.RegisterMove(token: token, move_action: move_action);
                        move_action = MoveAction.Create(
                            caster_id: players_2.id,
                            move_id: players_2.moveset[0].id,
                            target_position: battlefield.TryGetMonsterFieldPosition(robot_1.id)!.Value);
                        BattleService.RegisterMove(token: token, move_action: move_action);
                        BattleService.LockRegistrations(token);
                        break;
                    default:
                        Assert.True(false);
                        break;
                }
            }
        }

        [Fact]
        public async void TestBattle_BadReplacementOnSwapOut()
        {
            var robot_callbacks = RobotService.GetRobotCallbacks();
            var robot_team = new[] {
                BattlecoreTest.SlowBigDmgResistantMonster("strong1"),
            }.ToImmutableArray();
            ClientToken token = default;
            var player_team = new[] {
                BattlecoreTest.QuickLowDmgWeakMonster("weak1"),
                BattlecoreTest.QuickLowDmgWeakMonster("weak2"),
                BattlecoreTest.QuickLowDmgWeakMonster("weak3"),
                BattlecoreTest.QuickLowDmgWeakMonster("weak4"),
            }.ToImmutableArray();
            var exception = default(OrchestrationException);
            
            // Test not registering anything when prompted for a required swap
            exception = await Assert.ThrowsAsync<OrchestrationException>(async () =>
                await BattleService
                    .RunBattleWithRobot(
                        player_team: player_team, 
                        robot_team: robot_team, 
                        player_callbacks: CreateCallbacks(_OnRequiredSwap_LockWithoutRegistration),
                        robot_callbacks: robot_callbacks));

            // Test registering a bad incoming monster id when prompted for a required swap
            exception = await Assert.ThrowsAsync<OrchestrationException>(async () =>
                await BattleService
                    .RunBattleWithRobot(
                        player_team: player_team, 
                        robot_team: robot_team, 
                        player_callbacks: CreateCallbacks(_OnRequiredSwap_LockBadIncomingRegistration),
                        robot_callbacks: robot_callbacks));

            // Test registering a bad outgoing monster id when prompted for a required swap
            exception = await Assert.ThrowsAsync<OrchestrationException>(async () =>
                await BattleService
                    .RunBattleWithRobot(
                        player_team: player_team, 
                        robot_team: robot_team, 
                        player_callbacks: CreateCallbacks(_OnRequiredSwap_LockBadOutgoingRegistration),
                        robot_callbacks: robot_callbacks));

            void _OnRequiredSwap_LockBadOutgoingRegistration(
                Battlefield battlefield,
                BattleMonsterId outgoing_id,
                ImmutableArray<BattleMonsterId> eligible_replacements)
            {
                var outgoing_monster = battlefield.field_monsters[token.team_colour][0];
                var ineligible_monster = battlefield.field_monsters[token.team_colour][1];
                var first_party_monster = battlefield.party_monsters[token.team_colour][0];
                Assert.Equal(outgoing_id, outgoing_monster.id);
                Assert.NotEqual(outgoing_monster.id, ineligible_monster.id);
                Assert.DoesNotContain(expected: ineligible_monster.id, collection: eligible_replacements);
                Assert.Contains(expected: first_party_monster.id, collection: eligible_replacements);
                var swap_action = SwapAction.Create(outgoing: ineligible_monster.id, incoming: first_party_monster.id);
                BattleService.RegisterSwap(token, swap_action);
                BattleService.LockRegistrations(token);
            }

            void _OnRequiredSwap_LockBadIncomingRegistration(
                Battlefield battlefield,
                BattleMonsterId outgoing_id,
                ImmutableArray<BattleMonsterId> eligible_replacements)
            {
                var outgoing_monster = battlefield.field_monsters[token.team_colour][0];
                var ineligible_monster = battlefield.field_monsters[token.team_colour][1];
                Assert.Equal(outgoing_id, outgoing_monster.id);
                Assert.DoesNotContain(expected: ineligible_monster.id, collection: eligible_replacements);
                var swap_action = SwapAction.Create(outgoing: outgoing_monster.id, incoming: ineligible_monster.id);
                BattleService.RegisterSwap(token, swap_action);
                BattleService.LockRegistrations(token);
            }

            void _OnRequiredSwap_LockWithoutRegistration(
                Battlefield battlefield,
                BattleMonsterId outgoing_id,
                ImmutableArray<BattleMonsterId> eligible_replacements)
            {
                var outgoing_monster = battlefield.field_monsters[token.team_colour][0];
                var ineligible_monster = battlefield.field_monsters[token.team_colour][1];
                Assert.Equal(outgoing_id, outgoing_monster.id);
                Assert.DoesNotContain(expected: ineligible_monster.id, collection: eligible_replacements);
                BattleService.LockRegistrations(token);
            }

            ClientCallbacks CreateCallbacks(Action<Battlefield, BattleMonsterId, ImmutableArray<BattleMonsterId>> on_required_swap)
            {
                return new ClientCallbacks(
                    onNewBattle: (_token, _) => { token = _token; },
                    onNewTurn: (_battlefield) => { BattleService.LockRegistrations(token); },
                    onSwapAction: (_swapaction, _battlefieldbefore, _) => { },
                    onMoveAction: (_moveactionlog) => { },
                    onRequiredSwap: on_required_swap,
                    onBattleEnd: (_battlefield, _battleresult) => { });
            }
        }

    }
}
