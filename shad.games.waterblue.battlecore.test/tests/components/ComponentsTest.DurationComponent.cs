using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class ComponentsTest
    {
        [Fact]
        public void TestDurationComponent()
        {
            // Expected duration component behaviour:
            // the effect with a duration is attached
            // and will last the specified number of turns / ticks
            // before being detached.

            // System.Console.WriteLine("[TestDurationComponent] started");

            var effect_list = new List<(string name, byte duration)> 
            {
                ("debuff_effect_1", 0),
                ("debuff_effect_2", 1),
                ("debuff_effect_3", 9),
            };

            var battlefield = CreateBattlefield(effect_list);
            var caster_id = GetCaster(battlefield).id;
            // applies all debuffs on the caster
            var move_action = MoveAction.Create(
                caster_id: caster_id,
                move_id: GetCaster(battlefield).moveset[0].id);
            battlefield = Battlecore
                .DoCast(battlefield, move_action)
                .battlefield_after;

            // expected 2 debuffs (duration 0 is not applied)
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Length == 2);
            Assert.False(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[0].name));
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[1].name));
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[2].name));

            // Tick the second effect once: should make it disappear
            var debuff_id = GetDebuffId(battlefield, effect_list[1].name);
            battlefield = battlefield.DoTickEffect(debuff_id);
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Length == 1);
            Assert.False(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[1].name));

            // Tick the third effect eight times: should not make it disappear
            debuff_id = GetDebuffId(battlefield, effect_list[2].name);
            for (int i = 0; i < 8; i++)
            {
                battlefield = battlefield.DoTickEffect(debuff_id);
            }
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Length == 1);
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[2].name));

            // Tick the third effect one more time: should make it disappear
            debuff_id = GetDebuffId(battlefield, effect_list[2].name);
            battlefield = battlefield.DoTickEffect(debuff_id);
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Length == 0);
            Assert.False(battlefield.GetBattleMonsterDebuffs(caster_id).Any(x => x.effect.name == effect_list[2].name));

            // System.Console.WriteLine("[TestDurationComponent] ended");

            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }
            static BattleEffectId GetDebuffId(Battlefield battlefield, string debuff_name)
            {
                return battlefield.battle_effects.First(_effect => _effect.effect.name == debuff_name).id;
            }
            static Battlefield CreateBattlefield(List<(string name, byte duration)> effects)
            {
                var move_effects = effects
                    .Select(tuple => Effect.Create(
                        default_components: ImmutableArray.CreateRange(new [] {
                            TargetMonsterComponent.Create(), // target component is needed for the effect to be attached
                            TriggerComponent.Create(), // trigger or recurrence components is needed for the effect to be attached
                            DurationComponent.Create(tuple.duration),
                        }),
                        name: tuple.name));
                var moves = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.FixedSelf,
                        effects: ImmutableArray.CreateRange(move_effects)),
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    BattleMonster.Create(DummyMonster(moves)),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }
    }
}
