using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class ComponentsTest
    {

        private static Effect DummyEffect() => Effect.Create(
            default_components: ImmutableArray.CreateRange(new [] {
                ImmediateEffectComponent.Create(),
                DamageComponent.Create(Type.Dark, 50),
            }),
            name: "direct_damage");

        private static Effect BigDamageEffect() => Effect.Create(
            default_components: ImmutableArray.CreateRange(new [] {
                ImmediateEffectComponent.Create(),
                DamageComponent.Create(Type.Dark, byte.MaxValue),
            }),
            name: "direct_damage");

        private static ImmutableArray<Effect> DummyEffects() => ImmutableArray.CreateRange(new[] { DummyEffect() });

        private static Move DummyMove() => Move.Create(name: "move", move_target_type: MoveTargetType.NotApplicable, effects: ImmutableArray<Effect>.Empty);

        private static ImmutableArray<Move> DummyMoves() => ImmutableArray.CreateRange(new[] { DummyMove() });

        private static Stats DummyStats() => Stats.Create(100, 100, 100, 100, 100, 100);

        private static Species DummySpecies() => Species.Create(number: 26, name: "raichu", type_1: Type.Electric, basestats: DummyStats());

        private static Monster DummyMonster(ImmutableArray<Move>? moves = null) =>
            Monster.Create(name: "raichu", level: 50, moves: moves ?? DummyMoves(), species: DummySpecies());

        private static BattleMonster DummyBattleMonster() => BattleMonster.Create(DummyMonster());

        private static void PrintBattlefield(Battlefield battlefield)
        {
            System.Console.WriteLine($"----------------------------------------");
            System.Console.WriteLine($"------- DISPLAYING BATTLEFIELD ---------");
            System.Console.WriteLine($"----------------------------------------");
            foreach (var _team in new[] { TeamColour.Blue, TeamColour.Grey })
            {
                var team = _team == TeamColour.Blue ? "BLUE" : "GREY";
                System.Console.WriteLine($"Team: {team}");
                System.Console.WriteLine($"  On battlefield:");
                foreach (var _bmonster in battlefield.field_monsters[_team])
                {
                    System.Console.WriteLine($"   {_bmonster.monster.name} {_bmonster.health}");
                }
                System.Console.WriteLine($"  In party:");
                foreach (var _bmonster in battlefield.party_monsters[_team])
                {
                    System.Console.WriteLine($"   {_bmonster.monster.name} {_bmonster.health}");
                }
            }
            System.Console.WriteLine($"----------------------------------------");
        }

        public static BattleMonster KnockOut(BattleMonster monster)
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.CreateRange(new[] { big_damage_effect }));
            var moves = ImmutableArray.CreateRange(new[] { big_damage_move });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                monster,
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            int i = 0;
            while (!monster.IsKnockedOut())
            {
                battlefield = battlefield
                    .DoCast(MoveAction.Create(
                        caster_id: grey_array[0].id,
                        move_id: grey_array[0].moveset[0].id))
                    .battlefield_after;
                monster = battlefield.field_monsters[TeamColour.Blue][0];
                i++;
                if (i > 5000)
                {
                    throw new System.Exception("endless loop");
                }
            }
            return monster;
        }
        
    }
}
