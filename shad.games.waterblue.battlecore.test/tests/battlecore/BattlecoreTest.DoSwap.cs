using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void DoSwapTest()
        {
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: DummyMoves(), species: DummySpecies())),
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue3", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue4", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue5", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue6", level: 50, moves: DummyMoves(), species: DummySpecies())),
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            for (byte i = 0; i < teams.Length; i++)
            {
                var (team_colour, array) = teams[i];
                for (byte outgoing_idx = 0; outgoing_idx < 2; outgoing_idx++)
                {
                    for (byte incoming_idx = 2; incoming_idx < 6; incoming_idx++)
                    {
                        var outgoing_id = array[outgoing_idx].id;
                        var incoming_id = array[incoming_idx].id;
                        var swap_action = SwapAction.Create(
                            outgoing: outgoing_id,
                            incoming: incoming_id);
                        var battlefield_before = battlefield;
                        var battlefield_after = Battlecore.DoSwap(
                            battlefield: battlefield_before,
                            swap_action: swap_action);
                        Assert.False(battlefield_before.field_monsters[team_colour][outgoing_idx]
                            == battlefield_after.field_monsters[team_colour][outgoing_idx]);
                        Assert.False(battlefield_before.party_monsters[team_colour][incoming_idx - 2]
                            == battlefield_after.party_monsters[team_colour][incoming_idx - 2]);
                        Assert.True(battlefield_before.field_monsters[team_colour][outgoing_idx]
                            == battlefield_after.party_monsters[team_colour][incoming_idx - 2]);
                        Assert.True(battlefield_before.party_monsters[team_colour][incoming_idx - 2]
                            == battlefield_after.field_monsters[team_colour][outgoing_idx]);
                    }
                }
            }
        }

    }
}
