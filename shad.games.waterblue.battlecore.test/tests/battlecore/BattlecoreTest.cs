using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {

        internal static Monster QuickLowDmgWeakMonster(string name = "quick_low_dmg_weak_monster") => Monster.Create(
            name: name,
            species: Species.Create(
                number: 42,
                name: "quick_low_dmg_weak_monster_species",
                basestats: QuickLowDmgWeakStats(),
                type_1: Type.Water),
                level: 50,
                moves: ImmutableArray.CreateRange(new[] { DummyMove() }));

        internal static Stats QuickLowDmgWeakStats() => Stats.Create(health: 1, attack: 1, defense: 1, specialattack: 1, specialdefense: 1, speed: short.MaxValue);

        internal static Monster SlowBigDmgResistantMonster(string name = "slow_big_dmg_monster") => Monster.Create(
            name: name,
            species: Species.Create(
                number: 42,
                name: "slow_big_dmg_monster_species",
                basestats: SlowBigDamageResistantStats(),
                type_1: Type.Water),
                level: 50,
                moves: ImmutableArray.CreateRange(new[] { BigDamageMove(MoveTargetType.ChoiceBattlefieldSingleExceptCaster) }));

        internal static Stats SlowBigDamageResistantStats() => Stats.Create(health: short.MaxValue, attack: short.MaxValue, defense: short.MaxValue, specialattack: short.MaxValue, specialdefense: short.MaxValue, speed: 1);

        internal static Move BigDamageMove(MoveTargetType target_type) => Move.Create(
            name: "boum!",
            move_target_type: target_type,
            effects: ImmutableArray.CreateRange(new[] { BattlecoreTest.BigDamageEffect()}));

        internal static Effect DummyEffect() => Effect.Create(
            default_components: ImmutableArray.CreateRange(new [] {
                ImmediateEffectComponent.Create(),
                DamageComponent.Create(Type.Dark, 50),
            }),
            name: "direct_damage");

        internal static Effect BigDamageEffect() => Effect.Create(
            default_components: ImmutableArray.CreateRange(new [] {
                ImmediateEffectComponent.Create(),
                DamageComponent.Create(Type.Dark, byte.MaxValue),
            }),
            name: "direct_damage");

        internal static ImmutableArray<Effect> DummyEffects() => ImmutableArray.CreateRange(new[] { DummyEffect() });

        internal static Move DummyMove() => Move.Create(name: "move", move_target_type: MoveTargetType.NotApplicable, effects: ImmutableArray<Effect>.Empty);

        internal static ImmutableArray<Move> DummyMoves() => ImmutableArray.CreateRange(new[] { DummyMove() });

        internal static Stats DummyStats() => Stats.Create(100, 100, 100, 100, 100, 100);

        internal static Species DummySpecies() => Species.Create(number: 26, name: "raichu", type_1: Type.Electric, basestats: DummyStats());

        internal static Monster DummyMonster(ImmutableArray<Move>? moves = null) =>
            Monster.Create(name: "raichu", level: 50, moves: moves ?? DummyMoves(), species: DummySpecies());

        internal static BattleMonster DummyBattleMonster() => BattleMonster.Create(DummyMonster());

        internal static void PrintBattlefield(Battlefield battlefield)
        {
            System.Console.WriteLine($"----------------------------------------");
            System.Console.WriteLine($"------- DISPLAYING BATTLEFIELD ---------");
            System.Console.WriteLine($"----------------------------------------");
            foreach (var _team in new[] { TeamColour.Blue, TeamColour.Grey })
            {
                var team = _team == TeamColour.Blue ? "BLUE" : "GREY";
                System.Console.WriteLine($"Team: {team}");
                System.Console.WriteLine($"  On battlefield:");
                foreach (var _bmonster in battlefield.field_monsters[_team])
                {
                    System.Console.WriteLine($"   {_bmonster.monster.name} {_bmonster.health}");
                }
                System.Console.WriteLine($"  In party:");
                foreach (var _bmonster in battlefield.party_monsters[_team])
                {
                    System.Console.WriteLine($"   {_bmonster.monster.name} {_bmonster.health}");
                }
            }
            System.Console.WriteLine($"----------------------------------------");
        }

        internal static void PrintMoveAction(Battlefield battlefield, MoveAction move_action)
        {
            var caster = battlefield[battlefield.TryGetMonsterFieldPosition(move_action.caster_id)!.Value];
            var target = move_action.target_position == null
                ? (BattleMonster?)null
                : battlefield[move_action.target_position!.Value];
            var move = caster.moveset.First(x => x.id == move_action.move_id);
            System.Console.WriteLine($"[MOVE_ACTION] {caster.monster.name} uses {move.move.name}{(target == null ? string.Empty : $" on {target!.Value.monster.name}")}");
        }

        internal static void PrintSwapAction(Battlefield battlefield, SwapAction swap_action)
        {
            var outgoing = battlefield[battlefield.TryGetMonsterFieldPosition(swap_action.outgoing)!.Value];
            var incoming = battlefield[battlefield.TryGetMonsterPartyPosition(swap_action.incoming)!.Value];
            System.Console.WriteLine($"[SWAP_ACTION] {outgoing.monster.name} goes out and {incoming.monster.name} comes in");
        }

        internal static BattleMonster KnockOut(BattleMonster monster)
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.CreateRange(new[] { big_damage_effect }));
            var moves = ImmutableArray.CreateRange(new[] { big_damage_move });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                monster,
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            int i = 0;
            while (!monster.IsKnockedOut())
            {
                battlefield = battlefield
                    .DoCast(MoveAction.Create(
                        caster_id: grey_array[0].id,
                        move_id: grey_array[0].moveset[0].id))
                    .battlefield_after;
                monster = battlefield.field_monsters[TeamColour.Blue][0];
                i++;
                if (i > 5000)
                {
                    throw new System.Exception("endless loop");
                }
            }
            return monster;
        }
        
    }
}
