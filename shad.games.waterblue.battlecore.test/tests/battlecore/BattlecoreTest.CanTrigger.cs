using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void CanTriggerTest()
        {
            // System.Console.WriteLine("[CanTriggerTest] started");

            var can_trigger_configurations = new (byte power, byte warmup, bool expected)[]
            {
                (power: 0, warmup: 0, expected: true),
                (power: 1, warmup: 0, expected: true),
                (power: byte.MaxValue, warmup: 0, expected: true),
            };
            var cannot_trigger_configurations = new (byte power, byte warmup, bool expected)[]
            {
                (power: 1, warmup: 1, expected: false),
                (power: 1, warmup: byte.MaxValue, expected: false),
                (power: 0, warmup: 1, expected: false),
            };
            foreach (var (power, warmup, expected) in can_trigger_configurations.Concat(cannot_trigger_configurations))
            {
                var battlefield = CreateBattlefield(power, warmup);
                var move_action = MoveAction.Create(
                    caster_id: GetCaster(battlefield).id,
                    target_position: GetCasterPosition(battlefield),
                    move_id: GetCaster(battlefield).moveset[0].id);
                battlefield = Battlecore
                    .DoCast(battlefield, move_action)
                    .battlefield_after;
                // somehow testing DoCast's application of expected BattleEffect
                var caster_id = GetCaster(battlefield).id;
                Assert.False(battlefield.GetBattleMonsterDebuffs(caster_id).IsDefaultOrEmpty);
                Assert.True(battlefield.CanTrigger(battlefield.GetBattleMonsterDebuffs(caster_id)[0].id) == expected);
            }

            // System.Console.WriteLine("[CanTriggerTest] ended");
            
            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }

            static FieldPosition GetCasterPosition(Battlefield battlefield)
            {
                var caster = GetCaster(battlefield);
                var position = battlefield.TryGetMonsterFieldPosition(caster.id);
                Assert.True(position != null);
                return position!.Value;
            }

            static Battlefield CreateBattlefield(byte power, byte warmup)
            {
                var moves = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.FixedSelf,
                        effects: ImmutableArray.CreateRange(new[]
                        {
                            Effect.Create(
                                default_components: ImmutableArray.CreateRange(new [] {
                                    DamageComponent.Create(Type.Dark, power),
                                    TargetMonsterComponent.Create(),
                                    RecurrenceComponent.Create(EffectRecurrence.Postturn),
                                    DurationComponent.Create(1),
                                    WarmupComponent.Create(warmup),
                                }),
                                name: "direct_damage")
                        })),
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    BattleMonster.Create(DummyMonster(moves)),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }
    }
}
