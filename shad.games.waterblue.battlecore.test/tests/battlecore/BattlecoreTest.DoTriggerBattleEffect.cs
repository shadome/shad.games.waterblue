using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void DoTickEffectTest_Damage()
        {
            // System.Console.WriteLine("[DoTickEffectTest_Damage] started");

            var battlefield = CreateBattlefield();
            var caster_id = GetCaster(battlefield).id;
            // applies all debuffs on the caster
            var move_action = MoveAction.Create(
                    caster_id: caster_id,
                    move_id: GetCaster(battlefield).moveset[0].id);
            battlefield = Battlecore
                .DoCast(battlefield, move_action)
                .battlefield_after;
            var battlefield_after = battlefield;

            // expected 5 debuffs (out of 7, but 2 with duration == 0)
            Assert.True(battlefield.GetBattleMonsterDebuffs(caster_id).Length == 5); // redundant with DoCast test
            // debuff_effect_1: warmup = 0, duration = 1, power > 0 triggered, expected 4 remaining debuffs and health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_1"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 4);
            Assert.True(GetCaster(battlefield_after).health < GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // debuff_effect_2: warmup = 1, duration = 1, power > 0 triggered once, expected 4 remaining debuffs and no health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_2"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 4);
            Assert.True(GetCaster(battlefield_after).health == GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // debuff_effect_2: warmup = 1, duration = 1, power > 0 triggered twice, expected 3 remaining debuffs and health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_2"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 3);
            Assert.True(GetCaster(battlefield_after).health < GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // debuff_effect_3 does not exist on the caster because duration = 0
            // debuff_effect_4 does not exist on the caster because duration = 0

            // note: testing another effect index than monster.effects[0]
            // debuff_effect_7: warmup = 0, duration = 2, power = 0 triggered once, expected 3 remaining debuffs and no health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_7"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 3);
            Assert.True(GetCaster(battlefield_after).health == GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // note: testing another effect index than monster.effects[0]
            // debuff_effect_7: warmup = 0, duration = 2, power = 0 triggered twice, expected 2 remaining debuffs and no health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_7"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 2);
            Assert.True(GetCaster(battlefield_after).health == GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // note: Preturn turn stage
            // debuff_effect_5: warmup = 0, duration = 2, power > 0 triggered once, expected 2 remaining debuffs and some health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_5"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 2);
            Assert.True(GetCaster(battlefield_after).health < GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // note: Preturn turn stage
            // debuff_effect_5: warmup = 0, duration = 2, power > 0 triggered twice, expected 1 remaining debuffs and some health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_5"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 1);
            Assert.True(GetCaster(battlefield_after).health < GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // note: Preturn turn stage
            // debuff_effect_6: warmup = 0, duration = 2, power = 0 triggered once, expected 1 remaining debuffs and no health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_6"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 1);
            Assert.True(GetCaster(battlefield_after).health == GetCaster(battlefield).health);
            battlefield = battlefield_after;

            // note: Preturn turn stage
            // debuff_effect_6: warmup = 0, duration = 2, power = 0 triggered twice, expected 0 remaining debuffs and no health loss
            battlefield_after = Battlecore.DoTickEffect(
                battlefield, 
                GetDebuffId(battlefield, "debuff_effect_6"));
            Assert.True(battlefield_after.GetBattleMonsterDebuffs(caster_id).Length == 0);
            Assert.True(GetCaster(battlefield_after).health == GetCaster(battlefield).health);
            battlefield = battlefield_after;
            
            // System.Console.WriteLine("[DoTickEffectTest_Damage] ended");

            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }
            static BattleEffectId GetDebuffId(Battlefield battlefield, string debuff_name)
            {
                return battlefield.battle_effects.First(_effect => _effect.effect.name == debuff_name).id;
            }
            static Battlefield CreateBattlefield()
            {
                var debuff_effect_1 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(1),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(0),
                    }),
                    name: "debuff_effect_1");
                var debuff_effect_2 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(1),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(1),
                    }),
                    name: "debuff_effect_2");
                var debuff_effect_3 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(0),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(1),
                    }),
                    name: "debuff_effect_3");
                var debuff_effect_4 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(0),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(0),
                    }),
                    name: "debuff_effect_4");
                var debuff_effect_5 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Preturn),
                        DurationComponent.Create(2),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(0),
                    }),
                    name: "debuff_effect_5");
                var debuff_effect_6 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 0),
                        RecurrenceComponent.Create(EffectRecurrence.Preturn),
                        DurationComponent.Create(2),
                        TargetMonsterComponent.Create(),
                    }),
                    name: "debuff_effect_6");
                var debuff_effect_7 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 0),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(2),
                        TargetMonsterComponent.Create(),
                    }),
                    name: "debuff_effect_7");
                var moves = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.FixedSelf,
                        effects: ImmutableArray.CreateRange(new[]
                        {
                            debuff_effect_1,
                            debuff_effect_2,
                            debuff_effect_3,
                            debuff_effect_4,
                            debuff_effect_5,
                            debuff_effect_6,
                            debuff_effect_7,
                        })),
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    BattleMonster.Create(DummyMonster(moves)),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }
    }
}
