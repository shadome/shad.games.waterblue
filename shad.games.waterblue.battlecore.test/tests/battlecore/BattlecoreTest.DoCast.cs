using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {

        [Fact]
        public void DoCastTest_DirectDamage()
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.Create(big_damage_effect));
            var moves = ImmutableArray.Create(big_damage_move);
            var grey_array = ImmutableArray.Create(BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())));
            var blue_array = ImmutableArray.Create(BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: moves, species: DummySpecies())));
            var teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            });
            var battlefield_before = Battlefield.Create(teams);
            var battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: grey_array[0].id,
                    move_id: grey_array[0].moveset[0].id))
                .battlefield_after;
            var health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            var health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
        }

        [Fact]
        public void DoCastTest_MoveActionLog()
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.Create(big_damage_effect));
            var moves = ImmutableArray.Create(big_damage_move);
            var grey_array = ImmutableArray.Create(BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())));
            var blue_array = ImmutableArray.CreateRange(new[] {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: moves, species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: moves, species: DummySpecies())),
            });
            var teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            });
            var tmp_battlefield = Battlefield.Create(teams);
            var move_action = MoveAction.Create(
                caster_id: grey_array[0].id,
                move_id: grey_array[0].moveset[0].id);
            var move_action_log = tmp_battlefield
                .DoCast(move_action);
            var battlefield_before = move_action_log.battlefield_before;
            var battlefield_after = move_action_log.battlefield_after;
            var health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            var health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            // note: battlefield / moveaction equality functions are not overriden, might be unsufficient
            Assert.True(!battlefield_before.Equals(battlefield_after));
            Assert.True(battlefield_before.Equals(tmp_battlefield));
            Assert.True(move_action_log.move_action.Equals(move_action));
            Assert.True(move_action_log.mitigation_logs.Length == 2);
            Assert.True(move_action_log.mitigation_logs.Any(_log => _log.monster_id == blue_array[0].id));
            Assert.True(move_action_log.mitigation_logs.Any(_log => _log.monster_id == blue_array[1].id));
            for (int i = 0; i < 2; i++)
            {
                var mitigation_log_entry = move_action_log.mitigation_logs[i];
                Assert.True(mitigation_log_entry.mitigation > 0);
                Assert.True(mitigation_log_entry.mitigation_overflow > 0);
                Assert.True(mitigation_log_entry.mitigation_absorbed == 0);
                Assert.True(mitigation_log_entry.mitigation > mitigation_log_entry.mitigation_overflow);
                Assert.True(mitigation_log_entry.has_knocked_out_target);
            }
        }

        [Fact]
        public void DoCastTest_TypeEffectiveness()
        {
            // monster with one psychic attack
            var psychic_monster = BattleMonster.Create(
                Monster.Create(
                    name: "psychic_monster", 
                    level: 50, 
                    species: Species.Create(number: 1, name: "psychic_monster", type_1: Type.Psychic, basestats: DummyStats()),
                    moves: ImmutableArray.Create(
                        Move.Create(
                            move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                            effects: ImmutableArray.Create(
                                Effect.Create(
                                    default_components: ImmutableArray.CreateRange(new [] 
                                    {
                                        ImmediateEffectComponent.Create(),
                                        DamageComponent.Create(Type.Psychic, 20),
                                    })))))));
            // defending monsters
            var defending_monster_neutral = _GetDefendingMonster(Type.Normal, null);
            var defending_monster_pseudo_neutral = _GetDefendingMonster(Type.Fightning, Type.Psychic);
            var defending_monster_immune = _GetDefendingMonster(Type.Fightning, Type.Dark);
            var defending_monster_resistant_1 = _GetDefendingMonster(Type.Psychic, null);
            var defending_monster_resistant_2 = _GetDefendingMonster(Type.Steel, Type.Psychic);
            var defending_monster_weak_1 = _GetDefendingMonster(Type.Normal, Type.Fightning);
            var defending_monster_weak_2 = _GetDefendingMonster(Type.Poison, Type.Fightning);

            /* Test immune */
            var teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_immune)),
            });
            var battlefield_before = Battlefield.Create(teams);
            var battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            var health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            var health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before == health_after);

            /* Test neutral */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_neutral)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            var health_result_neutral = health_after;
            
            /* Test resistance (x1) */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_resistant_1)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            Assert.True(health_after > health_result_neutral);
            var health_result_resistant_1 = health_after;
            
            /* Test resistance (x2) */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_resistant_2)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            Assert.True(health_after > health_result_resistant_1);
            
            /* Test weakness (x1) */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_weak_1)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            Assert.True(health_after < health_result_neutral);
            var health_result_weak_1 = health_after;
            
            /* Test resistance (x2) */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_weak_2)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            Assert.True(health_after < health_result_weak_1);
            
            /* Test pseudo-neutral */
            teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster_pseudo_neutral)),
            });
            battlefield_before = Battlefield.Create(teams);
            battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id))
                .battlefield_after;
            health_before = battlefield_before.field_monsters[TeamColour.Blue][0].health;
            health_after = battlefield_after.field_monsters[TeamColour.Blue][0].health;
            Assert.True(health_before > health_after);
            Assert.True(health_result_resistant_1 > health_after);
            Assert.True(health_result_weak_1 < health_after);
            

            static BattleMonster _GetDefendingMonster(Type type1, Type? type2)
                => BattleMonster.Create(
                    Monster.Create(
                        name: "defending_monster",
                        level: 50,
                        moves: BattlecoreTest.DummyMoves(),
                        species: Species.Create(
                            number: 1,
                            name: "defending_monster",
                            type_1: type1,
                            type_2: type2,
                            basestats: DummyStats())));
        }

        [Fact]
        public void DoCastTest_STAB()
        {
            // psychic monster with one psychic attack and one normal attack, both the same except their types
            var psychic_monster = BattleMonster.Create(
                Monster.Create(
                    name: "psychic_monster", 
                    level: 50,
                    species: Species.Create(number: 1, name: "psychic_monster", type_1: Type.Psychic, basestats: DummyStats()),
                    moves: ImmutableArray.Create(
                        Move.Create(
                            move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                            effects: ImmutableArray.Create(
                                Effect.Create(
                                    default_components: ImmutableArray.CreateRange(new [] 
                                    {
                                        ImmediateEffectComponent.Create(),
                                        DamageComponent.Create(Type.Psychic, 20),
                                    })))),
                        Move.Create(
                            move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                            effects: ImmutableArray.Create(
                                Effect.Create(
                                    default_components: ImmutableArray.CreateRange(new [] 
                                    {
                                        ImmediateEffectComponent.Create(),
                                        DamageComponent.Create(Type.Normal, 20),
                                    })))))));
            // defending monster
            var defending_monster = _GetDefendingMonster(Type.Normal, null);
            // test
            var teams = ImmutableArray.CreateRange(new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(psychic_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(defending_monster)),
            });
            var battlefield_before = Battlefield.Create(teams);
            var stabbed_log = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[0].id));

            Assert.True(stabbed_log.mitigation_logs.Length == 1);
            Assert.True(stabbed_log.mitigation_logs[0].mitigation > 0);

            var unstabbed_log = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: psychic_monster.id,
                    move_id: psychic_monster.moveset[1].id));

            Assert.True(unstabbed_log.mitigation_logs.Length == 1);
            Assert.True(unstabbed_log.mitigation_logs[0].mitigation > 0);

            Assert.True(stabbed_log.mitigation_logs[0].mitigation > unstabbed_log.mitigation_logs[0].mitigation);


            static BattleMonster _GetDefendingMonster(Type type1, Type? type2)
                => BattleMonster.Create(
                    Monster.Create(
                        name: "defending_monster",
                        level: 50,
                        moves: BattlecoreTest.DummyMoves(),
                        species: Species.Create(
                            number: 1,
                            name: "defending_monster",
                            type_1: type1,
                            type_2: type2,
                            basestats: DummyStats())));
        }

        
        [Fact]
        public void DoCastTest_ApplyBattleEffects()
        {
            var battlefield = CreateBattlefield();
            // applies 3 debuffs on the caster (0 on the target)
            var move_action_1 = MoveAction.Create(
                caster_id: GetCaster(battlefield).id,
                // not relevant but should not fail
                target_position: GetCasterOpponentPosition(battlefield),
                move_id: GetCaster(battlefield).moveset[0].id);
            // applies 3 debuffs on the target
            var move_action_2 = MoveAction.Create(
                caster_id: GetCaster(battlefield).id,
                target_position: GetCasterOpponentPosition(battlefield),
                move_id: GetCaster(battlefield).moveset[1].id);
            // applies 1 debuff on the target
            var move_action_3 = MoveAction.Create(
                caster_id: GetCaster(battlefield).id,
                target_position: GetCasterOpponentPosition(battlefield),
                move_id: GetCaster(battlefield).moveset[2].id);
            // applies 0 debuffs on the target
            var move_action_4 = MoveAction.Create(
                caster_id: GetCaster(battlefield).id,
                target_position: GetCasterOpponentPosition(battlefield),
                move_id: GetCaster(battlefield).moveset[3].id);
            
            var expected_results = new (IEnumerable<MoveAction> move_actions, int caster_debuff_count, int opponent_debuff_count)[]
            {
                (move_actions: new[] { move_action_1 }, caster_debuff_count: 3, opponent_debuff_count: 0),
                (move_actions: new[] { move_action_2 }, caster_debuff_count: 0, opponent_debuff_count: 3),
                (move_actions: new[] { move_action_3 }, caster_debuff_count: 0, opponent_debuff_count: 1),
                (move_actions: new[] { move_action_4 }, caster_debuff_count: 0, opponent_debuff_count: 0),
                (move_actions: new[] { move_action_4, move_action_4 }, caster_debuff_count: 0, opponent_debuff_count: 0),
                (move_actions: new[] { move_action_1, move_action_4, move_action_3, move_action_2, move_action_1 }, caster_debuff_count: 6, opponent_debuff_count: 4),
            };
            foreach (var (move_actions, caster_debuff_count, opponent_debuff_count) in expected_results)
            {
                var tmp_battlefield = battlefield;
                foreach (var _moveaction in move_actions)
                {
                    tmp_battlefield = Battlecore
                        .DoCast(tmp_battlefield, _moveaction)
                        .battlefield_after;
                }
                var caster_id = GetCaster(tmp_battlefield).id;
                var actual_caster_debuff_count = tmp_battlefield.GetBattleMonsterDebuffs(caster_id).Length;
                var opponent_id = GetCasterOpponent(tmp_battlefield).id;
                var actual_opponent_debuff_count = tmp_battlefield.GetBattleMonsterDebuffs(opponent_id).Length;
                Assert.True(actual_caster_debuff_count == caster_debuff_count);
                Assert.True(actual_opponent_debuff_count == opponent_debuff_count);
            }
            
            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }

            static BattleMonster GetCasterOpponent(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Blue][0];
            }

            static FieldPosition GetCasterOpponentPosition(Battlefield battlefield)
            {
                var opponent = GetCasterOpponent(battlefield);
                var position = battlefield.TryGetMonsterFieldPosition(opponent.id);
                Assert.True(position != null);
                return position!.Value;
            }

            static Battlefield CreateBattlefield()
            {
                var debuff_effect = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(1),
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(0),
                    }),
                    name: "direct_damage");
                var no_debuff_effect_1 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        RecurrenceComponent.Create(EffectRecurrence.Postturn),
                        DurationComponent.Create(0), // duration to 0: should not apply a debuff
                        TargetMonsterComponent.Create(),
                        WarmupComponent.Create(0),
                    }),
                    name: "direct_damage");
                var no_debuff_effect_2 = Effect.Create(
                    default_components: ImmutableArray.CreateRange(new [] {
                        DamageComponent.Create(Type.Dark, 5),
                        DurationComponent.Create(1),
                        TargetMonsterComponent.Create(),
                        // no recurrence: should not apply a debuff
                    }),
                    name: "direct_damage");
                var moves = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.FixedSelf,
                        effects: ImmutableArray.CreateRange(new[]
                        {
                            debuff_effect,
                            debuff_effect,
                            debuff_effect,
                            no_debuff_effect_1,
                            no_debuff_effect_2,
                        })),
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
                        effects: ImmutableArray.CreateRange(new[]
                        {
                            debuff_effect,
                            debuff_effect,
                            debuff_effect,
                        })),
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
                        effects: ImmutableArray.CreateRange(new[]
                        {
                            debuff_effect,
                        })),
                    // this last move will not apply a remaining debuff
                    Move.Create(
                        name: "move",
                        move_target_type: MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
                        effects: DummyEffects()),
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    BattleMonster.Create(DummyMonster(moves)),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }
    }
}
