using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void GetEligibleBattleMonsterIdsForSwapInTest_Full()
        {
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue4", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue5", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue6", level: 50, moves: DummyMoves(), species: DummySpecies())),
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            var expected = new[] 
            {
                (team: TeamColour.Grey, array: ImmutableArray.CreateRange(new[] { grey_array[2].id, grey_array[3].id, grey_array[4].id, grey_array[5].id })),
                (team: TeamColour.Blue, array: ImmutableArray.CreateRange(new[] { blue_array[2].id, blue_array[3].id, blue_array[4].id, blue_array[5].id })),
            };
            foreach (var (team, array) in expected)
            {
                var actual = Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, team);
                Assert.True(actual.Length == actual.Distinct().Count());
                Assert.True(Enumerable.SequenceEqual(actual, array));
                actual = Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.GetOpposite(team));
                Assert.True(actual.Length == actual.Distinct().Count());
                Assert.False(Enumerable.SequenceEqual(actual, array));
            }
        }

        [Fact]
        public void GetEligibleBattleMonsterIdsForSwapInTest_Mixed()
        {
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            var expected_true = ImmutableArray.CreateRange(new[] { grey_array[2].id, grey_array[4].id });
            var expected_false = ImmutableArray.CreateRange(new[] { grey_array[3].id, grey_array[5].id });
            var actual = Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.Grey);
            Assert.True(Enumerable.SequenceEqual(actual, expected_true));
            Assert.False(Enumerable.SequenceEqual(actual, expected_false));
        }

        [Fact]
        public void GetEligibleBattleMonsterIdsForSwapInTest_Empty()
        {
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            Assert.True(Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.Grey).IsDefaultOrEmpty);
            Assert.True(Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.Blue).IsDefaultOrEmpty);
        }

        [Fact]
        public void GetEligibleBattleMonsterIdsForSwapInTest_KnockedOut()
        {
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "blue3", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            Assert.True(Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.Grey).IsDefaultOrEmpty);
            Assert.True(Battlecore.GetEligibleBattleMonsterIdsForSwapIn(battlefield, TeamColour.Blue).IsDefaultOrEmpty);
        }

    }
}
