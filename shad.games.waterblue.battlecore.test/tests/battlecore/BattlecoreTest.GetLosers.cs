using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void GetLosersTest()
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.CreateRange(new[] { big_damage_effect }));
            var moves = ImmutableArray.CreateRange(new[] { big_damage_move });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            // make blue lose
            var battlefield_blue_losing = MakeTeamLose(TeamColour.Blue, battlefield);
            var battlefield_blue_losers = battlefield_blue_losing.GetLosers();
            // make grey lose
            var battlefield_grey_losing = MakeTeamLose(TeamColour.Grey, battlefield);
            var battlefield_grey_losers = battlefield_grey_losing.GetLosers();

            /* assertions */
            Assert.True(battlefield.GetLosers().IsDefaultOrEmpty);
            Assert.False(battlefield_blue_losers.IsDefaultOrEmpty);
            Assert.False(battlefield_grey_losers.IsDefaultOrEmpty);
            Assert.True(battlefield_blue_losers.Length == 1 && battlefield_blue_losers[0] == TeamColour.Blue);
            Assert.True(battlefield_grey_losers.Length == 1 && battlefield_grey_losers[0] == TeamColour.Grey);

            static Battlefield MakeTeamLose(TeamColour team, Battlefield battlefield)
            {
                var caster = battlefield.field_monsters[TeamColour.GetOpposite(team)][0];
                while (!get_target(battlefield).IsKnockedOut())
                {
                    battlefield = battlefield
                        .DoCast(MoveAction.Create(
                            caster_id: caster.id,
                            move_id: caster.moveset[0].id))
                        .battlefield_after;
                }
                return battlefield;

                BattleMonster get_target(Battlefield bf) => bf.field_monsters[team][0];
            }
        }
    }
}
