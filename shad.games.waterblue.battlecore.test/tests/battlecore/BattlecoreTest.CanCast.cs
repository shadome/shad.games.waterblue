using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        // Testing CanCast should cross the following variables:
        //  - what is the MoveTargetType
        //  - are the targets valid or not (KO, not on the field, etc.)
        //  - are the casters valid or not (KO, not on the field, etc.)
        [Fact]
        public void CanCastTest()
        {
            // System.Console.WriteLine("[CanCastTest] started");

            var moves_fixed_bf_side_opponent = ImmutableArray.CreateRange(new[]
            {
                Move.Create(
                    name: "move",
                    move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                    effects: DummyEffects())
            });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue3", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue4", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue5", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue6", level: 50, moves: moves_fixed_bf_side_opponent, species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            for (byte i = 0; i < teams.Length; i++)
            {
                var team_colour = teams[i].team_colour;
                for (byte idx = 0; idx < battlefield.field_monsters[team_colour].Length; idx++)
                {
                    var caster = battlefield.field_monsters[team_colour][idx];
                    // OK
                    Assert.True(battlefield.CanCast(
                        MoveAction.Create(
                            caster_id: caster.id,
                            move_id: caster.moveset[0].id)));
                }
                for (byte idx = 0; idx < battlefield.party_monsters[team_colour].Length; idx++)
                {
                    var caster = battlefield.party_monsters[team_colour][idx];
                    // KO - party monster cannot cast
                    Assert.False(battlefield.CanCast(
                        MoveAction.Create(
                            caster_id: caster.id,
                            move_id: caster.moveset[0].id)));
                }
            }

            // System.Console.WriteLine("[CanCastTest] ended");
        }

        [Fact]
        public void CanCastTest_2v2_MoveTargetTypes()
        {
            // System.Console.WriteLine("[CanCastTest_2v2_MoveTargetTypes] started");

            Battlefield battlefield;
            MoveAction move_action;
            /* MoveTargetType.FixedBattlefieldSideOpponent */
            /* MoveTargetType.FixedBattlefieldAllExceptCaster */
            /* MoveTargetType.NotApplicable */
            /* MoveTargetType.FixedBattlefieldAll */
            /* MoveTargetType.FixedSelf */
            var similar_move_target_types = new[]
            {
                MoveTargetType.FixedBattlefieldSideOpponent,
                MoveTargetType.FixedBattlefieldAllExceptCaster,
                MoveTargetType.NotApplicable,
                MoveTargetType.FixedBattlefieldAll,
                MoveTargetType.FixedSelf,
            };
            foreach (var move_target_type in similar_move_target_types)
            {
                battlefield = CreateBattlefield(move_target_type);
                var teams = new TeamColour?[] { null, TeamColour.Blue, TeamColour.Grey };
                var battlemonster_positions =
                    new FieldPosition?[] { null, default(FieldPosition) }
                    .Concat(battlefield.field_monsters
                        .SelectMany(
                            collectionSelector: _kvp => _kvp.Value, 
                            resultSelector: (_kvp, _bmonster) => 
                                battlefield.TryGetMonsterFieldPosition(_bmonster.id)));
                move_action = MoveAction.Create(
                    caster_id: GetCaster(battlefield).id,
                    move_id: GetCaster(battlefield).moveset[0].id);
                Assert.True(battlefield.CanCast(
                    move_action: move_action));
                // unnecessary optional parameters should not fail
                foreach (var _team in teams)
                {
                    foreach (var _fieldposition in battlemonster_positions)
                    {
                        move_action = MoveAction.Create(
                            caster_id: GetCaster(battlefield).id,
                            move_id: GetCaster(battlefield).moveset[0].id,
                            target_position: _fieldposition,
                            target_team: _team);
                        Assert.True(battlefield.CanCast(
                            move_action: move_action));
                        move_action = MoveAction.Create(
                            caster_id: GetCaster(battlefield).id,
                            move_id: GetCaster(battlefield).moveset[0].id,
                            target_position: default(FieldPosition),
                            target_team: TeamColour.Blue);
                        Assert.True(battlefield.CanCast(
                            move_action: move_action));
                        move_action = MoveAction.Create(
                            caster_id: GetCaster(battlefield).id,
                            move_id: GetCaster(battlefield).moveset[0].id,
                            target_position: default(FieldPosition),
                            target_team: TeamColour.Blue);
                        Assert.True(battlefield.CanCast(
                            move_action: move_action));
                    }
                }
            }

            /* MoveTargetType.ChoiceBattlefieldSingleExceptCaster */
            battlefield = CreateBattlefield(MoveTargetType.ChoiceBattlefieldSingleExceptCaster);
            var valid_target_positions = battlefield.field_monsters
                .SelectMany(_kvp => _kvp.Value, (_kvp, _bmonster) => _bmonster.id)
                .Where(_id => _id != GetCaster(battlefield).id)
                .Select(_id => battlefield.TryGetMonsterFieldPosition(_id));
            foreach (var _targetposition in valid_target_positions)
            {
                var valid_move_actions = new[]
                {
                    MoveAction.Create(
                        caster_id: GetCaster(battlefield).id, 
                        move_id: GetCaster(battlefield).moveset[0].id, 
                        target_position: _targetposition),
                    // unnecessary optional parameters should not fail
                    MoveAction.Create(
                        caster_id: GetCaster(battlefield).id, 
                        move_id: GetCaster(battlefield).moveset[0].id, 
                        target_position: _targetposition, 
                        target_team: TeamColour.Blue),
                    // unnecessary optional parameters should not fail
                    MoveAction.Create(
                        caster_id: GetCaster(battlefield).id, 
                        move_id: GetCaster(battlefield).moveset[0].id, 
                        target_position: _targetposition, 
                        target_team: TeamColour.Grey),
                };
                foreach (var _move_action in valid_move_actions)
                {
                    Assert.True(battlefield.CanCast(
                        move_action: _move_action));
                }
            }

            // System.Console.WriteLine("[CanCastTest_2v2_MoveTargetTypes] ended");

            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Blue][1];
            }

            static Battlefield CreateBattlefield(MoveTargetType move_target_type)
            {
                var moves_fixed_bf_side_opponent = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: move_target_type,
                        effects: DummyEffects())
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                    DummyBattleMonster(), 
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                    BattleMonster.Create(DummyMonster(moves_fixed_bf_side_opponent)),
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }

        [Fact]
        public void CanCastTest_1v1_OpponentKo()
        {
            // System.Console.WriteLine("[CanCastTest_1v1_OpponentKo] started");

            var invalid_move_target_types = new[]
            {
                MoveTargetType.FixedBattlefieldSideOpponent,
                MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
                MoveTargetType.FixedBattlefieldAllExceptCaster,
            };
            var valid_move_target_types = new[]
            {
                MoveTargetType.NotApplicable, // rain, etc.
                MoveTargetType.FixedBattlefieldAll,
                MoveTargetType.FixedSelf,
            };
            foreach (var move_target_type in invalid_move_target_types)
            {
                var battlefield = CreateBattlefield(move_target_type);
                var move_action = MoveAction.Create(
                    caster_id: GetCaster(battlefield).id,
                    move_id: GetCaster(battlefield).moveset[0].id);
                Assert.False(battlefield.CanCast(
                    move_action: move_action));
            }
            foreach (var move_target_type in valid_move_target_types)
            {
                var battlefield = CreateBattlefield(move_target_type);
                var move_action = MoveAction.Create(
                    caster_id: GetCaster(battlefield).id,
                    move_id: GetCaster(battlefield).moveset[0].id);
                Assert.True(battlefield.CanCast(
                    move_action: move_action));
            }

            // System.Console.WriteLine("[CanCastTest_1v1_OpponentKo] ended");

            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }

            static Battlefield CreateBattlefield(MoveTargetType move_target_type)
            {
                var moves_fixed_bf_side_opponent = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: move_target_type,
                        effects: DummyEffects())
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    BattleMonster.Create(DummyMonster(moves_fixed_bf_side_opponent)),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    KnockOut(DummyBattleMonster()), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }


        [Fact]
        public void CanCastTest_1v1_CasterKo()
        {
            // System.Console.WriteLine("[CanCastTest_1v1_CasterKo] started");

            var invalid_move_target_types = new[]
            {
                MoveTargetType.FixedBattlefieldSideOpponent,
                MoveTargetType.ChoiceBattlefieldSingleExceptCaster,
                MoveTargetType.FixedBattlefieldAllExceptCaster,
                MoveTargetType.NotApplicable, // rain, etc.
                MoveTargetType.FixedBattlefieldAll,
                MoveTargetType.FixedSelf,
            };
            foreach (var move_target_type in invalid_move_target_types)
            {
                var battlefield = CreateBattlefield(move_target_type);
                var move_action = MoveAction.Create(
                    caster_id: GetCaster(battlefield).id,
                    move_id: GetCaster(battlefield).moveset[0].id);
                Assert.False(battlefield.CanCast(
                    move_action: move_action));
            }

            // System.Console.WriteLine("[CanCastTest_1v1_CasterKo] ended");

            static BattleMonster GetCaster(Battlefield battlefield)
            {
                return battlefield.field_monsters[TeamColour.Grey][0];
            }

            static Battlefield CreateBattlefield(MoveTargetType move_target_type)
            {
                var moves_fixed_bf_side_opponent = ImmutableArray.CreateRange(new[]
                {
                    Move.Create(
                        name: "move",
                        move_target_type: move_target_type,
                        effects: DummyEffects())
                });
                var grey_array = ImmutableArray.CreateRange(new[]
                {
                    KnockOut(BattleMonster.Create(DummyMonster(moves_fixed_bf_side_opponent))),
                });
                var blue_array = ImmutableArray.CreateRange(new[]
                {
                    DummyBattleMonster(), 
                });
                var teams = new[]
                {
                    (team_colour: TeamColour.Grey, array: grey_array),
                    (team_colour: TeamColour.Blue, array: blue_array),
                };
                return Battlefield.Create(ImmutableArray.CreateRange(teams));
            }
        }

    }
}
