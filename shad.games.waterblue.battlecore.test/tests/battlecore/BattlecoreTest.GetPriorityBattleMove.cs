using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void GetPriorityBattleMoveTest()
        {

            var slow_monster = BattleMonster.Create(BattlecoreTest.SlowBigDmgResistantMonster());
            var quick_monster = BattleMonster.Create(BattlecoreTest.QuickLowDmgWeakMonster());
            var slow_move = slow_monster.moveset[0];
            var quick_move = quick_monster.moveset[0];

            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: ImmutableArray.Create(slow_monster)),
                (team_colour: TeamColour.Blue, array: ImmutableArray.Create(quick_monster)),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            var slow_move_action = MoveAction.Create(
                caster_id: slow_monster.id,
                move_id: slow_move.id,
                target_team: TeamColour.Blue,
                target_position: battlefield.TryGetMonsterFieldPosition(quick_monster.id)!.Value);
            var quick_move_action = MoveAction.Create(
                caster_id: quick_monster.id,
                move_id: quick_move.id,
                target_team: TeamColour.Grey,
                target_position: battlefield.TryGetMonsterFieldPosition(slow_monster.id)!.Value);

            var priority_move = default(MoveAction?);

            priority_move = Battlecore.GetPriorityBattleMove(battlefield, ImmutableArray.CreateRange(new[] { slow_move_action, quick_move_action }));
            Assert.True(quick_move_action.Equals(priority_move));

            priority_move = Battlecore.GetPriorityBattleMove(battlefield, ImmutableArray.CreateRange(new[] { quick_move_action, slow_move_action }));
            Assert.True(quick_move_action.Equals(priority_move));

            priority_move = Battlecore.GetPriorityBattleMove(battlefield, ImmutableArray.CreateRange(new[] { slow_move_action }));
            Assert.True(slow_move_action.Equals(priority_move));

            priority_move = Battlecore.GetPriorityBattleMove(battlefield, ImmutableArray.CreateRange(new[] { quick_move_action }));
            Assert.True(quick_move_action.Equals(priority_move));

        }
    }
}
