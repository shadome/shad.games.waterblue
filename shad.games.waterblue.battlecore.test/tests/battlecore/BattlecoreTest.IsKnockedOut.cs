using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {

        [Fact]
        public void IsKnockedOutTest1()
        {
            // effect created to one-hit-ko its target
            var big_damage_effect = BigDamageEffect();
            var big_damage_move = Move.Create(
                name: "boum!",
                move_target_type: MoveTargetType.FixedBattlefieldSideOpponent,
                effects: ImmutableArray.CreateRange(new[] { big_damage_effect }));
            var moves = ImmutableArray.CreateRange(new[] { big_damage_move });
            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: moves, species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield_before = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            var battlefield_after = battlefield_before
                .DoCast(MoveAction.Create(
                    caster_id: grey_array[0].id,
                    move_id: grey_array[0].moveset[0].id))
                .battlefield_after;
            var monster_before = battlefield_before.field_monsters[TeamColour.Blue][0];
            var monster_after = battlefield_after.field_monsters[TeamColour.Blue][0];
            Assert.False(monster_before.IsKnockedOut());
            Assert.True(monster_after.IsKnockedOut());
        }

        [Fact]
        public void IsKnockedOutTest2()
        {
            var monster = BattleMonster.Create(
                Monster.Create(
                    name: "blue1", 
                    level: 50, 
                    moves: DummyMoves(), 
                    species: DummySpecies()));
            var monter_ko = BattlecoreTest.KnockOut(monster);
            Assert.False(monster.IsKnockedOut());
            Assert.True(monter_ko.IsKnockedOut());
        }
    }
}
