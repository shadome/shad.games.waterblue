using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class BattlecoreTest
    {
        [Fact]
        public void CanSwapTest()
        {
            // System.Console.WriteLine("[CanSwapTest] started");

            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey5", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "grey6", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue2", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue4", level: 50, moves: DummyMoves(), species: DummySpecies())),
                BattleMonster.Create(Monster.Create(name: "blue5", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                BattleMonster.Create(Monster.Create(name: "blue6", level: 50, moves: DummyMoves(), species: DummySpecies())),
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(
                ImmutableArray.CreateRange(teams));
            BattleMonsterId bmid;
            for (byte i = 0; i < teams.Length; i++)
            {
                var (team_colour, array) = teams[i];
                var opposite_team = teams[^(i + 1)];
                for (byte outgoing_idx = 0; outgoing_idx < 2; outgoing_idx++)
                {
                    for (byte incoming_idx = 2; incoming_idx < 6; incoming_idx++)
                    {
                        // OK
                        var can_swap = Battlecore.CanSwap(
                            battlefield: battlefield,
                            swap_action: SwapAction.Create(
                                outgoing: array[outgoing_idx].id, 
                                incoming: array[incoming_idx].id));
                        Assert.True(can_swap);
                        // KO - wrong outgoing: party id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[7 - incoming_idx].id, incoming: array[incoming_idx].id)));
                        // KO - wrong outgoing: opponent battlefield id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield, 
                            swap_action: SwapAction.Create(outgoing: opposite_team.array[outgoing_idx].id, incoming: array[incoming_idx].id)));
                        // KO - wrong outgoing: opponent party id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield, 
                            swap_action: SwapAction.Create(outgoing: opposite_team.array[7 - incoming_idx].id, incoming: array[incoming_idx].id)));
                        // KO - wrong incoming: battlefield id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[outgoing_idx].id, incoming: array[1 - outgoing_idx].id)));
                        // KO - wrong incoming: opponent party id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[outgoing_idx].id, incoming: opposite_team.array[incoming_idx].id)));
                        // KO - wrong incoming: opponent battlefield id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[outgoing_idx].id, incoming: opposite_team.array[1 - outgoing_idx].id)));
                        // KO - default outgoing id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: new(), incoming: array[incoming_idx].id)));
                        // KO - default incoming id
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[outgoing_idx].id, incoming: new())));
                        // KO - non-existing outgoing id
                        bmid = BattleMonster.Create(Monster.Create(name: "x", level: 50, moves: DummyMoves(), species: DummySpecies())).id;
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: bmid, incoming: array[incoming_idx].id)));
                        // KO - non-existing incoming id
                        bmid = BattleMonster.Create(Monster.Create(name: "x", level: 50, moves: DummyMoves(), species: DummySpecies())).id;
                        Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                            swap_action: SwapAction.Create(outgoing: array[outgoing_idx].id, incoming: bmid)));
                    }
                }
            }

            // System.Console.WriteLine("[CanSwapTest] ended");
        }

        [Fact]
        public void CanSwapTest_KnockedOutCannotSwapIn()
        {
            // System.Console.WriteLine("[CanSwapTest_KnockedOutCannotSwapIn] started");

            var grey_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "grey1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey2", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
                BattleMonster.Create(Monster.Create(name: "grey3", level: 50, moves: DummyMoves(), species: DummySpecies())), 
                KnockOut(BattleMonster.Create(Monster.Create(name: "grey4", level: 50, moves: DummyMoves(), species: DummySpecies()))), 
            });
            var blue_array = ImmutableArray.CreateRange(new[]
            {
                BattleMonster.Create(Monster.Create(name: "blue1", level: 50, moves: DummyMoves(), species: DummySpecies())), 
            });
            var teams = new[]
            {
                (team_colour: TeamColour.Grey, array: grey_array),
                (team_colour: TeamColour.Blue, array: blue_array),
            };
            var battlefield = Battlefield.Create(ImmutableArray.CreateRange(teams));
            // OK
            Assert.True(Battlecore.CanSwap(battlefield: battlefield,
                swap_action: SwapAction.Create(outgoing: grey_array[0].id, incoming: grey_array[2].id)));
            Assert.True(Battlecore.CanSwap(battlefield: battlefield,
                swap_action: SwapAction.Create(outgoing: grey_array[1].id, incoming: grey_array[2].id)));
            // KO - knocked out cannot swap in
            Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                swap_action: SwapAction.Create(outgoing: grey_array[0].id, incoming: grey_array[3].id)));
            Assert.False(Battlecore.CanSwap(battlefield: battlefield,
                swap_action: SwapAction.Create(outgoing: grey_array[1].id, incoming: grey_array[3].id)));

            // System.Console.WriteLine("[CanSwapTest_KnockedOutCannotSwapIn] ended");
        }
    }
}
