using Xunit;
using shad.games.waterblue.battlecore.model;
using shad.games.waterblue.battlecore.services;
using System.Collections.Immutable;
using System.Linq;

namespace shad.games.waterblue.battlecore.test
{
    public partial class TeamColourTest
    {

        [Fact]
        public void GetOppositeTest()
        {
            Assert.False(TeamColour.GetOpposite(TeamColour.Blue) == TeamColour.Blue);
            Assert.False(TeamColour.GetOpposite(TeamColour.Grey) == TeamColour.Grey);
            Assert.True(TeamColour.GetOpposite(TeamColour.Blue) == TeamColour.Grey);
            Assert.True(TeamColour.GetOpposite(TeamColour.Grey) == TeamColour.Blue);
            Assert.True(TeamColour.GetOpposite(TeamColour.GetOpposite(TeamColour.Blue)) == TeamColour.Blue);
            Assert.True(TeamColour.GetOpposite(TeamColour.GetOpposite(TeamColour.Grey)) == TeamColour.Grey);
        }
    }
}
